#
# SMTP hi-level handling class
#

class SMTP < SMTPsocket
include Fold
  def initialize(rc)
    @rc = rc
    @stat = nil
    @prog = nil
    @dlog = nil
    super(rc['server'],rc['port'],rc['domain'])
  end
  def callback(stat,prog,dlog)
    @stat = stat
    @prog = prog
    @dlog = dlog
  end
  # MIME encode
  def mime(str)
    s = OS.tojis(fold(str,"",36,"\n "))
    s.gsub!(/(\x1b\$B.*?\x1b\(B)/) {
      "=?ISO-2022-JP?B?"+[$1].pack("m").chop+"?="
    }
    s.gsub!(/\?=(.*?)=\?/) { "?="+$1+"\n =?" }
    s
  end
  # make RFC822 text
  def rfc822(hdr,text,sign,files)
    if(files.size != 0) then
      srand
      r = ""
      for i in 1..24
	r += (rand(254)+1).chr
      end
      rnd = ([r].pack("m")).chop+"/"+@rc['server']
      hdr.each {|k,v|
	case k
	when /^Mime-Version$/i,/^Content-Type$/i
	when /^Content-Transfer-Encoding$/i
	when /^X-Mailer$/i
	when /^Received$/i
	else
	  if(v != "") then
	    str = k+": "+mime(v+"\n")
	    str.each_line {|line|
	      yield(line.chop)
	    }
	  end
	end
      }
      yield("Mime-Version: 1.0")
      yield("Content-Type: multipart/mixed; boundary=\"#{rnd}\"")
      yield("Content-Transfer-Encoding: 7bit")
      yield("X-Mailer: #{$info}")
      yield("")
      yield("This is a MIME-encapsulated message")
      yield("")
      if(text) then
	yield("--#{rnd}")
	yield("Content-Type: text/plain; charset=ISO-2022-JP")
	yield("Content-Transfer-Encoding: 7bit")
	yield("")
	text.each_line {|line|
	  line = "."+line if(line =~ /^\./)
	  line << "\n" if(line !~ /\n/)
	  yield(OS.tojis(line.chop))
	}
      end
      if(sign) then
	sign.each_line {|line|
	  line = "."+line if(line =~ /^\./)
	  line << "\n" if(line !~ /\n/)
	  yield(OS.tojis(line.chop))
	}
      end
      files.each {|k,v|
	case k
	when /^\[B\]/
	  file = k.sub(/^\[B\]/,"")
	  case k
	  when /\.html/,/\.htm/
	    ctype = "text/html"
	  when /\.jpeg/,/\.jpg/
	    ctype = "image/jpeg"
	  when /\.gif/
	    ctype = "image/gif"
	  else
	    ctype = "application/octet-stream"
	  end
	  yield("--#{rnd}")
	  yield("Content-Type: #{ctype}; name=\"#{file}\"")
	  yield("Content-Transfer-Encoding: base64")
	  yield("")
	  open(OS.path(v)) {|fd|
	    fd.binmode
	    while(block = fd.read(45))
	      yield([block].pack("m").chop)
	    end
	  }
	else
	  file = k.sub(/^\[T\]/,"")
	  case k
	  when /\.html/,/\.htm/
	    ctype = "text/html"
	  else
	    ctype = "text/plain"
	  end
	  yield("--#{rnd}")
	  yield("Content-Type: #{ctype}; name=\"#{file}\"")
	  yield("Content-Transfer-Encoding: 7bit")
	  yield("")
	  open(OS.path(v)) {|fd|
	    fd.each {|line|
	      line = "."+line if(line =~ /^\./)
	      line << "\n" if(line !~ /\n/)
	      yield(OS.tojis(line.chop))
	    }
	  }
	end
      }
    else
      hdr.each {|k,v|
	case k
	when /^Mime-Version$/i,/^Content-Type$/i
	when /^Content-Transfer-Encoding$/i
	when /^X-Mailer$/i
	when /^Received$/i
	else
	  if(v != "") then
	    str = k+": "+mime(v+"\n")
	    str.each_line {|line|
	      yield(line.chop)
	    }
	  end
	end
      }
      yield("Mime-Version: 1.0")
      yield("Content-Type: text/plain; charset=ISO-2022-JP")
      yield("Content-Transfer-Encoding: 7bit")
      yield("X-Mailer: #{$info}")
      yield("")
      if(text) then
	text.each_line {|line|
	  line = "."+line if(line =~ /^\./)
	  line << "\n" if(line !~ /\n/)
	  yield(OS.tojis(line.chop))
	}
      end
      if(sign) then
	sign.each_line {|line|
	  line = "."+line if(line =~ /^\./)
	  line << "\n" if(line !~ /\n/)
	  yield(OS.tojis(line.chop))
	}
      end
    end
  end
  # send message
  def send(hdr,body,sign,files)
    @stat.call("Send...")
    login()
    mail(hdr['From'])
    if(hdr['To'].gsub(/ /,"") != "") then
      list = hdr['To'].gsub(/ /,"").split(/,/)
      list.each {|item| rcpt(item) }
    end
    if(hdr['Cc'].gsub(/ /,"") != "") then
      list = hdr['Cc'].gsub(/ /,"").split(/,/)
      list.each {|item| rcpt(item) }
    end
    if(hdr['Bcc'].gsub(/ /,"") != "") then
      list = hdr['Bcc'].gsub(/ /,"").split(/,/)
      list.each {|item| rcpt(item) }
    end
    data()
    rfc822(hdr,body,sign,files) {|line| text(line+"\r\n") }
    text(".\r\n")
    quit()
    @stat.call()
  end
end
