#
# SGmail configuration
#

class Address
attr :adr
attr :rev
  def initialize(text)
    @adr = []
    @rev = {}
    make_address(text)
  end
  ## set address table
  def make_address(address)
    @array = address.split(/\n/)
    @lines = 0
    tbl = []
    rev = {}
    if(make_address_sub(tbl,rev,true) != 0) then
      return nil
    end
    @adr = tbl
    @rev = rev
    return true
  end
  def make_address_sub(n,r,f)
    while(line = @array.shift)
      @lines += 1
      line.sub!(/^\s*/,"")
      line.sub!(/\s*$/,"")
      next if(line =~ /^#.*$/)
      next if(line =~ /^$/)
      return if(line == "}")
      if(line =~ /^(.*?):\s*(.*)$/) then
        if(f) then
          n << { 'nam'=>$1,'val'=>$2 }
          r[$2] = $1
        else
          if(r[$2] == nil) then
            r[$2] = $1
          end
        end         
      elsif(line =~ /^(.*?)\s*\{$/) then
        if($1 != "") then
          child = []
          n << { 'nam'=>$1,'cld'=>child }
          make_address_sub(child,r,true)
        else
          make_address_sub(child,r,false)
        end
      else
        SGDialog.new("addr_error",@lines)
        return @lines
      end
    end
    return 0
  end
end

class Signature
attr :sign
  def initialize(text)
    make_signature(text)
  end
  ## make signature table
  def make_signature(signature)
    @sign = []
    idle = true
    tag = ""
    name = ""
    text = ""
    signature.each_line {|line|
      line.chop!
      if(idle) then
        if(line =~ /^(.*?)::(.*?)$/) then
          name = $1
          tag = $2
          text = ""
          idle = false
        end
      else
        if(line =~ /^#{tag}$/) then
          @sign << { 'nam'=>name,'val'=>text }
          idle = true
        else
          text << line+"\n"
        end
      end
    }
    @sign << { 'nam'=>"None",'val'=>"" }
    return true
  end
end

class Refile
attr :refile
  def initialize(text)
    @refile = []
    make_refile(text)
  end
  ## find string
  def find_mbox(tree,val)
    tree.each {|v|
      if(v['cld'] != nil) then
        if(find_mbox(v['cld'],val)) then
          return true
        end
      else
        if(v['val'].sub(/^(.*?):/,"") == val && v['flg'] != 'F') then
          return true
        end
      end
    }
    return false
  end
  def make_refile(refile)
    @refile = []
    @errors = 0
    @lines = 0
    refile.each_line {|line|
      @lines += 1
      line.chop!
      if(line =~ /^$/ || line =~ /^#.*$/) then next end
      if(line =~ /^(!*)([\w-]+):\s*\/(.*)\/\s*>>\s*(.*?)\s*$/) then
        if($1 == nil || $2 == nil || $3 == nil || $4 == nil) then
          SGDialog.new("refile_error",@lines)
          return false
        end
        d = $4
#        if(find_mbox(tree,d)) then
          @refile << { 'flg'=>$1,'nam'=>$2,'cnd'=>$3,'dst'=>d }
#        else
#          SGDialog.new(
#            $rc2['dialog']['no_mbox_error'][0],
#            format($rc2['dialog']['no_mbox_error'][1],d)
#          )
#          @errors += 1
#        end
      end
    }
    if(@errors != 0) then
      return false
    else
      return true
    end
  end
  # find
  def find(tag,see)
    hdr = tag.header[0]
    dst = nil
    @refile.each {|item|
      if(hdr[item['nam']]) then
	if(hdr[item['nam']] =~ /#{item['cnd']}/) then
	  if(see or item['flg'] == "!") then
	    if(item['dst'] != tag.folder.box) then
	      dst = item['dst']
	    end
	  end
	  break
	end
      end
    }
    dst
  end
end
