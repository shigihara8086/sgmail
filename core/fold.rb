#
# text folding
#

require 'kconv'

module Fold
  ## fold string
  def fold(str,pat,width=76,sep="\n")
    l = width
    s = ""
    w = ""
    t = ""
    jmode = false
    str.split(/.*?/).each {|c|
      if(jmode) then
        if(c.length == 1) then
          w << c
          jmode = false
        else
          if(l <= 0) then
	    if(pat =~ /#{c}/) then
              l -= c.length
              s << c
            else
              t << s + sep
              l = width
              l -= c.length
              s = c
            end
          else
            l -= c.length
            s << c
          end
        end
      else
        if(c.length == 1) then
          if(c == ' ') then
            if(s.length + w.length <= width) then
              l -= w.length + 1
              s << w + " "
              w = ""
            else
              t << s + sep
              s = w + " "
              w = ""
              l = width - s.length
            end
          else
            w << c
          end
        else
          if(s.length + w.length <= width) then
            l -= w.length
            s << w
            w = ""
          else
            t << s + sep
            s = w
            w = ""
            l = width - s.length
          end
          l -= c.length
          s << c
          jmode = true
        end
      end
    }
    t << s + w
  end
  module_function :fold
  ## fild string by width and MIME encode
  def mime(str,width=36)
    s = Kconv::tojis(fold(str,width,"\n "))
    s.gsub!(/(\x1b\$B.*?\x1b\(B)/) {
      "=?ISO-2022-JP?B?"+[$1].pack("m").chop+"?="
    }
    s.gsub!(/\?=(.*?)=\?/) {
      "?="+$1+"\n =?"
    }
    s
  end
  module_function :mime
end
