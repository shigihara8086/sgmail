#
# IMAP4 hi-level handling class
#

class IMAP < IMAPsocket
attr :success
  def initialize(rc)
    @rc = rc
    @pass = nil
    @stat = nil
    @prog = nil
    @dlog = nil
    @success = false
    @work = false
    @logincnt = 0
    super(rc['server'],rc['user'],rc['passwd'],rc['path'],rc['port'])
  end
  # set callback
  def callback(pass,stat,prog,dlog)
    @pass = pass
    @stat = stat
    @prog = prog
    @dlog = dlog
  end
  # update (dummy for pop3)
  def update(box)
  end
  # login
  def login
    if(@logincnt == 0) then
      ok = false
      if(@user == "" || @passwd == "") then
	@user,@passwd = @pass.call(@user)
      end
      if(@passwd) then
	if(@user != "" && @passwd != "") then
	  begin
	    super
	    @logincnt += 1 if(@rc['ever'])
	    ok = true
	    @success = true
	  rescue
	    @passwd = ""
	    @dlog.call("login_error")
	  end
	else
	  @dlog.call("login_error")
	end
      else
	@passwd = ""
      end
      if(ok == false) then
	raise
      end
    else
      ok = true
    end
    if(ok) then
      @logincnt += 1
    end
  end
  # logout
  def logout
    @logincnt -= 1
    if(@logincnt == 0) then
      super
    end
  end
  # get recent count
  def recent
    cnt = nil
    status("","(RECENT)") {|line|
      if(line) then
	line.chop!
	if(line =~ /^\*\s+(\d+)\s+RECENT/) then cnt = $1.to_i end
      end
    }
    return cnt
  end
  # get messages count
  def messages(box)
    cnt = 0
    status(box,"(MESSAGES)") {|line|
      line.chop!
      if(line =~ /\(MESSAGES\s+(\d+)\)/) then cnt = $1.to_i end
    }
    return cnt
  end
  # get mailbox list (Cyrus)
  def mboxlist_cyrus(ary,prefix)
    list(prefix) {|line|
      line.chop!
      if(line =~ /^\*\s+LIST\s+\((.*?)\)\s+\"(.)\"\s+\"(.*)\"/ ||
	 line =~ /^\*\s+LIST\s+\((.*?)\)\s+\"(.)\"\s+(.*)/) then
	if($3 != "") then
	  flag = $1
	  path = $3
	  path.gsub!(/\./,'/')
	  if(prefix == "") then
	    name = path.sub(/INBOX\//,"")
	    value = path.sub(/INBOX\//,"")
	  else
	    name = path.sub(/INBOX\/#{prefix}\//,"")
	    value = path.sub(/INBOX\//,"")
	  end
	  ary << { 'flg'=>flag,'nam'=>name,'val'=>value,'cld'=>[] }
	end
      end
    }
    ary.each {|item|
      if(prefix == "") then
	mboxlist_cyrus(item['cld'],item['nam'])
      else
	mboxlist_cyrus(item['cld'],prefix+"."+item['nam'])
      end
    }
  end
  # get mailbox list (uw)
  def mboxlist_uw(ary,prefix)
    list(prefix) {|line|
      line.chop!
      if(line =~ /^\*\s+LIST\s+\((.*?)\)\s+\"(.)\"\s+#{@root}\/(.*)/) then
	if($3 != "" && $3 != prefix+"/") then
	  flag = $1
	  path = $3
	  if(prefix == "") then
	    name = path
	    value = path
	  else
	    name = path.sub(/#{prefix}\//,"")
	    value = path
	  end
	  ary << { 'flg'=>flag,'nam'=>name,'val'=>value,'cld'=>[] }
	end
      end
    }
    ary.each {|item|
      if(item['flg'] != /\\NoInferiors/) then
	if(prefix == "") then
	  mboxlist_uw(item['cld'],item['nam'])
	else
	  mboxlist_uw(item['cld'],prefix+"/"+item['nam'])
	end
      end
    }
  end
  # get mailbox tree
  def mblist(box)
    list = []
    if(box == "") then
      list << { 'flg'=>"\\NoInferiors",'nam'=>'INBOX','val'=>"",'cld'=>[] }
    end
    case @type
    when 'cyrus'
      mboxlist_cyrus(list,box)
    else
      mboxlist_uw(list,box)
    end
    list
  end
  # get all list
  def listall
    yield({ 'flg'=>"\\NoInferiors",'nam'=>"INBOX",'val'=>"" })
    super {|line|
      if(line =~ /^\*\s+LIST\s+\((.*?)\)\s+\"(.)\"\s+(.*)/) then
	flg = $1
	box = $3
	nam = box.split("/").pop
	if(nam) then
	  yield({ 'flg'=>flg,'nam'=>nam,'val'=>box })
	end
      end
    }
  end
  # set delete flag
  def remove(uid)
    uid_store("#{uid} +FLAGS (\\Deleted)")
  end
  # set seen flag
  def seen(uid)
    uid_store("#{uid} +FLAGS (\\Seen)")
  end
  # copy message
  def copy(uid,dst)
    to = path(dst)
    uid_copy("#{uid} #{to}")
  end
  # get UID list
  def uidlist
    list = nil
    uid_search("ALL") {|line|
      list = line.chop.sub(/\*\s+SEARCH\s*/,"").split(/ +/)
    }
    if(list && iterator?) then
      size = list.size
      list.each_index {|i|
	yield(list[i],i,size)
      }
      nil
    else
      list
    end
  end
  # get UNSEEN list
  def unseenlist
    list = nil
    uid_search("UNSEEN") {|line|
      list = line.chop.sub(/\*\s+SEARCH\s*/,"").split(/ +/)
    }
    if(list && iterator?) then
      size = list.size
      list.each_index {|i|
	yield(list[i],i,size)
      }
      nil
    else
      list
    end
  end
  # search
  def search(scope,string)
    list = nil
    sv = OS.tojis(string).gsub(/\x5c/,"\x5c\x5c\x5c\x5c")
    uid_search("CHARSET ISO-2022-JP #{scope} \"#{sv}\"") {|line|
      list = line.chop.sub(/\*\s+SEARCH\s*/,"").split(/ +/)
    }
    list
  end
  # get mail header
  def header(uid)
    mime = []
    mime[0] = {}
    mime[0]['Received'] = []
    part = 0
    received = -1
    text = ""
    lines = 0
    bytes = 0
    uid_fetch("#{uid} BODY.PEEK[HEADER]") {|line|
      if(lines == 0) then
	if(line =~ /^\*\s+\d+\s+FETCH\s+\(UID\s+\d+\s+.*?\s+\{(\d+)\}/) then
	  bytes = $1.to_i
	end
      else
	text << line
      end
      lines += 1
    }
    text = text[0..bytes - 1]
    id = nil
    text.each_line {|line|
      line.chop!
      case line
      when /^([\w-]+):\s(.*)$/
	id = $1
	str = $2
	id = "Subject" if(id =~ /^Subject$/i)
	id = "From" if(id =~ /^From$/i)
	id = "To" if(id =~ /^To$/i)
	id = "Date" if(id =~ /^Date$/i)
	id = "Reply-To" if(id =~ /^Reply-To$/i)
	id = "In-Reply-To" if(id =~ /^In-Reply-To$/i)
	id = "Message-Id" if(id =~ /^Message-Id$/i)
	id = "References" if(id =~ /^References$/i)
	id = "Content-Type" if(id =~ /^Content-Type$/i)
	id = "Content-Transfer-Encoding" if(id =~ /^Content-Transfer-Encoding$/i)
	id = "Content-Disposition" if(id =~ /^Content-Disposition$/i)
	id = "Recieved" if(id =~ /^Recieved$/i)
	if(id == "Received") then
	  received += 1
	  mime[part][id][received] = str
	else
	  mime[part][id] = OS.tosys(str).gsub(/\t/," ")
	end
      when /^\s+(.*)$/
	if(id == "Received") then
	  mime[part][id][received] << " "+$1
	else
	  mime[part][id] << OS.tosys($1).gsub(/\t/," ")
	end
      end
    }
    if(mime[0]['Content-Type'] =~ /boundary/i) then
#      part += 1
      more = true
      while(more)
	mime[part] = {} if(part != 0)
	bytes = 0
	lines = 0
	text = ""
	uid_fetch("#{uid} BODY.PEEK[#{part+1}.MIME]") {|line|
	  if(lines == 0) then
	    if(line =~ /^\*\s+\d+\s+FETCH\s+.*?\s+\{(\d+)\}/) then
	      bytes = $1.to_i
	    else
	      more = false
	    end
	  else
	    text << line
	  end
	  lines += 1
	}
	if bytes == 0 then
	  mime.delete( part )
	  more = false
	  next
	end
	text = text[0..bytes - 1]
	id = nil
	text.each_line {|line|
	  line.chop!
	  case line
	  when /^([\w-]+):\s(.*)$/
	    id = $1
	    str = $2
	    id = "Content-Type" if(id =~ /^Content-Type$/i)
	    id = "Content-Transfer-Encoding" if(id =~ /^Content-Transfer-Encoding$/i)
	    id = "Content-Disposition" if(id =~ /^Content-Disposition$/i)
	    mime[part][id] = OS.tosys(str).gsub(/\t/," ")
	  when /^\s+(.*)$/
	    mime[part][id] << OS.tosys($1).gsub(/\t/," ")
	  end
	}
	part += 1
      end
    end
    mime[0]['Subject'] = ""    if(mime[0]['Subject'] == nil)
    mime[0]['From'] = ""       if(mime[0]['From'] == nil)
    mime[0]['To'] = ""         if(mime[0]['To'] == nil)
    mime[0]['Date'] = ""       if(mime[0]['Date'] == nil)
    mime[0]['Message-Id'] = "" if(mime[0]['Message-Id'] == nil)
    mime[0]['References'] = "" if(mime[0]['References'] == nil)
    mime
  end
  # get mail body
  def body(uid,part)
    lines = 0
    bytes = 0
    left = 0
    uid_fetch("#{uid} BODY.PEEK[#{part+1}]") {|line|
      if(lines == 0) then
	if(line =~ /^\*\s+\d+\s+FETCH\s+.*?\s+\{(\d+)\}/) then
	  bytes = left = $1.to_i
	end
      else
	len = line.size
	if(left >= len) then
	  yield(line,bytes-left,bytes) if(iterator?)
	  left -= len
	else
	  if(left > 0) then
	    yield(line[0..left],bytes-left,bytes) if(iterator?)
	  end
	end
      end
      lines += 1
    }
  end
  # store
  def store(box,text)
    append(box,"(\\seen) {#{text.size}}\r\n#{text}\r\n")
  end
end
