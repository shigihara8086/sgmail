#
# POP3 socket level class
#

require 'socket'
require 'md5'

class POP3socket
attr :work
  def initialize(server,user,pass,port,type)
    @server = server
    @user = user
    @pass = pass
    @port = port
    @type = type
    @work = false
  end
  # send command
  def send(cmd)
    @work = true
    print "<",cmd.sub(/#{@pass}/,"********"),">\n" if($SGDEBUG)
    @sock.puts("#{cmd}")
    res = @sock.gets
    if(res =~ /^-\ERR.*/) then
      @work = false
      raise res
    end
    if(iterator?) then
      while(res !~ /^\.\r\n/)
        print "[",res.chop,"]\n" if($SGDEBUG)
        yield(res)
        res = @sock.gets
      end
    end
    @work = false
  end
  # login
  def login
    @sock = TCPsocket.open(@server,@port)
    res = @sock.gets
    print "[",res.chop,"]\n" if($SGDEBUG)
    stamp = nil
    if(res =~ /(<[^@]+@[^@]+>)/) then
      stamp = $1
    end
    if(@type == "pop" or stamp == nil) then
      send("USER #{@user}")
      send("PASS #{@pass}")
    else
      md5 = MD5.new(stamp + @pass).digest
      digest = ""
      md5.each_byte {|i| digest << '%02x' % i }
      send("APOP #{@user} #{digest}")
    end
  end
  # logout command
  def logout
    send("QUIT")
    @sock.close
  end
  # LIST command
  def list
    send("LIST") {|line|
      yield(line) if(iterator?)
    }
  end
  # RETR command
  def retr(i)
    send("RETR #{i}") {|line|
      yield(line) if(iterator?)
    }
  end
  # UIDL command
  def uidl
    send("UIDL") {|line|
      yield(line) if(iterator?)
    }
  end
  # DELE command
  def dele(i)
    send("DELE #{i}")
  end
end
