#
# SMTP socket level class
#

require 'socket'

class SMTPsocket
attr :work
  def initialize(server,port,domain)
    @server = server
    @port = port
    @domain = domain
    @work = false
  end
  # send command
  def scmd(cmd)
    print "<",cmd,">\n" if($SGDEBUG)
    @sock.puts("#{cmd}")
    res = @sock.gets
    print "[",res.chop,"]\n" if($SGDEBUG)
    if(res !~ /^[23]\d\d\s.*/) then
      raise res
    end
  end
  # login
  def login
    @sock = TCPsocket.open(@server,@port)
    while(res = @sock.gets)
      print "[",res.chop,"]\n" if($SGDEBUG)
      if(res =~ /^5\d\d\s/) then
	raise res
      end
      if(res =~ /^[23]\d\d\s/) then
	break
      end
    end
    scmd("HELO #{@domain}")
  end
  # QUIT command
  def quit
    scmd("QUIT")
    @sock.close
    @sock = nil
  end
  # MAIL command
  def mail(from)
    scmd("MAIL FROM: <"+from+">")
  end
  # RCPT command
  def rcpt(to)
    scmd("RCPT TO: <"+to+">")
  end
  # DATA command
  def data
    scmd("DATA")
  end
  # send mail text
  def text(msg)
    @sock.puts(msg)
  end
end
