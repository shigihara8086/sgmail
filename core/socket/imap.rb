#
# IMAP4 socket level class
#

require 'socket'

class IMAPsocket
attr :work
  # initialise
  def initialize(server,user,passwd,root,port)
    @server = server
    @user = user
    @passwd = passwd
    @root = root
    @port = port
    @type = "uw"
    @delm = "/"
    @pty = nil
    @pid = nil
    @sock = nil
    @tag = "A0000"
    @work = false
  end
  # reform mailbox path
  def path(box)
    if(box == "") then
      "INBOX"
    else
      @root+@delm+box.gsub(/\//,@delm)
    end
  end
  # send command and yield each liine
  def send(cmd)
    @work = true
    print "<",@tag," ",cmd.sub(/#{@passwd}/,"********"),">\n" if($SGDEBUG)
    @sock.puts(@tag+" "+cmd)
    while((res = @sock.gets) !~ /^#{@tag}\s/)
      print "[",res.chop,"]\n" if($SGDEBUG)
      yield(res) if(iterator?)
    end
    print "[",res.chop,"]\n" if($SGDEBUG)
    if(res !~ /^#{@tag}\sOK/) then
      @work = false
      raise
    end
    @tag.succ!
    @work = false
  end
  # LOGIN command
  def login
    @sock = TCPsocket.open(@server,@port)
    pass = @passwd.gsub(/([\"\\])/) { '\\'+$1 }
    send("LOGIN #{@user} \""+pass+"\"") {|line|
      if(line =~ /^\*/ and (line =~ /cyrus/i or line =~ /Courier/i)) then
	@type = "cyrus"
	@root = "INBOX"
	@delm = "."
      end
    }
  end
  # LOGOUT command
  def logout
    send("LOGOUT")
    @sock.close
  end
  # NOOP command
  def noop
    send("NOOP")
  end
  # LIST all command
  def listall
    if(@type == "cyrus") then
      send("LIST \"\" INBOX.*") {|line|
	if(line =~ /^\*\s+LIST\s+\((.*?)\)\s+\"(.)\"\s+\"INBOX\.(.*)\"/ ||
	   line =~ /^\*\s+LIST\s+\((.*?)\)\s+\"(.)\"\s+INBOX\.(.*)/) then
	  path = $3
	  path.gsub!(/\./,"/")
	  yield("* LIST (#{$1}) \"/\" #{path}")
	end
      }
    else
      send("LIST \"\" #{@root}/*") {|line|
	if(line =~ /^\*\s+LIST\s+\((.*?)\)\s+\"(.)\"\s+#{@root}\/(.*)\r/) then
	  yield("* LIST (#{$1}) \"/\" #{$3}")
	end
      }
    end
  end
  # LIST command
  def list(box)
    if(box == "") then
      box = @root
    else
      box = @root+@delm+box.gsub(/\//,@delm)
    end
    send("LIST \"\" #{box}#{@delm}%") {|line|
      yield(line) if(iterator?)
    }
  end
  # SELECT command
  def select(box)
    box = path(box)
    send("SELECT #{box}") {|line|
      yield(line) if(iterator?)
    }
  end
  # CLOSE command
  def close
    send("CLOSE") {|line|
      yield(line) if(iterator?)
    }
  end
  # EXPUNGE command
  def expunge
    send("EXPUNGE") {|line|
      yield(line) if(iterator?)
    }
  end
  # CREATE command
  def create(box)
    box = path(box)
    send("CREATE #{box}") {|line|
      yield(line) if(iterator?)
    }
  end
  # DELETE command
  def delete(box)
    box = path(box)
    send("DELETE #{box}") {|line|
      yield(line) if(iterator?)
    }
  end
  # RANAME command
  def rename(src,dst)
    if(src != "" && dst != "") then
      src = path(src)
      dst = path(dst)
      send("RENAME #{src} #{dst}") {|line|
	yield(line) if(iterator?)
      }
    end
  end
  # STATUS command
  def status(box,arg)
    box = path(box)
    send("STATUS #{box} #{arg}") {|line|
      yield(line) if(iterator?)
    }
  end
  # APPEND command
  def append(box,arg)
    box = path(box)
    send("APPEND #{box} #{arg}") {|line|
      yield(line) if(iterator?)
    }
  end
  # UID SEARCH command
  def uid_search(arg)
    send("UID SEARCH #{arg}") {|line|
      yield(line) if(iterator?)
    }
  end
  # UID FETCH command
  def uid_fetch(arg)
    send("UID FETCH #{arg}") {|line|
      yield(line) if(iterator?)
    }
  end
  # UID STORE command
  def uid_store(arg)
    send("UID STORE #{arg}") {|line|
      yield(line) if(iterator?)
    }
  end
  # UID COPY command
  def uid_copy(arg)
    send("UID COPY #{arg}") {|line|
      yield(line) if(iterator?)
    }
  end
end
