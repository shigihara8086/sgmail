#
# SSH support
#

class SSH
attr :pid
  def initialize(cmd,passwd)
    line = ""
    yesno = "yes"
    @pty = PTY.spawn(cmd)
    @pid = @pty[2]
    print "=== ssh start ===\n" if($SGDEBUG)
    while(1)
      c = @pty[0].getc.chr
      print c if($SGDEBUG)
      line << c
      if(line =~ /\(yes\/no\)/i) then
	@pty[1].puts(yesno)
	line = ""
      end
      if(line =~ /password:/i) then
	@pty[1].puts(passwd)
	line = ""
      end
      if(line =~ /passphrase/i and line =~ /:\s/) then
	@pty[1].puts(passwd)
	line = ""
      end
      if(line =~ /[%$>]/) then
	print "\n" if($SGDEBUG)
	break
      end
      if(c == '\n') then
	line = ""
      end
    end
  end
end
