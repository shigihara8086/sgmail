#
# SGcache class
#

class SGcache
attr :path
attr :rc
  def initialize(rc,path)
    @path = path
    Dir.mkdir(OS.path(@path),0700) if(not test(?e,OS.path(@path)))
    Dir.mkdir(OS.path(@path+"/tmp"),0700) if(not test(?e,OS.path(@path+"/tmp")))
    @rc = rc
    @server = {}
    @prog = nil
    @dlog = nil
    rc['server'].each {|name,param|
      @server[name] = SGserver.new(self,name,param)
    }
  end
  def each_server
    @server.each {|name,server| yield(name,server) }
  end
  def [](name)
    @server[name]
  end
  def callback(pass,stat,prog,dlog)
    @prog = prog
    @stat = stat
    @dlog = dlog
    each_server {|name,server|
      server.callback(pass,stat,prog,dlog)
    }
  end
  def status(msg)
    @stat.call(msg) if(@stat)
  end
  def progress(ratio)
    @prog.call(ratio) if(@prog)
  end
  def sort(type)
    @rc['sort'] = type
  end
  def dialog(msg)
    @dlog.call(msg) if(@dlog)
  end
  def finish
    @server.each {|name,server| server.finish() }
  end
end

#
# SGserver class
#

class SGserver
attr :path
attr :name
attr :box
attr :imap
attr :smtp
attr :rc
attr :recent
attr :selected
attr :server
attr :inbox
attr :outbox
attr :draft
attr :config
attr :trash
attr :conf
attr :template
attr :address
attr :signature
attr :refile
attr :where
attr :folder
include Marshal
  def initialize(root,name,rc)
    @path = root.path+"/"+name
    Dir.mkdir(OS.path(@path),0700) if(not test(?e,OS.path(@path)))
    @root = root
    @rc = rc
    @name = name
    @box = "/"
    @server = self
    @folder = []
    @inbox = nil
    @outbox = nil
    @draft = nil
    @trash = nil
    @config = nil
    @selected = nil
    @recent = nil
    @address = Address.new("")
    @signature = Signature.new("")
    @refile = Refile.new("")
    @template = []
    @conf = {
      'address'=>"",
      'refile'=>"",
      'signature'=>"",
      'send'=>"",
      'reply'=>"",
      'transfer'=>""
    }
    @where = [
      {'nam'=>"direct",'val'=>""},
      {'nam'=>"draft",'val'=>"draft"},
      {'nam'=>"config",'val'=>"config"}]
    @smtp = SMTP.new(rc['smtp'])
    case rc['imap']['mode']
    when "imap"
      @imap = IMAP.new(rc['imap'])
    else
      @imap = POP3.new(rc['imap'],name)
      @where << {'nam'=>"outbox",'val'=>"outbox"}
    end
  end
  def callback(pass,stat,prog,dlog)
    @imap.callback(pass,stat,prog,dlog)
    @smtp.callback(stat,prog,dlog)
  end
  # send mail with via server
  def send(hdr,text,sign,files = nil)
    @smtp.send(hdr,text,sign,files)
  end
  # find folder
  def folder?(name)
    folder = nil
    @folder.each {|f|
      if(f.name == name) then
	folder = f
	break
      end
    }
    folder
  end
  # login
  def login
    begin
      @imap.login()
      @imap.select(@selected) if(@selected)
      yield
      @imap.logout()
    rescue
      raise
    end
  end
  # get config data
  def getconfig
    @template = []
    @config.dbopen {
      @config.lopen
      @config.each_mail {|tag|
	name = tag.header[0]['Subject']
	@conf[name] = @config.body(tag['uid'],0,false)
	case name
	when "address"
	  @address = Address.new(@conf[name])
	when "signature"
	  @signature = Signature.new(@conf[name])
	when "refile"
	  @refile = Refile.new(@conf[name])
	else
	  @template << { 'nam'=>name,'val'=>@conf[name] }
	end
      }
      @config.lclose
    }
  end
  # delete cache
  def delete_cache
  end
  # open folder
  def open
    @root.status("Folder...")
    begin
      @imap.login()
      @imap.listall {|v|
	if(v['val'] == "") then
	  if(folder?("INBOX") == nil) then
	    @folder << SGfolder.new(@root,self,self,v['nam'],v)
	  end
	else
	  par = self
	  ary = v['val'].split("/")
	  left = ""
	  while(ary)
	    nam = ary.shift
	    break if(nam == nil)
	    if(left != "") then left << "/" end
	    left << nam
	    cld = par.folder?(nam)
	    if(cld) then
	      par = cld
	    else
	      val = {'flg'=>"",'nam'=>nam,'val'=>left.dup }
	      folder = SGfolder.new(@root,self,par,val['nam'],val)
	      par.folder << folder
	      par = folder
	    end
	  end
	end
      }
      @imap.logout()
      @folder.each {|v|
	case v.name
	when "INBOX"
	  @inbox = v
	when "draft"
	  @draft = v
	when "outbox"
	  @outbox = v
	when "trash"
	  @trash = v
	when "config"
	  @config = v
	  getconfig()
	end
      }
    rescue
      @folder = []
    end
    @root.status(nil)
  end
  # close folder
  def close
    @folder = []
  end
  # select folder
  def select(box)
    @imap.update(box)
    @selected = box
    @imap.close() if(box == nil)
  end
  # create folder
  def create(name)
    if(folder?(name) == nil) then
      @root.status("Create...")
      @imap.login()
      @imap.create(name)
      delete_cache()
      @imap.logout()
      @root.status(nil)
    end
  end
  # delete child
  def delete_child(name)
    if(folder?(name)) then
      @root.status("Delete...")
      @imap.login()
      begin
	@imap.delete(name)
	delete_cache()
      rescue
      end
      @imap.logout()
      @folder.delete_if {|f| f.name == name }
      @root.status(nil)
    end
  end
  # rename child
  def rename_child(box,src,dst)
    if(folder?(box)) then
      @root.status("Rename...")
      @imap.login()
      @imap.rename(src,dst)
      @imap.logout()
      delete_cache()
      folder = @server
      prev = nil
      dst.split(/\//).each {|path|
	prev = folder
	folder = folder.folder?(path)
	if(folder == nil and prev != nil) then
	  prev.delete_cache()
	  break
	end
      }
      @folder.delete_if {|f| f.name == box }
      @root.status(nil)
    end
  end
  # trash clear
  def trash
    if(@trash) then
      @root.status("Trash...")
      @trash.clear()
      @root.status(nil)
    end
  end
  # biff
  def biff
    if(@imap.success == true and @imap.work == false) then
      @root.status("Biff...")
      @imap.login()
      @recent = @imap.recent()
      @imap.logout()
      @root.status(nil)
    end
  end
  # finish
  def finish
    begin
      @imap.logout()
    rescue
    end
  end
end

#
# SGfolder class
#

class SGfolder
attr :path
attr :name
attr :box
attr :flag
attr :folder
attr :cache
attr :server
attr :par
include Marshal
  def initialize(root,top,par,name,v)
    @path = par.path+"/"+name
    Dir.mkdir(OS.path(@path),0700) if(not test(?e,OS.path(@path)))
    @root = root
    @server = top
    @par = par
    @name = v['nam']
    @box = v['val']
    @flag = v['flg']
    @folder = []
    @opencnt = 0
    @cache = nil
    @list = nil
  end
  # find folder
  def folder?(name)
    folder = nil
    @folder.each {|f|
      if(f.name == name) then
	folder = f
	break
      end
    }
    folder
  end
  # delete cache
  def delete_cache
  end
  # delete child
  def delete_child(name)
    if(folder?(name)) then
      @root.status("Delete...")
      @server.imap.login()
      @server.imap.delete(@box+"/"+name)
      @server.imap.logout()
      delete_cache()
      @folder.delete_if {|f| f.name == name }
      @root.status(nil)
    end
  end
  # rename child
  def rename_child(box,src,dst)
    if(folder?(box)) then
      @root.status("Rename...")
      @server.imap.login()
      @server.imap.rename(src,dst)
      @server.imap.logout()
      delete_cache()
      folder = @server
      prev = nil
      dst.split(/\//).each {|path|
	prev = folder
	folder = folder.folder?(path)
	if(folder == nil and prev != nil) then
	  prev.delete_cache()
	  break
	end
      }
      @folder.delete_if {|f| f.name == box }
      @root.status(nil)
    end
  end
  # create folder
  def create(name)
    if(folder?(name) == nil) then
      @root.status("Create...")
      @server.imap.login()
      @server.imap.create(@box+"/"+name)
      delete_cache()
      @server.imap.logout()
      @root.status(nil)
    end
  end
  # delete folder
  def delete
    @par.delete_child(@name)
    Dir.foreach(OS.path(@path)) {|item|
      if(item !~ /^\.+$/) then
	File.unlink(OS.path(@path+"/"+item))
      end
    }
    Dir.rmdir(@path)
  end
  # rename folder
  def rename(name)
    if(@box != name) then
      Dir.foreach(OS.path(@path)) {|item|
	if(item !~ /^\.+$/) then
	  File.unlink(OS.path(@path+"/"+item))
	end
      }
      Dir.rmdir(@path)
      @par.rename_child(@name,@box,name)
    end
  end
  # cache block
  def dbopen
    if(@opencnt == 0) then
      @cache = DBM.open(OS.path(@path+"/header"),0600)
    end
    @opencnt += 1
    yield
    @opencnt -= 1
    if(@opencnt == 0) then
      @cache.close
      @cache = nil
    end
  end
  # get header data
  def header(uid)
    hdr = nil
    dbopen {
      if(@cache[uid]) then
	hdr = load(@cache[uid])
      else
	@server.login {
	  hdr = @server.imap.header(uid)
	  @cache[uid] = dump(hdr)
	}
      end
    }
    hdr
  end
  # get mail body
  def body(uid,part,prog = true)
    hdr = header(uid)
    code = hdr[part]['Content-Transfer-Encoding']
    file = OS.path(@path+"/"+uid+"."+part.to_s)
    text = ""
    if(test(?e,file)) then
      File.open(file) {|fd|
	fd.each_line {|line|
	  case code
	  when /quoted\-printable/i
	    line.gsub!(/^(.*?)=\r\n/) { $1 }
	    line.gsub!(/=([0-9A-F]{2})/) { ($1.hex).chr }
	    if(line =~ /\r\n/) then
	      text << line.chop+"\n"
	    else
	      text << line
	    end
	  when /base64/i
	    text << line.unpack("m")[0]
	  when /x\-uuencode/i
	    text << line.unpack("u")[0]
	  else
	    text << line.chop+"\n"
	  end
	}
      }
    else
      if(@server.rc['imap']['mode'] == "imap") then
	fd =  File.open(file,"w")
	fd.binmode
      else
	fd = nil
      end
      @server.login {
	@root.status("Recieve...") if(prog)
	@server.imap.body(uid,part) {|line,read,bytes|
	  fd.write(line) if(fd)
	  case code
	  when /quoted\-printable/i
	    line.gsub!(/^(.*?)=\r\n/) { $1 }
	    line.gsub!(/=([0-9A-F]{2})/) { ($1.hex).chr }
	    if(line =~ /\r\n/) then
	      text << line.chop+"\n"
	    else
	      text << line
	    end
	  when /base64/i
	    text << line.unpack("m")[0]
	  when /x\-uuencode/i
	    text << line.unpack("u")[0]
	  else
	    text << line.chop+"\n"
	  end
	  if(bytes > 0) then
	    @root.progress(read.to_f/bytes.to_f) if(prog)
	  end
	}
	@root.status(nil) if(prog)
      }
      fd.close if(fd)
    end
    body = ""
    text.each_line {|line| body << OS.tosys(line.gsub(/\r/,"")) }
    body
  end
  # save mail body
  def save(uid,part,fdst,prog = true)
    hdr = header(uid)
    if(fdst == nil) then
      if(hdr[part]['Content-Disposition'] =~ /filename\s*=\s*\"(.*?)\"/) then
	name = $1
      else
	if(hdr[part]['Content-Type'] =~ /name=\"(.*?)\"/) then
	  name = $1
	else
	  case hdr[part]['Content-Type']
	  when /\/html/
	    name = "Untitled.html"
	  else
	    name = "Untitled.txt"
	  end
	end
      end
      fdst = OS.path(@root.path+"/tmp/"+name)
    end
    code = hdr[part]['Content-Transfer-Encoding']
    file = OS.path(@path+"/"+uid+"."+part.to_s)
    if(test(?e,file)) then
      File.open(file) {|fd|
	File.open(fdst,"w") {|out|
	  out.binmode
	  fd.each_line {|line|
	    case code
	    when /quoted\-printable/i
	      line.gsub!(/^(.*?)=\r\n/) { $1 }
	      line.gsub!(/=([0-9A-F]{2})/) { ($1.hex).chr }
	      if(line =~ /\r\n/) then
		out.write(line.chop+"\n")
	      else
		out.write(line)
	      end
	    when /base64/i
	      out.write(line.unpack("m")[0])
	    when /x\-uuencode/i
	      out.write(line.unpack("u")[0])
	    else
	      out.write(line.chop+"\n")
	    end
	  }
	}
      }
    else
      if(@server.rc['imap']['mode'] == "imap") then
	fd =  File.open(file,"w")
	fd.binmode
      else
	fd = nil
      end
      @server.login {
	File.open(fdst,"w") {|out|
	  out.binmode
	  @root.status("Recieve...") if(prog)
	  @server.imap.body(uid,part) {|line,read,bytes|
	    fd.write(line) if(fd)
	    case code
	    when /quoted\-printable/i
	      line.gsub!(/^(.*?)=\r\n/) { $1 }
	      line.gsub!(/=([0-9A-F]{2})/) { ($1.hex).chr }
	      if(line =~ /\r\n/) then
		out.write(line.chop+"\n")
	      else
		out.write(line)
	      end
	    when /base64/i
	      out.write(line.unpack("m")[0])
	    when /x\-uuencode/i
	      out.write(line.unpack("u")[0])
	    else
	      out.write(line.chop+"\n")
	    end
	    if(bytes > 0) then
	      @root.progress(read.to_f/bytes.to_f) if(prog)
	    end
	  }
	  @root.status(nil) if(prog)
	}
      }
      fd.close if(fd)
    end
    fdst
  end
  # make local date string
  def _date(dstr,tz)
    date = ParseDate::parsedate(dstr)
    if(date[0]) then
      if(date[0] < 100) then
	date[0] += 1900
      end
      if(date[0] > 2037) then date[0] = 2037 end
      if(date[0] < 1970) then date[0] = 1970 end
      gm = Time.gm(date[0],date[1],date[2],date[3],date[4],date[5])
    else
      gm = Time.gm(1970,1,1,0,0,0)
    end
    if(date[6] =~ /^([+|-])(\d\d)(\d\d)$/) then
      if($1 == '+') then
	gm -= ($2.to_i * 3600) + ($3.to_i * 60)
	gm += tz
      else
	gm += ($2.to_i * 3600) + ($3.to_i * 60)
	gm += tz
      end
    end
    jst = gm.strftime("%Y/%m/%d %H:%M:%S")
  end
  # tree walk
  def _walk(list,ind,dst)
    if(list) then
      size = list.size
      list.each_index {|i|
        if(list[i]) then
          item = list[i]
          item['ind'] = ind
          list[i] = nil
	  dst << item
          _walk(item['lnk'],ind + 1,dst)
        end
	@root.progress(i.to_f/size.to_f) if(ind == 0)
      }
    end
  end
  # sorted list
  def lopen
    tz = 0
    if(@root.rc['timezone'] =~ /^([+|-])(\d\d)(\d\d)$/) then
      if($1 == '+') then
        tz += ($2.to_i * 3600) + ($3.to_i * 60)
      else
        tz -= ($2.to_i * 3600) + ($3.to_i * 60)
      end
    end
    dbopen {
      @root.status("List...")
      @server.select(@box)
      @server.login {
	@list = []
	tags = []
	usee = {}
	hmid = {}
	mlpat = $rc2['mlpat']
	@server.imap.unseenlist {|uid,i,size| usee[uid] = true }
	@server.imap.uidlist {|uid,i,size|
	  hdr = header(uid)
	  if(usee[uid]) then
	    see = false
	    body(uid,0,false) if(@server.rc['imap']['mode'] == "imap")
	  else
	    see = true
	  end
	  # JST date
	  if(hdr[0]['Date']) then
	    jst = _date(hdr[0]['Date'],tz)
	  else
	    jst = "???/??/?? ??:??:??"
	  end
	  # message-id
	  mid = hdr[0]['Message-Id']
	  rep = nil
	  if(hdr[0]['In-Reply-To']) then
	    if(hdr[0]['In-Reply-To'] =~ /(<.*?>)/) then
	      rep = $1
	    end
	  else
	    if(hdr[0]['References'] =~ /(<.*?>)/) then
	      rep = $1
	    end
	  end
	  from = hdr[0]['From']
	  if(from =~ /<(.*?)>/) then
	    from = $1.strip
	  else
	    from = from.sub(/(\(.*?\))/,"").strip
	  end
	  tag = {
	    'from'=>from,'subject'=>hdr[0]['Subject'],
	    'uid'=>uid,'see'=>see,'jst'=>jst,
	    'mid'=>mid,'rep'=>rep,'lnk'=>[],
	    'ind'=>0
	  }
	  tags << tag
	  hmid[mid] = tag
	  @root.progress(i.to_f/size.to_f)
	}
	@root.status("Sort...")
	case @root.rc['sort']
	when "msgid"
	  tags.sort! {|a,b| a['jst'] <=> b['jst'] }
	  # make reference link
	  size = tags.size
	  tags.each_index {|i|
	    k = tags[i]['rep']
	    if(hmid[k]) then
	      hmid[k]['lnk'] << tags[i]
	      tags[i] = nil
	    end
	  }
	  _walk(tags,0,@list)
	when "date"
	  tags.sort! {|a,b| a['jst'] <=> b['jst'] }
	  tags.each {|tag| @list << tag }
	when "from"
	  tags.sort! {|a,b|
	    if(a['from'] != b['from']) then
	      a['from'] <=> b['from']
	    else
	      a['jst'] <=> b['jst']
	    end
	  }
	  tags.each {|tag| @list << tag }
	end
      }
      @root.status(nil)
    }
    @list
  end
  # update
  def lupdate
    if(@list) then
      @root.status("Update...")
      dbopen {
	@server.login {
	  usee = {}
	  @server.imap.unseenlist {|uid,i,size| usee[uid] = true }
	  size = @list.size
	  count = 0
	  @list.each {|tag|
	    if(usee[tag['uid']]) then
	      if(tag['see']) then
		@server.imap.seen(tag['uid'])
	      end
	    end
	    count += 1
	    @root.progress(count.to_f/size.to_f)
	  }
	}
      }
      @root.status(nil)
    end
  end
  # close
  def lclose
    if(@list) then
      @root.status("Close...")
      lupdate
      @list = nil
      @root.status(nil)
    end
  end
  # each list
  def each_mail
    if(@list) then
      size = @list.size
      @list.each_index {|i|
	yield(SGtag.new(self,@list[i]))
      }
    end
  end
  # move mail
  def move(tag)
    dbopen {
      @server.imap.copy(tag['uid'],tag['mov'])
      @server.imap.remove(tag['uid'])
      @cache[tag['uid']] = nil
    }
  end
  # expunge
  def expunge
    @server.imap.expunge()
  end
  # clear folder
  def clear
    @root.status("Clear...")
    sel = @server.selected
    @server.select(@box)
    dbopen {
      @server.login {
	@server.imap.uidlist {|uid,i,size|
	  @server.imap.remove(uid)
	  @cache[uid] = nil
	  @root.progress(i.to_f/size.to_f)
	}
	@server.imap.expunge()
      }
    }
    @server.select(sel)
    @root.status(nil)
  end
  # find
  def find(kind,text)
    list = []
    @root.status("Find...")
    sel = @server.selected
    @server.select(@box)
    dbopen {
      if(@server.rc['imap']['mode'] == "imap") then
	@server.login {
	  if(kind == "BODY") then
	    list = @server.imap.search("BODY",text)
	  else
	    list = @server.imap.search("HEADER #{kind}",text)
	  end
	}
      else
	if(kind == "BODY") then
	  @list.each {|tag|
	    mail = body(tag['uid'],0)
	    list << tag['uid'] if(mail =~ /#{text}/i)
	  }
	else
	  @list.each {|tag|
	    hdr = header(tag['uid'])
	    case kind
	    when "SUBJECT"
	      list << tag['uid'] if(hdr[0]['Subject'] =~ /#{text}/i)
	    when "FROM"
	      list << tag['uid'] if(hdr[0]['From'] =~ /#{text}/i)
	    end
	  }
	end
      end
    }
    @server.select(sel)
    @root.status(nil)
    list
  end
  # remove
  def remove(uid)
    @root.status("Remove...")
    sel = @server.selected
    @server.select(@box)
    dbopen {
      @server.login {
	@server.imap.remove(uid)
	@server.imap.expunge()
      }
    }
    @server.select(sel)
    @root.status(nil)
  end
  # store message
  def store(hdr,body,sign,files = nil)
    @root.status("Store...")
    dbopen {
      @server.login {
	rfc = ""
	@server.smtp.rfc822(hdr,body,sign,files) {|line|
	  rfc << line + "\r\n"
	}
	@server.imap.store(@box,rfc)
      }
    }
    @root.status(nil)
  end
end

#
# SGtag class
#

class SGtag
attr :folder
  def initialize(fld,tag)
    @folder = fld
    @tag = tag
  end
  # get tag instance
  def [](name)
    @tag[name]
  end
  # set tag instance
  def []=(name,val)
    @tag[name] = val
  end
  # get header
  def header
    @folder.header(@tag['uid'])
  end
  # get body text
  def body(part)
    @folder.body(@tag['uid'],part)
  end
  # get body text
  def save(part,file = nil)
    @folder.save(@tag['uid'],part,file)
  end
  # move mail
  def move
    @folder.move(@tag)
  end
end
