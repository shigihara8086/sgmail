#
# resource handling class
#

class RC
  def initialize(rc,tmp = nil)
    @err = 0
    @lines = 0
    if(tmp) then
      file = OS::path(tmp)
      if(test(?e,file)) then
	@tmp = {}
	fh = File.open(file)
	read(fh,@tmp)
	fh.close
      end
      file = OS::path(rc)
      if(test(?e,file)) then
	@rc = {}
	@tmp.each {|k,v|
	  if(k != "*") then
	    if(v.kind_of?(Hash) == false) then
	      @rc[k] = v
	    end
	  end
	}
	fh = File.open(file)
	checkread(fh,@rc,@tmp)
	fh.close
	exit if(@err > 0)
      end
    else
      file = OS::path(rc)
      if(test(?e,file)) then
	@rc = {}
	fh = File.open(file)
	read(fh,@rc)
	fh.close
      end
    end
  end
  ## read resource tree
  def read(fh,tree)
    begin
      @lines = 0
      while(line = fh.gets)
	@lines += 1
	line.chop!
	line.sub!(/^\s*/,"")
	line.sub!(/\s*$/,"")
	next if(line =~ /^#.*$/)
	next if(line =~ /^$/)
	case line
	when /^(.*?):\s+(.*)/
	  tree[$1] = eval($2)
	when /^(.*?)\s*\{/
	  tree[$1] = {}
	  read(fh,tree[$1])
	when /^}$/
	  return
	end
      end
    rescue
      print "Resource Error:(#{@lines})#{$!}\n"
      @err += 1
    end
  end
  ## read resource tree with check
  def checkread(fh,tree,tmp)
    begin
      @lines = 0
      while(line = fh.gets)
	@lines += 1
	line.chop!
	line.sub!(/^\s*/,"")
	line.sub!(/\s*$/,"")
	next if(line =~ /^#.*$/)
	next if(line =~ /^$/)
	case line
	when /^(.*?):\s+(.*)/
	  if(tmp[$1]) then
	    tree[$1] = eval($2)
	    if(tmp[$1].kind_of?(Array)) then
	      if(tmp[$1].include?(tree[$1]) == false) then
		print "Resource Error:(#{@lines})Invalid #{$1}:#{tree[$1]}.\n"
		@err += 1
	      end
	    end
	  elsif(tmp['*']) then
	    tree[$1] = eval($2)
	    if(tmp['*'].kind_of?(Array)) then
	      if(tmp['*'].include?(tree[$1]) == false) then
		print "Resource Error:(#{@lines})Invalid #{$1}:#{tree[$1]}.\n"
		@err += 1
	      end
	    end
	  else
	    print "Resource Error:(#{@lines})Invalid #{$1}:#{tree[$1]}.\n"
	    @err += 1
	  end
	when /^(.*?)\s*\{/
	  if(tmp[$1]) then
	    tree[$1] = {}
	    tmp[$1].each {|k,v|
	      if(k != "*") then
		if(v.kind_of?(Hash) == false) then
		  if(v.kind_of?(Array)) then
		    tree[$1][k] = v[0]
		  else
		    tree[$1][k] = v
		  end
		end
	      end
	    }
	    checkread(fh,tree[$1],tmp[$1])
	  elsif(tmp['*']) then
	    tree[$1] = {}
	    tmp['*'].each {|k,v|
	      if(k != "*") then
		if(v.kind_of?(Hash) == false) then
		  if(v.kind_of?(Array)) then
		    tree[$1][k] = v[0]
		  else
		    tree[$1][k] = v
		  end
		end
	      end
	    }
	    checkread(fh,tree[$1],tmp['*'])
	  else
	    print "Resource Error:(#{@lines})Invalid #{$1}{}.\n"
	    @err += 1
	  end
	when /^}$/
	  return
	end
      end
    rescue
      print "Resource Error:(#{@lines})#{$!}\n"
      @err += 1
    end
  end
  # each
  def each
    @rc.each {|k,v| yield(k,v) }
  end
  # get resource
  def [](name)
    @rc[name]
  end
  # set resource
  def []=(name,value)
    @rc[name] = value
  end
end
