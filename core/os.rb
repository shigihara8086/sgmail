#
# detect oparating system
#

class OS
  # replace file path separator
  def OS.path(p)
    p
  end
  # convert kanji code(jis->internal)
  def OS.tosys(s)
    case SYS
    when "win"
      s = Kconv::tosjis(s)
    else
      s = Kconv::toeuc(s)
    end
    s
  end
  # convert kanji code(internal->jis)
  def OS.tojis(s)
    return Kconv::tojis(s)
  end
  # convert to UTF8
  def OS.toutf8(s)
    case $rc['gui']
    when "tk"
      case Tk::TK_VERSION
      when /7\.6/,/^8\.0/
	return s
      else
	case SYS
	when "win"
	  s = Tk::toUTF8(s,'shiftjis')
	else
	  s = Tk::toUTF8(s,'euc-jp')
	end
	s
      end
    else
      s
    end
  end
  # convert from UTF8
  def OS.fromutf8(s)
    case $rc['gui']
    when "tk"
      case Tk::TK_VERSION
      when /7\.6/,/^8\.0/
	return s
      else
	case SYS
	when "win"
	  s = Tk::fromUTF8(s,'shiftjis')
	else
	  s = Tk::fromUTF8(s,'euc-jp')
	end
	s
      end
    else
      s
    end
  end
  # fork process
  def OS.fork(proc,*arg)
    PTY.protect_signal {
      super() {
	pid = super() {
	  exec(proc,*arg)
	}
	exit!(0)
      }
      Process.wait
    }
  end
  # fork netscape
  def OS.netscape(cmd,url)
    case SYS
    when "unix"
      mozilla = false
      PTY.protect_signal {
	ps = IO.popen("ps x")
	ps.each_line {|line|
	  if(line =~ /netscape/) then
	    mozilla = true
	  end
	}
	ps.close
      }
      if(mozilla) then
        OS.fork("#{cmd}","-remote","openURL(#{url})")
      else
        OS.fork("#{cmd}","#{url}")
      end
    when "win"
      ie = WIN32OLE.new("InternetExplorer.Application")
      ie.visible = TRUE
      ie.navigate({'url'=>url})
    end
  end
  # initialize
  $prog_path = $0
  while(File.symlink?($prog_path))
    $prog_path = File.readlink($prog_path)
  end
  case RUBY_PLATFORM
  when /cygwin/
    $KCODE = "S"
    if(ENV['CMDLINE']) then
      require 'win32ole'
      SYS = "win"
      cmd,arg = ENV['CMDLINE'].split(/ +/)
      arg.gsub!(/\x5c/,"/")
      LIB = File.expand_path(File.dirname(arg))
      PRG = File.basename(arg,".rb")
      HOME = LIB
      CACHE = "#{LIB}"
      RC = "#{LIB}/#{PRG}rc.txt"
    else
      require 'win32ole'
      SYS = "win"
      LIB = ENV['!C:']
      PRG = "sgmail"
      HOME = LIB
      CACHE = ENV['USERPROFILE']+"\\My Documents\\sgmail"
      if(not test(?e,CACHE)) then Dir.mkdir(CACHE) end
      RC = CACHE+"\\#{PRG}rc.txt"
    end
  else
    $KCODE = "E"
    SYS = "unix"
    progpath = File.expand_path(File.dirname($prog_path))
    if not FileTest.directory?(progpath + '/tk') and
          not FileTest.directory?(progpath + '/gtk')
      LIB = '/usr/share/sgmail'
    else
      LIB = progpath
    end
    PRG = File.basename($prog_path,".rb")
    HOME = "#{ENV['HOME']}"
    CACHE = "#{ENV['HOME']}/.#{PRG}"
    RC = "#{ENV['HOME']}/.#{PRG}rc"
  end
end
