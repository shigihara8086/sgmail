#
# file type check
#

module Binary
  def binary?(string)
    return true if(string.count("\x00") > 0)
    check = string[0,512]
    check.size < 10 * check.count("\x00-\x07\x0b\x0e-\x1a\x1c-\x1f")
  end
  module_function :binary?
end
