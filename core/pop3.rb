#
# POP3 IMAP emulation class
#

class POP3
attr :success
include Marshal
  def initialize(rc,name)
    @rc = rc
    @pass = nil
    @stat = nil
    @prog = nil
    @dlog = nil
    @pop3 = nil
    @user = rc['user']
    @passwd = rc['passwd']
    @name = name
    @selected = nil
    @logincnt = 0
    @success = false
    @uidl = {}
    @mhlist = nil
    if(@rc['path'] != "") then
      @mhdir = OS::HOME+"/"+@rc['path']
      @inbox = @mhdir+"/inbox"
      Dir.mkdir(OS.path(@mhdir),0700) if(not test(?e,OS.path(@mhdir)))
      Dir.mkdir(OS.path(@inbox),0700) if(not test(?e,OS.path(@inbox)))
    else
      @mhdir = OS::CACHE+"/"+@name
      @inbox = @mhdir+"/INBOX"
      Dir.mkdir(OS.path(@mhdir),0700) if(not test(?e,OS.path(@mhdir)))
      Dir.mkdir(OS.path(@inbox),0700) if(not test(?e,OS.path(@inbox)))
    end
    file = @mhdir+"/outbox"
    Dir.mkdir(OS.path(file),0700) if(not test(?e,OS.path(file)))
    file = @mhdir+"/draft"
    Dir.mkdir(OS.path(file),0700) if(not test(?e,OS.path(file)))
    file = @mhdir+"/config"
    Dir.mkdir(OS.path(file),0700) if(not test(?e,OS.path(file)))
    file = @mhdir+"/trash"
    Dir.mkdir(OS.path(file),0700) if(not test(?e,OS.path(file)))
    file = OS.path(OS::CACHE+"/"+@name+"/uidl.che")
    if(test(?e,file)) then
      File.open(file) {|fd| @uidl = Marshal.load(fd) }
    end
  end
  # set callback
  def callback(pass,stat,prog,dlog)
    @pass = pass
    @stat = stat
    @prog = prog
    @dlog = dlog
  end
  # work?
  def work?
    if(@pop3) then
      @pop3.work
    else
      false
    end
  end
  # pop3 login
  def _login
    if(@logincnt == 0) then
      ok = false
      if(@user == "" || @passwd == "") then
	@user,@passwd = @pass.call(@user)
      end
      if(@passwd) then
	if(@user != "" && @passwd != "") then
	  begin
	    @pop3 = POP3socket.new(@rc['server'],@user,@passwd,@rc['port'],@rc['mode'])
	    @pop3.login
	    @success = true
	    ok = true
	  rescue
	    @passwd = ""
	    @dlog.call("login_error")
	  end
	else
	  @dlog.call("login_error")
	  raise
	end
      else
	@passwd = ""
	raise
      end
    else
      ok = true
    end
    if(ok) then
      @logincnt += 1
    end
  end
  # pop3 logout
  def _logout
    @logincnt -= 1
    if(@logincnt == 0) then
      @pop3.logout
    end
  end
  # get real path
  def _path(box)
    if(box == "") then
      dir = @inbox
    else
      dir = @mhdir+"/"+box
    end
    dir
  end
  # get last mail number
  def _next(box)
    last = "0"
    dir = _path(box)
    Dir.foreach(OS.path(dir)) {|item|
      if(item =~ /^\d+$/) then
	if(last.to_i < item.to_i) then
	  last = item
	end
      end
    }
    last.succ
  end
  # recv mail
  def _retr(no,file)
    lines = 0
    File.open(file,"w") {|fd|
      @pop3.retr(no) {|line|
	line.chop!
	fd.puts(line) if(lines != 0)
	lines += 1
      }
      fd.chmod(0600)
    }
  end
  # delete mail
  def _dele(no)
    @pop3.dele(no)
  end
  # delete mailbox
  def _delete(box)
    dir = _path(box)
    Dir.foreach(OS.path(dir)) {|item|
      if(item !~ /^\.+$/) then
	if(test(?d,OS.path(dir+"/"+item))) then
	  _delete(box+"/"+item)
	else
	  File.unlink(OS.path(dir+"/"+item))
	end
      end
    }
    Dir.rmdir(dir)
  end
  # get seen flag
  def _seen?(box,uid)
    seen = {}
    dir = _path(box)
    file = OS.path(dir+"/.sgmail")
    if(test(?e,file)) then
      File.open(file) {|fd| seen = Marshal.load(fd) }
    end
    seen[uid]
  end
  # set seen flag
  def _seen(box,uid)
    seen = {}
    dir = _path(box)
    file = OS.path(dir+"/.sgmail")
    if(test(?e,file)) then
      File.open(file) {|fd| seen = Marshal.load(fd) }
    end
    seen[uid] = true
    File.open(file,"w") {|fd| Marshal.dump(seen,fd); fd.chmod(0600) }
  end
  # reset seen flag
  def _unseen(box,uid)
    seen = {}
    dir = _path(box)
    file = OS.path(dir+"/.sgmail")
    if(test(?e,file)) then
      File.open(file) {|fd| seen = Marshal.load(fd) }
    end
    seen.delete(uid)
    File.open(file,"w") {|fd| Marshal.dump(seen,fd); fd.chmod(0600) }
  end
  # login
  def login
  end
  # logout
  def logout
  end
  # expunge
  def expunge
  end
  # get recent count
  def recent
    list = {}
    cnt = 0
    case @rc['mode']
    when "pop","apop"
      begin
	@stat.call("Login...")
	_login()
	@pop3.uidl {|line|
	  line.chop!
	  if(line !~ /^\+OK/) then
	    no,uid = line.split(/\s/)
	    list[uid] = no
	  end
	}
	_logout()
	size = list.size
	list.each {|k,v|
	  if(@uidl[k] == nil) then
	    cnt += 1
	  end
	}
	@stat.call()
      rescue
      end
    end
    cnt
  end
  # get mailbox tree
  def listall_sub(dir)
    Dir.foreach(OS.path(@mhdir+"/"+dir)) {|item|
      if(item !~ /^\.+$/) then
	if(test(?d,OS.path(@mhdir+"/"+dir+"/"+item))) then
	  yield({ 'flg'=>"",'nam'=>item,'val'=>dir+"/"+item })
	  listall_sub(dir+"/"+item) {|val| yield(val) }
	end
      end
    }
  end
  def listall
    yield({ 'flg'=>"\\NoInferiors",'nam'=>"INBOX",'val'=>"" })
    Dir.foreach(OS.path(@mhdir)) {|item|
      if(item !~ /^\.+$/) then
	if(test(?d,OS.path(@mhdir+"/"+item))) then
	  if(item !~ /^inbox$/i) then
	    yield({ 'flg'=>"",'nam'=>item,'val'=>item })
	    listall_sub(item) {|val| yield(val) }
	  end
	end
      end
    }
  end
  # get mailbox tree
  def mblist(box)
    list = []
    if(box == "") then
      dir = @mhdir
      list << { 'flg'=>'I','nam'=>'INBOX','val'=>"" }
    else
      dir = @mhdir+"/"+box
    end
    Dir.foreach(OS.path(dir)) {|item|
      if(item !~ /^\.+$/) then
	if(test(?d,OS.path(dir+"/"+item))) then
	  if(box != "" or item !~ /^inbox$/i) then
	    if(box != "") then
	      val = box+"/"+item
	    else
	      val = item
	    end
	    list << { 'flg'=>'F','nam'=>item,'val'=>val }
	  end
	end
      end
    }
    list
  end
  # update mhdir
  def update(box)
    if(box == "") then
      dir = _path("")
      list = {}
      case @rc['mode']
      when "pop","apop"
	begin
	  @stat.call("Login...")
	  _login()
	  @pop3.uidl {|line|
	    line.chop!
	    if(line !~ /^\+OK/) then
	      no,uid = line.split(/\s/)
	      list[uid] = no
	    end
	  }
	  last = _next("")
	  @stat.call("Update...")
	  cnt = 0
	  size = list.size
	  list.each {|k,v|
	    if(@uidl[k] == nil) then
	      _retr(v,dir+"/"+last)
	      last.succ!
	    end
	    if(@rc['keep'] == false) then
	      _dele(v)
	    end
	    @prog.call(cnt.to_f/size.to_f)
	    cnt += 1
	  }
	  @stat.call()
	  _logout()
	  @uidl = list
	  file = OS.path(OS::CACHE+"/"+@name+"/uidl.che")
	  File.open(file,"w") {|fd| Marshal.dump(@uidl,fd); fd.chmod(0600) }
	rescue
	end
      end
    end
    @selected = box
  end
  # select mail box
  def select(box)
    @selected = box
  end
  # get UID list
  def uidlist
    list = []
    dir = _path(@selected)
    Dir.foreach(OS.path(dir)) {|item|
      if(item =~ /^\d+$/) then
	if(test(?f,OS.path(dir+"/"+item))) then
	  list << item
	end
      end
    }
    if(list && iterator?) then
      size = list.size
      list.each_index {|i|
	yield(list[i],i,size)
      }
      nil
    else
      list
    end
  end
  # get UNSEEN list
  def unseenlist
    seen = {}
    unseen = []
    list = []
    dir = _path(@selected)
    Dir.foreach(OS.path(dir)) {|item|
      if(item =~ /^\d+$/) then
	if(test(?f,OS.path(dir+"/"+item))) then
	  list << item
	end
      end
    }
    file = OS.path(dir+"/.sgmail")
    if(test(?e,file)) then
      File.open(file) {|fd| seen = Marshal.load(fd) }
    end
    list.each {|v|
      if(seen[v] != true) then
	unseen << v
      end
    }
    if(unseen && iterator?) then
      size = unseen.size
      unseen.each_index {|i|
	yield(unseen[i],i,size)
      }
      nil
    else
      unseen
    end
  end
  # set seen flag
  def seen(uid)
    _seen(@selected,uid)
  end
  # create folder
  def create(box)
    if(box != "") then
      dir = _path(box)
      Dir.mkdir(OS.path(dir),0700)
    end
  end
  # delete folder
  def delete(box)
    if(box != "") then
      _delete(box)
    end
  end
  # rename folder
  def rename(src,dst)
    create(dst)
    sel = @selected
    select(src)
    uidlist {|uid,i,size|
      copy(uid,dst)
      remove(uid)
    }
    select(sel)
    delete(src)
  end
  # set delete flag
  def remove(uid)
    dir = _path(@selected)
    if(test(?e,OS.path(dir+"/"+uid))) then
      File.unlink(OS.path(dir+"/"+uid))
      _unseen(@selected,uid)
    end
  end
  # copy message
  def copy(uid,to)
    src = _path(@selected)
    dst = _path(to)
    nxt = _next(to)
    File.open(OS.path(src+"/"+uid)) {|i|
      File.open(OS.path(dst+"/"+nxt),"w") {|o|
	i.each_line {|line|
	  o.write(line)
	}
	o.chmod(0600)
      }
    }
    see = _seen?(@selected,uid)
    if(see) then
      _seen(to,nxt)
    end
  end
  # get mail header
  def header(uid)
    mime = []
    mime[0] = {}
    mime[0]['Received'] = []
    part = 0
    received = -1
    id = nil
    mode = "header"
    boundary = nil
    dir = _path(@selected)
    file = OS.path(dir+"/"+uid)
    if(test(?f,file)) then
      File.open(file) {|fd|
	fd.each_line {|line|
	  line.chop!
	  case mode
	  when "header"
	    case line
	    when /^([\w-]+):\s(.*)$/
	      id = $1
	      str = $2
	      id = "Subject" if(id =~ /^Subject$/i)
	      id = "From" if(id =~ /^From$/i)
	      id = "To" if(id =~ /^To$/i)
	      id = "Date" if(id =~ /^Date$/i)
	      id = "Reply-To" if(id =~ /^Reply-To$/i)
	      id = "In-Reply-To" if(id =~ /^In-Reply-To$/i)
	      id = "Message-Id" if(id =~ /^Message-Id$/i)
	      id = "References" if(id =~ /^References$/i)
	      id = "Content-Type" if(id =~ /^Content-Type$/i)
	      id = "Content-Transfer-Encoding" if(id =~ /^Content-Transfer-Encoding$/i)
	      id = "Content-Disposition" if(id =~ /^Content-Disposition$/i)
	      id = "Recieved" if(id =~ /^Recieved$/i)
	      if(id == "Received") then
		received += 1
		mime[part][id][received] = str
	      else
		mime[part][id] = OS.tosys(str).gsub(/\t/," ")
	      end
	    when /^\s+(.*)$/
	      if(id == "Received") then
		mime[part][id][received] << " "+$1
	      else
		mime[part][id] << OS.tosys($1).gsub(/\t/," ")
	      end
	    when /^$/
	      if(mime[0]['Content-Type'] =~ /boundary=\"(.*?)\"/i or
		 mime[0]['Content-Type'] =~ /boundary=(.*)$/i) then
		boundary = Regexp.quote("--"+$1)
		if(part == 0) then
		  mode = "skip-1st"
		else
		  mode = "body"
		end
		part += 1
		mime[part] = {}
	      else
		break
	      end
	    end
	  when "skip-1st"
	    if(boundary) then
	      if(line =~ /^#{boundary}$/) then
		mode = "skip-2nd"
	      end
	    end
	  when "skip-2nd"
	    if(boundary) then
	      if(line =~ /^#{boundary}$/) then
		mode = "header"
	      end
	    end
	  when "body"
	    if(boundary) then
	      if(line =~ /^#{boundary}$/) then
		mode = "header"
	      end
	    end
	  end
	}
      }
    end
    mime[0]['Subject'] = ""    if(mime[0]['Subject'] == nil)
    mime[0]['From'] = ""       if(mime[0]['From'] == nil)
    mime[0]['To'] = ""         if(mime[0]['To'] == nil)
    mime[0]['Date'] = ""       if(mime[0]['Date'] == nil)
    mime[0]['Message-Id'] = "" if(mime[0]['Message-Id'] == nil)
    mime[0]['References'] = "" if(mime[0]['References'] == nil)
    mime
  end
  # get mail body
  def body(uid,part)
    ctype = ""
    boundary = nil
    bcount = 0
    mode = "header"
    id = nil
    dir = _path(@selected)
    file = OS.path(dir+"/"+uid)
    if(test(?f,file)) then
      left = bytes = File.size(file)
      File.open(file) {|fd|
	fd.each_line {|line|
	  text = line.chop
	  left -= line.size
	  case mode
	  when "header"
	    case text
	    when /^([\w-]+):\s(.*)$/
	      id = $1.downcase
	      str = $2
	      if(id == "content-type") then
		ctype = OS.tosys(str).gsub(/\t/," ")
	      end
	    when /^\s+(.*)$/
	      if(id == "content-type") then
		ctype << OS.tosys($1).gsub(/\t/," ")
	      end
	    when /^$/
	      if(ctype =~ /boundary=\"(.*?)\"/i or
		 ctype =~ /boundary=(.*)$/i) then
		boundary = Regexp.quote("--"+$1)
		mode = "mime-hunt"
	      else
		mode = "plane"
	      end
	    end
	  when "mime-hunt"
	    if(text =~ /^#{boundary}$/) then
	      if(bcount == part) then
		mode = "mime-header"
	      else
		bcount += 1
	      end
	    end
	  when "mime-header"
	    if(text == "") then
	      mode = "mime-body"
	    end
	  when "mime-body"
	    if(text =~ /^#{boundary}$/ or
	       text =~ /^#{boundary}--$/) then
	      break
	    else
	      yield(line,bytes-left,bytes) if(iterator?)
	    end
	  when "plane"
	    yield(line,bytes-left,bytes) if(iterator?)
	  end
	}
      }
    end
  end
  # store message
  def store(box,text)
    dir = _path(box)
    last = _next(box)
    File.open(OS.path(dir+"/"+last),"w") {|fd|
      text.each_line {|line|
	fd.write(line)
      }
      fd.chmod(0600)
    }
  end
end
