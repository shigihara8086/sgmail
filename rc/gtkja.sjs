# style <name> [= <name>]
# {
#   <option>
# }
#
# widget <widget_set> style <style_name>
# widget_class <widget_class_set> style <style_name>

#style "default" {
#  fontset = "-misc-fixed-medium-r-normal--14-*-*-*-*-*-*-*,-misc-fixed-medium-r-normal--14-*-*-*-*-*-*"
#}
style "default" {
  font = "-unknown-*-*-r-*-*-*-90-*-*-*-*-windows-shiftjis"
}

widget_class "*" style "default"
