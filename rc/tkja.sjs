#
# sgmail system resource
#

mbox_width:	17				# mail box width
list_height:	10				# subject list lines
text_height:	25				# mail viewer lines
text_width:	80				# mail viewer columns
send_height:    25                              # mail editor lines
send_width:     80                              # mail editor columns
conf_height:    25                              # config editor lines
conf_width:     80                              # config editor columns
header_width:	60				# header width
header_height:	4				# header view lines
from_width:	10				# from list columns
line_width:	74				# send line limit
scbar_width:	12				# scroll bar width
pbar_height:	4				# progress bar height
menu_font:	["�l�r\\ �S�V�b�N","�l�r\\ �S�V�b�N",{'size'=>"10"}]
list_font:	["�l�r\\ �S�V�b�N","�l�r\\ �S�V�b�N",{'size'=>"10"}]
text_font:	["�l�r\\ �S�V�b�N","�l�r\\ �S�V�b�N",{'size'=>"10"}]
pbar_color:	["darkblue","darkblue"]		# progress bar color
mbox_color:	["black","white"]		# mailbox list color
from_color:	["black","white"]		# from list color
subject_color:	["black","white"]		# subject list color
header_color:	["black","white"]		# header view color
viewer_color:	["black","white"]		# mail viewer color
mime_color:	["black","lightgray"]		# MIME list color
send_color:	["black","white"]		# mail editor color
config_color:	["black","white"]		# config editor color
reply_color:	["brown"]			# reply line color
url_color:	["blue"]			# URL color
mailto_color:	["red"]				# mail address color
search_color:	["yellow","black"]		# searched string color
entry_color:	["black","white"]		# entry box color
button_color:	["black","lightgray"]		# button color
menu_color:	["black","lightgray"]		# menu color
label_color:	["black","lightgray"]		# label color
frame_color:	["black","lightgray"]		# frame color
select_color:	["black","gray"]		# select color
biff_nomail:	["black","lightgray"]		# biff normal color
biff_recent:	["white","blue"]		# resent exist color
biff_unseen:	["black","yellow"]		# unseen exist color

#
# end
#
