#
# mail boxes tree list 
#

class MboxTree
attr :widget
attr :server
  def initialize(top)
    @top = top
    @server = nil
    @folder = nil
    @parent = nil
    @widget = Gtk::VBox.new(false,0)
    @widget.show
    # tool bar
    tbar = Gtk::Toolbar.new(Gtk::ORIENTATION_HORIZONTAL,Gtk::TOOLBAR_ICONS)
    tbar.show
    tbar.set_button_relief(Gtk::RELIEF_NONE)
    tbar.set_space_style(Gtk::Toolbar::SPACE_LINE)
    tbar.set_space_size(5)
    tbar.append_item(
      nil,$rc2['button']['send'],nil,
      Gtklib.pixmap("send.xpm",top.window),nil) { send }
    tbar.append_space()
    tbar.append_item(
      nil,$rc2['button']['create'],nil,
      Gtklib.pixmap("mbnew.xpm",top.window),nil) { create }
    tbar.append_item(
      nil,$rc2['button']['delete'],nil,
      Gtklib.pixmap("mbdelete.xpm",top.window),nil) { delete }
    tbar.append_item(
      nil,$rc2['button']['rename'],nil,
      Gtklib.pixmap("mbrename.xpm",top.window),nil) { rename }
    tbar.append_item(
      nil,$rc2['button']['trash'],nil,
      Gtklib.pixmap("mbtrash.xpm",top.window),nil) { trash }
    @widget.pack_start(tbar,false,false,0)
    @widget.pack_start(Gtk::HSeparator.new,false,false,0)
    # server selecter
    tbar = Gtk::Toolbar.new(Gtk::ORIENTATION_HORIZONTAL,Gtk::TOOLBAR_ICONS)
    tbar.show
    tbar.set_button_relief(Gtk::RELIEF_NONE)
    tbar.set_space_size(5)
    # set servers
    @widget.pack_start(tbar,false,false,0)
    # tree box
    scroll = Gtk::ScrolledWindow.new(nil,nil)
    @tree = Gtk::Tree.new
    @tree.set_view_mode(Gtk::Tree::VIEW_ITEM  | Gtk::Tree::VIEW_LINE)
    @tree.set_selection_mode(1)
    scroll.add_with_viewport(@tree)
    $CACHE.each_server {|name,folder|
      item = Gtk::TreeItem.new(name)
      @tree.append(item)
      tree = Gtk::Tree.new
      tree.set_view_mode(Gtk::Tree::VIEW_ITEM  | Gtk::Tree::VIEW_LINE)
      tree.set_selection_mode(1)
      item.set_subtree(tree)
      item.signal_connect("expand",tree,folder) {|item,tree,folder|
	@server = folder.server
	folder.open()
	if(folder.folder != []) then
	  maketree(tree,folder.folder)
	  tree.show_all
	else
	  item.collapse()
	  @server = nil
	end
      }
      item.signal_connect("select",folder) {|item,folder|
	@parent = folder if(@server)
      }
    }
    scroll.set_usize(150,0)
    scroll.show_all
    @widget.pack_start(scroll,true,true,0)
  end
  # maketree
  def maketree(tree,list)
    first = nil
    tree.foreach {|item|
      if(first == nil) then
	first = item
      else
	tree.remove(item)
      end
    }
    list.each {|folder|
      item = Gtk::TreeItem.new(folder.name)
      tree.append(item)
      if(folder.folder != []) then
	subtree = Gtk::Tree.new
	subtree.set_view_mode(Gtk::Tree::VIEW_ITEM|Gtk::Tree::VIEW_LINE)
	subtree.set_selection_mode(1)
	item.set_subtree(subtree)
	item.signal_connect("expand",tree,folder) {|item,tree,folder|
	  subtree.show_all
	}
	item.signal_connect("select",folder) {|item,folder|
	  @parent = nil
	  @folder = folder
	  @server = folder.server
	  begin
	    $main.widget.wlist.open(folder)
	  rescue
	  end
	}
	maketree(subtree,folder.folder)
      else
	item.signal_connect("select",folder) {|item,folder|
	  @folder = folder
	  @server = folder.server
	  @parent = nil
	  $main.widget.wlist.open(folder)
	}
      end
    }
    if(first) then
      tree.remove(first)
    end
  end
  # send new mail
  def send
    if(@server) then
	send = Send.new("Send",@server,nil,nil)
    else
      SGDialog.new(
	$rc2['select_server_error'][0],
	$rc2['select_server_error'][1])
    end
  end
  # create new folder
  def create
    if(@parent) then
      if(@parent.box != "") then
	dlg = MboxCreateDialog.new()
	if(dlg.value && dlg.value != "") then
	  if(@parent.create(dlg.value)) then
	  end
	end
      end
    end
  end
  # delete folder
  def delete
    if(@folder) then
      if(@folder.box != "/" && @folder.box != "") then
        dlg = SGDialog.new(
	  "delete_correct",["OK","Cancel"],@folder.name)
	if(dlg.value == "OK") then
	  $main.widget.wlist.close
	  if(@folder.delete()) then
	  end
	end
      end
    end
  end
  # rename folder
  def rename
    if(@folder) then
      dlg = MboxRenameDialog.new(@folder.box)
      if(dlg.value && dlg.value != "") then
	$main.widget.wlist.close
	if(@folder.rename(dlg.value)) then
	end
      end
    end
  end
  # trash clear
  def trash
    if(@server) then
      dlg = SGDialog.new("trash_correct",["OK","Cancel"])
      if(dlg.value == "OK") then
	$main.widget.wlist.close
	@server.trash()
      end
    end
  end
end
