#
# mail list
#

class MailList
attr :widget
  def initialize(top,window)
    @top = top
    @now = nil
    @tags = nil
    @folder = nil
    @unseen = 0
    @recent = 0
    @target = nil
    @search_target = nil
    @search_list = []
    @search_string = nil
    @moveto = "trash"
    @seen_pix,@seen_msk = Gdk::Pixmap::create_from_xpm(
      window.window,nil,OS::LIB+"/gtk/images/seen.xpm")
    @unseen_pix,@unseen_msk = Gdk::Pixmap::create_from_xpm(
      window.window,nil,OS::LIB+"/gtk/images/unseen.xpm")
    @widget = Gtk::VBox.new(false,0)
    @widget.show
   # tool bar
    tbox = Gtk::HBox.new(false,0)
    tbox.show
    @widget.pack_start(tbox,false,false,0)
    @widget.pack_start(Gtk::HSeparator.new,false,false,0)
    # left tool bar
    tbar = Gtk::Toolbar.new(Gtk::ORIENTATION_HORIZONTAL,Gtk::TOOLBAR_ICONS)
    tbar.show
    tbar.set_button_relief(Gtk::RELIEF_NONE)
    tbar.set_space_style(Gtk::Toolbar::SPACE_LINE)
    tbar.set_space_size(5)
    tbar.append_item(
      nil,$rc2['button']['refile'],nil,
      Gtklib.pixmap("mark.xpm",window.window),nil) {
      refile
    }
    tbar.append_item(
      nil,$rc2['button']['fource'],nil,
      Gtklib.pixmap("fmark.xpm",window.window),nil) {
      refile_fource
    }
    tbar.append_item(
      nil,$rc2['button']['unrefile'],nil,
      Gtklib.pixmap("unmark.xpm",window.window),nil) {
      refile_undo
    }
    tbar.append_item(
      nil,$rc2['button']['dorefile'],nil,
      Gtklib.pixmap("refile.xpm",window.window),nil) {
      refile_exec
    }
    tbar.append_item(
      nil,$rc2['button']['seen'],nil,
      Gtklib.pixmap("seenall.xpm",window.window),nil) {
      seen_all
    }
    tbar.append_space()
    # serch target
    w = [
      { 'nam'=>"Subject",'val'=>"SUBJECT" },
      { 'nam'=>"From",'val'=>"FROM" },
      { 'nam'=>"Body",'val'=>"BODY" }
    ]
    @target = w[0]['val']
    target = Gtk::OptionMenu.new
    target.set_relief(Gtk::RELIEF_NONE)
    target.set_usize(90,0)
    menu = Gtk::Menu.new
    menu.show
    w.each {|v|
      item = Gtk::MenuItem.new(v['nam'])
      item.child
      item.show
      item.signal_connect("activate",v['val']) {|w,v| @target = v }
      menu.append(item)
    }
    target.set_menu(menu)
    target.show
    tbar.append_widget(target,"Search type",nil)
    # search entry
    @search = Gtk::Entry.new
    @search.show
    @search.set_usize(80,0)
    tbar.append_widget(@search,$rc2['button']['search'],nil)
    # search button
    tbar.append_item(
      nil,$rc2['button']['search'],nil,
      Gtklib.pixmap("search.xpm",window.window),nil) {
      search
    }
    tbox.pack_start(tbar,false,false,0)
    # right tool bar
    tbar = Gtk::Toolbar.new(Gtk::ORIENTATION_HORIZONTAL,Gtk::TOOLBAR_ICONS)
    tbar.show
    tbar.set_button_relief(Gtk::RELIEF_NONE)
    tbar.set_space_style(Gtk::Toolbar::SPACE_LINE)
    tbar.set_space_size(5)
    tbar.append_item(
      nil,$rc2['button']['sgrc_conf'],nil,
      Gtklib.pixmap("config.xpm",window.window),nil) {
      conf = Config_window.new
    }
    tbar.append_space()
    tbar.append_item(
      nil,$rc2['button']['exit'],nil,
      Gtklib.pixmap("cancel.xpm",window.window),nil) {
      top.quit
    }
    tbox.pack_end(tbar,false,false,0)
    # clist box
    hadj = Gtk::Adjustment.new(0,0,0,7,0,0)
    vadj = Gtk::Adjustment.new(0,0,0,16,0,0)
    scroll = Gtk::ScrolledWindow.new(hadj,vadj)
    scroll.show
    @list = Gtk::CList.new(['',"From:#{@moveto}",'Date','Subject'])
    @list.show
    @list.set_row_height(16)
    @list.set_column_width(0,16)
    @list.set_column_width(1,7 * 12)
    @list.set_column_width(2,7 * 11)
    @list.set_column_width(3,7 * 64)
    @list.set_usize(-1,164)
    @list.set_selection_mode(1)
    @list.signal_connect("click_column") {|w,col|
      case col
      when 0
      when 1
	$CACHE.sort("from")
      when 2
	$CACHE.sort("date")
      when 3
	$CACHE.sort("msgid")
      end
      if(col != 0) then
	case $CACHE.rc['sort']
	when "from"
	  @list.set_column_title(0,"")
	  @list.set_column_title(1,"[From:#{@moveto}]")
	    @list.set_column_title(2,"Date")
	  @list.set_column_title(3,"Subject")
	when "date"
	  @list.set_column_title(0,"")
	  @list.set_column_title(1,"From:#{@moveto}")
	    @list.set_column_title(2,"[Date]")
	  @list.set_column_title(3,"Subject")
	when "msgid"
	  @list.set_column_title(0,"")
	  @list.set_column_title(1,"From:#{@moveto}")
	    @list.set_column_title(2,"Date")
	  @list.set_column_title(3,"[Subject]")
	end
	folder = @folder
	open(folder)
      end
    }
    @list.signal_connect("select_row") {|w,r,c,b|
      @now = r
      case c
      when 0
      when 1
	if(@tags[r]['mov']) then
	  @tags[r]['mov'] = nil
	else
	  @tags[r]['mov'] = @moveto
	end
	lupdate(r)
      when 2,3
	if(b) then
	  if(@tags[r]['see'] == false) then
	    @tags[r]['see'] = true
	    @unseen -= 1
	    $main.widget.wprog.status(nil)
	    lupdate(r)
	  end
	  $main.widget.wview.disp(@tags[r],0)
	end
      end
    }
    @list.signal_connect("key_press_event") {|w,e|
      if(e.keyval == Gdk::GDK_space or e.keyval == Gdk::GDK_Return) then
	if(@tags[@now]['see'] == false) then
	  @tags[@now]['see'] = true
	  @unseen -= 1
	  $main.widget.wprog.status(nil)
	  lupdate(@now)
	end
      end
    }
    scroll.add(@list)
    @widget.pack_start(scroll)
  end
  # make menu tree
  def makemenutree(folder)
    menu = Gtk::Menu.new
    menu.show
    folder.folder.each {|v|
      item = Gtk::MenuItem.new(v.name)
      item.show
      menu.append(item)
      if(v.folder != []) then
	item.set_submenu(makemenutree(v))
      else
	item.signal_connect("activate",v) {|w,v|
	  yield(v)
	}
      end
    }
    menu
  end
  # close folder
  def close
    if(@folder) then
      @list.freeze
      @list.clear
      @list.thaw
      $main.widget.wview.clear
      @folder.lclose
    end
  end
  # show mblist
  def open(folder)
    $main.widget.wprog.status("Disp...")
    close
    @folder = folder
    @tags = []
    @now = 0
    @recent = 0
    @unseen = 0
    @search_string = nil
    mlpat = $rc2['mlpat']
    find = false
    i = 0
    case $CACHE.rc['sort']
    when "from"
      @list.set_column_title(1,"[From:#{@moveto}]")
      @list.set_column_title(2,"Date")
      @list.set_column_title(3,"Subject")
    when "date"
      @list.set_column_title(1,"From:#{@moveto}")
      @list.set_column_title(2,"[Date]")
      @list.set_column_title(3,"Subject")
    when "msgid"
      @list.set_column_title(1,"From:#{@moveto}")
      @list.set_column_title(2,"Date")
      @list.set_column_title(3,"[Subject]")
    end
    @folder.lopen()
    @folder.each_mail {|tag|
      @tags << tag
      if(tag['mov']) then
	from = ">>"+tag['mov']
      else
	from = tag['from']
        rev = folder.server.address.rev[from]
	from = rev if(rev)
      end
      from_item = OS.toutf8(from)
      date_item = OS.toutf8(tag['jst'])
      subj = tag['subject']
      if(folder.box != "" and subj =~ /#{mlpat}/) then
	subj = $2
      end
      subj_item = OS.toutf8(format("%s%s"," " * tag['ind'],subj))
      @list.append(['',from_item,date_item,subj_item])
      if(tag['see']) then
	@list.set_pixmap(i,0,@seen_pix,@seen_msk)
      else
	@list.set_pixmap(i,0,@unseen_pix,@unseen_msk)
      end
      if(tag['see'] == false) then
	@unseen += 1
	if(find == false) then
	  find = true
	  @now = i
	end
      end
      if(!find) then @now = i end
      i += 1
    }
    @list.thaw
    @list.grab_focus
    @list.moveto(@now,0,0.0,0.0)
    @list.select_row(@now,-1)
    $main.widget.wprog.status(nil)
    mbar = Gtk::MenuBar.new
    menu = Gtk::MenuItem.new()
    menu.border_width(0)
    menu.show
    list = makemenutree(@folder.server) {|v|
      @moveto = v.box
      case $CACHE.rc['sort']
      when "from"
	@list.set_column_title(1,"[From:#{@moveto}]")
      else
	@list.set_column_title(1,"From:#{@moveto}")
      end
    }
    menu.set_submenu(list)
    mbar.append(menu)
    mbar.show
    @list.set_column_widget(0,mbar)
  end
  # update
  def lupdate(i)
    if(@tags[i]['mov']) then
      from = ">>"+@tags[i]['mov']
    else
      from = @tags[i]['from']
      rev = @folder.server.address.rev[from]
      from = rev if(rev)
    end
    from_item = OS.toutf8(from)
    @list.freeze
    if(@tags[i]['see']) then
      @list.set_pixmap(i,0,@seen_pix,@seen_msk)
    else
      @list.set_pixmap(i,0,@unseen_pix,@unseen_msk)
    end
    @list.set_text(i,1,from_item)
    @list.thaw
  end
  # find next article
  def findnext
    now = @now + 1
    last = @tags.size
    while(now < last)
      if(@tags[now]['see'] == false) then
	@now = now
	@unseen -= 1
	@list.moveto(@now,0,0.0,0.0)
	@list.select_row(@now,-1)
	if(@tags[@now]['see'] == false) then
	  @tags[@now]['see'] = true
	  lupdate(@now)
	  $main.widget.wprog.status(nil)
	end
	$main.widget.wview.disp(@tags[@now],0)
	break
      end
      now += 1
    end
    if(now == last) then
      $main.widget.wview.clear
    end
  end
  # refile
  def refile
    size = @tags.size
    count = 0
    $main.widget.wprog.status(format("(%d/%d/%d)",count,0,size))
    @tags.each_index {|i|
      hdr = @tags[i].header[0]
      if(dst = @folder.server.refile.find(@tags[i],@tags[i]['see'])) then
	@tags[i]['mov'] = dst
	lupdate(i)
	count += 1
      end
      $main.widget.wprog.progress(i.to_f/size.to_f)
      $main.widget.wprog.status(format("(%d/%d/%d)",count,i+1,size))
    }
    $main.widget.wprog.status(nil)
  end
  # refile fource
  def refile_fource
    size = @tags.size
    count = 0
    $main.widget.wprog.status(format("(%d/%d/%d)",count,0,size))
    @tags.each_index {|i|
      if(dst = @folder.server.refile.find(@tags[i],true)) then
	@tags[i]['mov'] = dst
	lupdate(i)
	count += 1
      end
      $main.widget.wprog.progress(i.to_f/size.to_f)
      $main.widget.wprog.status(format("(%d/%d/%d)",count,i+1,size))
    }
    $main.widget.wprog.status(nil)
  end
  # undo refile
  def refile_undo
    size = @tags.size
    count = 0
    $main.widget.wprog.status(format("(%d/%d/%d)",count,0,size))
    @tags.each_index {|i|
      if(@tags[i]['mov']) then
	@tags[i]['mov'] = nil
	lupdate(i)
	count += 1
      end
      $main.widget.wprog.progress(i.to_f/size.to_f)
      $main.widget.wprog.status(format("(%d/%d/%d)",count,i+1,size))
    }
    $main.widget.wprog.status(nil)
  end
  # refile execute
  def refile_exec
    size = @tags.size
    count = 0
    $main.widget.wprog.status(format("(%d/%d/%d)",count,0,size))
    @folder.server.login {
      @folder.lupdate()
      @tags.each_index {|i|
	if(@tags[i]['mov']) then
	  @tags[i].move()
	  count += 1
	end
	$main.widget.wprog.progress(i.to_f/size.to_f)
	$main.widget.wprog.status(format("(%d/%d/%d)",count,i+1,size))
      }
      @folder.expunge()
      $main.widget.wprog.status(nil)
    }
    folder = @folder
    open(folder)
  end
  # set seen all
  def seen_all
    size = @tags.size
    count = 0
    $main.widget.wprog.status(format("(%d/%d/%d)",count,0,size))
    @tags.each_index {|i|
      if(@tags[i]['see'] == false) then
	@tags[i]['see'] = true
	lupdate(i)
	count += 1
      end
      $main.widget.wprog.progress(i.to_f/size.to_f)
      $main.widget.wprog.status(format("(%d/%d/%d)",count,i+1,size))
    }
    @unseen = 0
    $main.widget.wprog.status(nil)
  end
  # search
  def search
    if(@folder) then
      changed = false
      if(@search.get_text() == "" or
	 @search_string != @search.get_text() or
	 @search_target != @target) then
	changed = true
      end
      if(changed and @search.get_text() != "") then
	@search_target = @target
	@search_string = @search.get_text()
	@search_list = @folder.find(@search_target,@search_string)
      end
      uid = @search_list.shift
      if(uid) then
	@tags.each_index {|now|
	  if(@tags[now]['uid'] == uid) then
	    @now = now
	    @list.moveto(@now,0,0.0,0.0)
	    @list.select_row(@now,-1)
	    if(@tags[@now]['see'] == false) then
	      @tags[@now]['see'] = true
	      @unseen -= 1
	      lupdate(@now)
	      $main.widget.wprog.status(nil)
	    end
	    $main.widget.wview.disp(@tags[@now],0)
	    if(@target == "BODY")
#	      $main.widget.wview.search(@search_string)
	    end
	    break
	  end
	}
      else
	if(@search.get_text() != "") then
	  SGDialog.new("search_end","OK",@search_string)
	  @search_string = nil
	  @search_target = nil
	end
      end
    end
  end
  # get folder status
  def status
    if(@folder) then
      [@tags.size,@unseen,@recent]
    else
      [nil,nil,nil]
    end
  end
  # biff
  def biff(ticks)
    if(@folder) then
      server = @folder.server
      if(server.rc['imap']['biff'] != 0) then
	if(ticks % (server.rc['imap']['biff'] * 10) == 0) then
	  server.biff()
	  if(server.recent) then
	    @recent = server.recent
	    $main.widget.wprog.status(nil)
	  end
	end
      end
    end
  end
end
