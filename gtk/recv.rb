#
# tiny mailbox type
#

class Recv_tiny
attr :wmbox
attr :wlist
attr :wview
attr :wprog
  def initialize(top)
    frame = Gtk::VBox.new(false,0)
    frame.show
    box3 = Gtk::HPaned.new
    box3.show
    frame.pack_start(box3,true,true,0)
    @wmbox = MboxTree.new(top)
    box3.add1(@wmbox.widget)
    @wlist = MailList.new(top,top)
    box3.add2(@wlist.widget)
    @wprog = Progress.new(top)
    frame.pack_start(@wprog.widget,false,false,0)
    top.add(frame)
    @wview = MailView_window.new()
  end
  # make view
  def makeview(top)
    @wview = MailView_window.new()
  end
end

#
# small mailbox type
#

class Recv_small
attr :wmbox
attr :wlist
attr :wview
attr :wprog
  def initialize(top)
    frame = Gtk::VBox.new(false,0)
    frame.show
    box2 = Gtk::VPaned.new
    box2.show
    frame.pack_start(box2,true,true,0)
    box3 = Gtk::HPaned.new
    box3.show
    box2.add1(box3)
    @wmbox = MboxTree.new(top)
    box3.add1(@wmbox.widget)
    @wlist = MailList.new(top,top)
    box3.add2(@wlist.widget)
    @wview = MailView.new(top,true)
    @wview.widget.show
    box2.add2(@wview.widget)
    @wprog = Progress.new(top)
    frame.pack_start(@wprog.widget,false,false,0)
    top.add(frame)
  end
end

#
# large mailbox type
#

class Recv_large
attr :wmbox
attr :wlist
attr :wview
attr :wprog
  def initialize(top)
    frame = Gtk::VBox.new(false,0)
    frame.show
    box2 = Gtk::HPaned.new
    box2.show
    frame.pack_start(box2,true,true,0)
    @wmbox = MboxTree.new(top)
    box2.add1(@wmbox.widget)
    box4 = Gtk::VPaned.new
    box4.show
    @wlist = MailList.new(top,top)
    @wview = MailView.new(top,true)
    @wview.widget.show
    box4.add1(@wlist.widget)
    box4.add2(@wview.widget)
    box2.add2(box4)
    @wprog = Progress.new(top)
    frame.pack_start(@wprog.widget,false,false,0)    
    top.add(frame)
  end
end

#
# main window
#

class Recv < Gtk::Window
attr :widget
  def initialize
    @widget = nil
    @view = nil
    @idling = false
    @work = false
    super(Gtk::WINDOW_TOPLEVEL)
    realize
    set_policy(true,true,0)
    set_title("#{OS::PRG} Ver #{$Version}")
    signal_connect("destroy") { quit }
    if($gtkv <= "1.2.3") then
      signal_connect("size_allocate") {|w,a|
	set_default_size(a.width,a.height)
      }
    end
    # password callback
    pass_cb = Proc.new {|user|
      dlg = PasswdDialog.new(user)
      user = dlg.user
      passwd = dlg.passwd
      [user,passwd]
    }
    # status,progress,dialog callback
    stat_cb = Proc.new {|msg| @widget.wprog.status(msg) }
    prog_cb = Proc.new {|ratio| @widget.wprog.progress(ratio) }
    dlog_cb = Proc.new {|msg| SGDialog.new(msg) }
    $CACHE.callback(pass_cb,stat_cb,prog_cb,dlog_cb)
    # make gtk widget
    case $rc['style']
    when "tiny"
      @widget = Recv_tiny.new(self)
    when "small"
      @widget = Recv_small.new(self)
    when "large"
      @widget = Recv_large.new(self)
    end
  end
  # busy?
  def busy?
    @work
  end
  # busy
  def busy
    @work = true
  end
  # idle
  def idle
    @work = false
  end
  # tiny view destroy
  def view_destroy
    @widget.makeview(self)
  end
  # quit
  def quit
    @widget.wlist.close
    $CACHE.finish()
    exit
  end
end
