#
# main
#

class Main_window < Gtk::Window
  def initialize
    @process = 0
    super(Gtk::WINDOW_TOPLEVEL)
    realize
    signal_connect("destroy") { quit }
    set_title("#{OS::PRG}")
    # tool bar
    tbar = Gtk::Toolbar.new(
      Gtk::ORIENTATION_HORIZONTAL,Gtk::TOOLBAR_ICONS)
    tbar.show
#    tbar.set_button_relief(Gtk::RELIEF_NONE)
    tbar.set_space_style(Gtk::Toolbar::SPACE_LINE)
    tbar.set_space_size(5)
    $rc['items'].each {|k,v|
      if(v['command']) then
	if(v['icon'] =~ /^\//) then
	  pix,msk = Gdk::Pixmap::create_from_xpm(
            self.window,nil,v['icon'])
	else
	  pix,msk = Gdk::Pixmap::create_from_xpm(
            self.window,nil,OS::LIB+"/gtk/images/"+v['icon'])
	end
	pixmap = Gtk::Pixmap::new(pix,msk)
	tbar.append_item(nil,k,nil,pixmap,nil) { launch(v) }
      end
    }
    pix,msk = Gdk::Pixmap::create_from_xpm(
      self.window,nil,OS::LIB+"/gtk/images/exit.xpm")
    pixmap = Gtk::Pixmap::new(pix,msk)
    tbar.append_item(nil,"exit",nil,pixmap,nil) { quit }
    add(tbar)
    show_all
  end
  #
  def launch(prm)
    if(prm) then
      ssh = ""
      if(prm['port']) then
	prm['port'].each {|k,v|
	  if(v['local'] > 1024 and v['remote'] != 0) then
	    ssh << " -L "
	    ssh << v['local'].to_s
	    ssh << ":"+prm['server']+":"
	    ssh << v['remote'].to_s
	  end
	}
      end
      ary = prm['command'].split(" ")
      cmd = ary.shift
      arg = ary.join(" ")
      if(ssh == "") then
	OS.fork(cmd,arg)
      else
	dlg = PasswdDialog.new(prm['user'])
	thread = Thread.start {
	  sh = SSH.new(
          $rc['ssh']+" -l "+dlg.user+ssh+" "+prm['server'],dlg.passwd)
	  @process += 1
	  PTY.protect_signal {
	    fork() {
	      exec(cmd,arg)
	    }
	    Process.wait
	  }
	  @process -= 1
	  Process.kill("KILL",sh.pid)
	}
      end
    end
  end
  # quit
  def quit
    if(@process == 0) then
      exit
    else
      dlg = SGDialog.new("process",["OK","Cancel"])
      if(dlg.value == "OK") then
	exit
      end
    end
  end
end

$rc2 = {}
$rc2['dialog'] = {}
$rc2['dialog']['process'] = []
$rc2['dialog']['process'][0] = "Warning"
$rc2['dialog']['process'][1] = "Child process is running!"
$main = Main_window.new
Gtk.main
