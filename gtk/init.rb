#
# load gtk resource
#

$gtkv = sprintf("%d.%d.%d",
  Gtk::MAJOR_VERSION,Gtk::MINOR_VERSION,Gtk::MICRO_VERSION)
$info = "#{OS::PRG} Ver #{$Version}(OS:#{OS::SYS},Ruby:#{VERSION},Gtk:#{$gtkv})"
if($rc['theme'] != "") then
  Gtk::RC.parse($rc['theme'])
end
Gtk::RC.parse(OS::LIB+"/rc/gtk"+$rc['language']+"."+$rc['encoding'])
