#
# progress bar
#

class Progress
attr :widget
attr :prog
attr :pbar
  def initialize(top)
    @top = top
    @idling = false
    # progress bar
    @widget = Gtk::HBox.new(false,2)
    @widget.show
    @prog = Gtk::Button.new("Idle")
    @prog.show
    @prog.set_usize(154,-1)
    @prog.signal_connect("clicked") {
      if($main.widget.wmbox.server) then
	$main.widget.wlist.open($main.widget.wmbox.server.inbox)
      end
    }
    @widget.pack_start(@prog,false,false,0)
    adj = Gtk::Adjustment.new(0,1,100,0,0,0)
    @pbar = Gtk::ProgressBar.new(adj)
    @pbar.show
    @pbar.set_format_string("%p%%")
    @pbar.set_show_text(true)
    @pbar.set_bar_style(0)
    @pbar.set_discrete_blocks(10)
    @pbar.set_text_alignment(0.5,1.0)
    @pbar.set_usize(-1,24)
    @widget.pack_start(@pbar,true,true,0)
    Gtk::idle_add() { @idling = true }
  end
  # show status
  def status(msg)
    if(msg == nil) then
      progress(0)
      all,unseen,recent = $main.widget.wlist.status()
      if(all) then
        msg = format($rc2['button']['biff'],all,unseen,recent)
      else
        msg = ""
      end
    end
    @prog.child.set_text(msg)
    @idling = false
    while(@idling == false)
      Gtk.main_iteration
    end
  end
  # show progress
  def progress(ratio)
    @pbar.update(ratio)
    Gtk.main_iteration
  end
end
