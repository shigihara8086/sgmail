#
# mail view
#

class MailView
attr :widget
  def initialize(window,flag)
    @window = window
    @mime = nil
    @tbar = nil
    @tag = nil
    @widget = Gtk::VBox.new(false,0)
    @widget.show
    # tool bar
    tbox = Gtk::HBox.new(false,0)
    tbox.show
    # tool bar
    tbar = Gtk::Toolbar.new(Gtk::ORIENTATION_HORIZONTAL,Gtk::TOOLBAR_ICONS)
    tbar.show
    tbar.set_button_relief(Gtk::RELIEF_NONE)
    tbar.set_space_style(Gtk::Toolbar::SPACE_LINE)
    tbar.set_space_size(5)
    if(flag) then
      tbar.append_item(nil,$rc2['button']['reply'],nil,
        Gtklib.pixmap("reply.xpm",window.window),nil) { reply }
      tbar.append_item(nil,$rc2['button']['xfer'],nil,
        Gtklib.pixmap("xfer.xpm",window.window),nil) { transfer }
      tbar.append_item( nil,$rc2['button']['resend'],nil,
        Gtklib.pixmap("send.xpm",window.window),nil) { resend }
      tbar.append_space()
    end
    tbar.append_item(nil,$rc2['button']['copy'],nil,
      Gtklib.pixmap("copy.xpm",window.window),nil) { copy }
    tbar.append_space()
    tbar.append_item(nil,$rc2['button']['save'],nil,
      Gtklib.pixmap("save.xpm",window.window),nil) { save }
    tbar.append_item(nil,$rc2['button']['print'],nil,
      Gtklib.pixmap("print.xpm",window.window),nil) { printout }
    tbar.append_space()
    tbar.append_item(nil,$rc2['button']['browser'],nil,
      Gtklib.pixmap("netscape.xpm",window.window),nil) { exec }
    tbox.pack_start(tbar,false,false,0)
    # right tool bar
    if($rc['style'] == "tiny" or flag == false) then
      tbar = Gtk::Toolbar.new(Gtk::ORIENTATION_HORIZONTAL,Gtk::TOOLBAR_ICONS)
      tbar.show
      tbar.set_button_relief(Gtk::RELIEF_NONE)
      tbar.set_space_style(Gtk::Toolbar::SPACE_LINE)
      tbar.set_space_size(5)
      tbar.append_item(nil,$rc2['button']['close'],nil,
	Gtklib.pixmap("cancel.xpm",window.window),nil) { window.hide }
      tbox.pack_end(tbar,false,false,0)
    end
    @widget.pack_start(tbox,false,false,0)
    # header box
    hhadj = Gtk::Adjustment.new(0,0,0,7,0,0)
    hvadj = Gtk::Adjustment.new(0,0,0,16,0,0)
    hscroll = Gtk::ScrolledWindow.new(hhadj,hvadj)
    hscroll.show
    @header = Gtk::CList.new(['header','body'])
    @header.show
    @header.column_titles_hide
    @header.set_row_height(16)
    @header.set_column_width(0,7 * 12)
    @header.set_column_width(1,7 * 256)
    @header.set_usize(-1,76)
    @header.set_selection_mode(3)
    hscroll.add(@header)
    @widget.pack_start(hscroll,false,false,0)
    # text box
    hbox = Gtk::HBox.new(false,0)
    hbox.show
    vadj = Gtk::Adjustment.new(0,0,0,0,0,0)
    vbar = Gtk::VScrollbar.new(vadj)
    vbar.show
    @text = Gtk::Text.new(nil,vadj)
    @text.show
    @text.set_editable(false)
    @text.signal_connect("key_press_event") {|w,e|
      if(e.keyval == Gdk::GDK_space) then
        now = vadj.value
        lmt = vadj.upper - vadj.page_size
        if(now == lmt) then
	  $main.widget.wlist.findnext
        else
          now += vadj.page_size
          if(now > lmt) then
            now = lmt
          end
          vadj.value=now
        end
      end
      true
    }
    hbox.pack_start(@text,true,true,0)
    hbox.pack_start(vbar,false,false,0)
    @widget.pack_start(hbox,true,true,0)
    # MIME
    @mime = Gtk::ScrolledWindow::new(nil,nil)
    @mime.border_width(0)
    @mime.set_policy(Gtk::POLICY_ALWAYS,Gtk::POLICY_NEVER)
    @widget.pack_end(@mime,false,false,0)
    @mime.hide
    begin
      @view = Gtk::Viewport.new()
    rescue
      @view = Gtk::ViewPort.new()
    end
    @view.show
    @mime.add(@view)
    @tbar = Gtk::Toolbar.new(Gtk::ORIENTATION_HORIZONTAL)
    @tbar.show
    @view.add(@tbar)
    @tbar.set_button_relief(Gtk::RELIEF_NONE)
    @tbar.set_space_style(Gtk::Toolbar::SPACE_LINE)
    @tbar.set_space_size(5)
  end
  # clear mail text
  def clear
    @header.clear
    @text.set_editable(true)
    @text.freeze
    @text.set_point(0)
    @text.forward_delete(@text.get_length)
    @text.set_point(0)
    @text.thaw
    @text.grab_focus
    @text.set_editable(false)
    @mime.hide
  end
  # show mail text
  def disp(tag,part)
    @tag = tag
    @part = part
    hdr = tag.header
    # show header
    @header.clear
    if(part == 0) then
      @header.append(["Subject",":"+hdr[0]['Subject']])
      @header.append(["From",":"+hdr[0]['From']])
      @header.append(["Date",":"+hdr[0]['Date']])
      @header.append(
	["To",":"+(hdr[0]['To'] ? hdr[0]['To'] : "")])
      @header.append(
	["Message-Id",":"+(hdr[0]['Message-Id'] ? hdr[0]['Message-Id'] : "")])
      @header.append(
	["In-Reply-To",":"+(hdr[0]['In-Reply-To'] ? hdr[0]['In-Reply-To'] : "")])
      hdr[0].each {|k,v|
	case k
	when 'Subject','Date','From','To','Message-Id','In-Reply-To'
	else
	  if(k != 'Received') then
	    @header.append([k,":"+v])
	  end
	end
      }
      if(hdr[0]['Received'] != nil) then
	hdr[0]['Received'].each {|line|
	  @header.append(["Received",":"+line])
	}
      end
    else
      hdr[part].each {|k,v|
	@header.append([k,":"+v])
      }
    end
    # show text
    text = tag.body(part)
    @text.set_editable(true)
    @text.freeze
    @text.set_point(0)
    @text.forward_delete(@text.get_length)
    @text.insert_text(text,0)
    @text.set_point(0)
    @text.thaw
    @text.grab_focus
    @text.set_editable(false)
    # show MIME icons
    if(part == 0) then
      @view.remove(@tbar)
      @tbar = Gtk::Toolbar.new(Gtk::ORIENTATION_HORIZONTAL)
      @tbar.show
      @view.add(@tbar)
      @tbar.set_button_relief(Gtk::RELIEF_NONE)
      @tbar.set_space_style(Gtk::Toolbar::SPACE_LINE)
      @tbar.set_space_size(5)
      cnt = 0
      hdr.each_index {|i|
	if(i == 0) then next end
	if(hdr[i]['Content-Type'] == nil) then break end
	if(hdr[i]['Content-Type']) then
	  type,charset = hdr[i]['Content-Type'].split(/;/)
	else
	  type = "text/unknown"
	end
	if(hdr[i]['Content-Type'] =~ /name=\"(.*?)\"/) then
	  name = $1
	else
	  name = "unknown.txt"
	end
	case type
	when /^text\/html/
	  @tbar.append_item(name,type,nil,
	    Gtklib.pixmap("html.xpm",@window.window),nil) {
	    file = tag.save(i)
	    OS.netscape($rc['html'],file)
	  }
	when /^text\//,/^message\//
	  @tbar.append_item(name,type,nil,
	    Gtklib.pixmap("text.xpm",@window.window),nil) {
	    view = MailView_mime.new()
	    view.disp(tag,i)
	  }
	when /^image\//
	  @tbar.append_item(name,type,nil,
	    Gtklib.pixmap("image.xpm",@window.window),nil) {
	    file = tag.save(i)
	    OS.netscape($rc['image'],file)
	  }
	else
	  @tbar.append_item(name,type,nil,
	    Gtklib.pixmap("unknown.xpm",@window.window),nil) {
	    fs = Gtk::FileSelection.new('Save')
	    fs.show
	    fs.set_filename(name)
	    fs.show_all
	    fs.grab_add
	    loop = true
	    flag = false
	    fs.ok_button.signal_connect("clicked") {
	      flag = true
	      loop = false
	    }
	    fs.cancel_button.signal_connect("clicked") {
	      flag = false
	      loop = false
	    }
	    while(loop)
	      Gtk.main_iteration
	    end
	    fs.grab_remove
	    fs.hide
	    if(flag) then
	      file = tag.save(i,fs.get_filename)
	    end
	    fs.destroy
	  }
	end
	cnt += 1
      }
      if(cnt != 0) then
	@mime.show
      else
	@mime.hide
      end
    end
  end
  # reply
  def reply
    send = Send.new("Reply",@tag.folder.server,@tag,nil)
  end
  # transfer
  def transfer
    send = Send.new("Transfer",@tag.folder.server,@tag,nil)
  end
  # resend
  def resend
    send = Send.new("Resend",@tag.folder.server,@tag,nil)
  end
  # copy
  def copy
    @text.copy_clipboard()
  end
  # save
  def save
    fs = Gtk::FileSelection.new('Save')
    fs.show
    fs.show_all
    fs.grab_add
    loop = true
    flag = false
    fs.ok_button.signal_connect("clicked") {
      flag = true
      loop = false
    }
    fs.cancel_button.signal_connect("clicked") {
      flag = false
      loop = false
    }
    while(loop)
      Gtk.main_iteration
    end
    fs.grab_remove
    fs.hide
    if(flag) then
      file = @tag.save(@part,fs.get_filename)
    end
    fs.destroy
  end
  # print
  def printout
    hdr = @tag.header
    if(fd = open("|"+$rc['lpr'],"w")) then
      fd.puts format("Subject: %s\n",hdr[0]['Subject'])
      fd.puts format("From:    %s\n",hdr[0]['From'])
      fd.puts format("Date:    %s\n",hdr[0]['Date'])
      fd.puts format("To:      %s\n",hdr[0]['To'])
      fd.puts ""
      @tag.body(@part).each_line {|line|
	fd.puts(line)
      }
      fd.close
    end
  end
  # execute browser or new mail
  def exec
    s = @text.selection_start_pos()
    e = @text.selection_end_pos()
    sel = (s < e) ? @text.get_chars(s,e) : @text.get_chars(e,s)
    case sel
    when /#{$rc2['urlpat']}/
      OS.netscape($rc['html'],sel)
    when /#{$rc2['mailpat']}/
      send = Send.new("Send",@tag.folder.server,@tag,sel)
    end
  end
end

#
# 
#

class MailView_window < Gtk::Window
attr :view
  def initialize
    @pos_x = nil
    @pos_y = nil
    @size_x = nil
    @size_y = nil
    super(Gtk::WINDOW_TOPLEVEL)
    set_title("View")
    realize
    @view = MailView.new(self,true)
    @view.widget.set_usize(579,500)
    @view.widget.show
    add(@view.widget)
    signal_connect("destroy") { quit }
    if($gtkv <= "1.2.3") then
      signal_connect("size_allocate") {|w,a|
	set_default_size(a.width,a.height)
      }
    end
    signal_connect("configure_event") {|w,a|
      @size_x = a.width
      @size_y = a.height
      @pos_x,@pos_y = window.get_root_origin()
    }
  end
  # clear mail text
  def clear
    @view.clear
    hide
  end
  # show mail text
  def disp(tag,part)
    @view.disp(tag,part)
    if(visible? == false) then
      show
      if(@pos_x and @pos_y) then
	set_uposition(@pos_x,@pos_y)
      end
      if(@size_x and @size_y) then
	set_usize(@size_x,@size_y)
      end
    end
  end
  # quit
  def quit
    $main.view_destroy
  end
end

#
# 
#

class MailView_mime < Gtk::Window
attr :view
  def initialize
    @pos_x = nil
    @pos_y = nil
    super(Gtk::WINDOW_TOPLEVEL)
    set_title("View")
    realize
    @view = MailView.new(self,false)
    @view.widget.set_usize(580,500)
    @view.widget.show
    add(@view.widget)
    if($gtkv <= "1.2.3") then
      signal_connect("size_allocate") {|w,a|
	set_default_size(a.width,a.height)
      }
    end
  end
  # show mail text
  def disp(tag,part)
    @view.disp(tag,part)
    show
  end
end
