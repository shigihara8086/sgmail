#
# Extended Gtk class
#

class Gtklib
  def Gtklib.pixmap(file,window)
    pix,msk = Gdk::Pixmap::create_from_xpm(window,nil,OS::LIB+"/gtk/images/#{file}")
    pixmap = Gtk::Pixmap::new(pix,msk)
    pixmap
  end
end
