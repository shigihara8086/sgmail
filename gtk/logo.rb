#
# logo window
#

class Logo < Gtk::Window
  def initialize
    super(Gtk::WINDOW_TOPLEVEL)
    realize
    set_title("#{OS::PRG} Ver #{$Version}")
    vbox = Gtk::VBox.new(false,0)
    pix = Gtklib.pixmap("banner.xpm",self.window)
    lbl = Gtk::Label.new("Ver #{$Version} (C) by SHIGIHARA,Atsuhiro.")
    vbox.pack_start(pix,true,true,0)
    vbox.pack_start(lbl,true,true,0)
    add(vbox)
    show_all
    first = true
    case $rc['style']
    when "large"
      lx = 740
      ly = 730
    when "small"
      lx = 580
      ly = 730
    when "tiny"
      lx = 579
      ly = 246
    end
    ticks = 0
    timer = 15
    Gtk::timeout_add(100) {
      ticks += 1
      if(ticks == timer and first) then
	$main.set_usize(lx,ly)
	$main.show
	first = false
      end
      if(ticks == timer + 1) then
	$main.set_usize(lx,ly)
	$logo.hide
      end
      if(ticks > timer) then
	$main.widget.wlist.biff(ticks)
      end
      Gtk.main_iteration
      true
    }
  end
end
