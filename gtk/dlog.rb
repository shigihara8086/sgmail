#
# generic dialog
#

class SGDialog < Gtk::Dialog
attr :value
  def initialize(msg,button="OK",arg=nil)
    super()
    @value = nil
    @loop = true
    set_title($rc2['dialog'][msg][0])
    box = Gtk::VBox.new
    lbl = Gtk::Label.new(format($rc2['dialog'][msg][1],arg))
    box.pack_start(lbl)
    box.border_width(10)
    child.pack_start(box,true,true,0)
    if(button.kind_of?(Array)) then
      f = true
      button.each {|b|
	btn = Gtk::Button.new(b)
	begin
	  btn.flags |= Gtk::Widget::CAN_DEFAULT
	rescue
	  btn.flags |= Gtk::CAN_DEFAULT
	end
	action_area.pack_start(btn)
	if(f == true) then
	  btn.grab_default
	  f = false
	end
	btn.signal_connect("clicked",b) {|w,b|
	  @value = b
	  @loop = false
	}
      }
    elsif(button.kind_of?(Hash)) then
      button.each {|k,v|
	btn = Gtk::Button.new(k)
	begin
	  btn.flags |= Gtk::Widget::CAN_DEFAULT
	rescue
	  btn.flags |= Gtk::CAN_DEFAULT
	end
	action_area.pack_start(btn)
	if(v == 0) then
	  btn.grab_default
	end
	btn.signal_connect("clicked",v) {|w,v|
	  @value = v
	  @loop = false
	}
      }
    else
      btn = Gtk::Button.new(button)
      begin
	btn.flags |= Gtk::Widget::CAN_DEFAULT
      rescue
	btn.flags |= Gtk::CAN_DEFAULT
      end
      action_area.pack_start(btn)
      btn.grab_default
      btn.signal_connect("clicked",button) {|w,v|
	@value = v
	@loop = false
      }
    end
    show_all
    px,py = $main.window.get_root_origin()
    sx,sy = $main.window.get_size()
    dx,dy = window.get_size()
    set_uposition(px+(sx-dx)/2,py+(sy-dy)/2)
    grab_add
    while(@loop)
      Gtk.main_iteration
    end
    grab_remove
    hide
    destroy
  end
end

#
# entry dialog
#

class EntryDialog < Gtk::Dialog
attr :value
  def initialize(title,label,value = "")
    super()
    @loop = true
    @value = nil
    set_title(title)
    table = Gtk::Table.new(2,1,false)
    table.border_width(10)
    lbl = Gtk::Label.new(label)
    table.attach(lbl,0,1,0,1)
    entry = Gtk::Entry.new
    entry.set_text(value)
    entry.signal_connect("key_press_event") {|w,e|
      if(e.keyval == Gdk::GDK_Return) then
	loop = true
      end
      true
    }
    table.attach(entry,1,2,0,1)
    child.pack_start(table,true,true,0)
    ok = Gtk::Button.new("Ok")
    begin
      ok.flags |= Gtk::Widget::CAN_DEFAULT
    rescue
      ok.flags |= Gtk::CAN_DEFAULT
    end
    ok.signal_connect("clicked") {
      @value = entry.get_text
      @loop = false
    }
    action_area.pack_start(ok)
    ok.grab_default
    cancel = Gtk::Button.new("Cancel")
    cancel.signal_connect("clicked") {
      @value = nil
      @loop = false
    }
    action_area.pack_start(cancel)
    entry.grab_focus
    show_all
    px,py = $main.window.get_root_origin()
    sx,sy = $main.window.get_size()
    dx,dy = window.get_size()
    set_uposition(px+(sx-dx)/2,py+(sy-dy)/2)
    grab_add
    while(@loop)
      Gtk.main_iteration
    end
    grab_remove
    hide
    destroy
  end
end

#
# password dislog
#

class PasswdDialog < Gtk::Dialog
attr :user
attr :passwd
attr :pphrase
  def initialize(user)
    super()
    @user = ""
    @passwd = ""
    loop = nil
    set_title("Password")
    table = Gtk::Table.new(2,2,false)
    table.border_width(10)
    lbl = Gtk::Label.new("UserID     ")
    table.attach(lbl,0,1,0,1)
    lbl = Gtk::Label.new("Password   ")
    table.attach(lbl,0,1,1,2)
    id = Gtk::Entry.new
    id.set_text(user)
    table.attach(id,1,2,0,1)
    pw = Gtk::Entry.new
    pw.set_text("")
    pw.set_visibility(false)
    pw.signal_connect("key_press_event") {|w,e|
      if(e.keyval == Gdk::GDK_Return) then
	loop = true
      end
      true
    }
    table.attach(pw,1,2,1,2)
    child.pack_start(table,true,true,0)
    ok = Gtk::Button.new("Ok")
    begin
      ok.flags |= Gtk::Widget::CAN_DEFAULT
    rescue
      ok.flags |= Gtk::CAN_DEFAULT
    end
    ok.signal_connect("clicked") { loop = true }
    action_area.pack_start(ok)
    ok.grab_default
    cancel = Gtk::Button.new("Cancel")
    cancel.signal_connect("clicked") { loop = false }
    action_area.pack_start(cancel)
    if(user == "") then
      id.grab_focus
    else
      pw.grab_focus
    end
    show_all
    px,py = $main.window.get_root_origin()
    sx,sy = $main.window.get_size()
    dx,dy = window.get_size()
    set_uposition(px+(sx-dx)/2,py+(sy-dy)/2)
    grab_add
    while(loop == nil)
      Gtk.main_iteration
    end
    if(loop == true) then
      @user = id.get_text
      @passwd = pw.get_text
    end
    grab_remove
    hide
    destroy
  end
end

#
# mailbox create dialog
#

class MboxCreateDialog < EntryDialog
  def initialize
    super("Create mailbox",$rc2['dialog']['mbox_name'],"")
  end
end

#
# mailbox delete dialog
#

class MboxDeleteDialog < EntryDialog
  def initialize
    super("Delete mailbox",$rc2['dialog']['mbox_name'],"")
  end
end

#
# mailbox rename dialog
#

class MboxRenameDialog < EntryDialog
  def initialize(old)
    super("Rename mailbox",$rc2['dialog']['mbox_name'],old)
  end
end
