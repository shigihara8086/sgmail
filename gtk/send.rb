#
# send window
#

require "thread"

class Send
include Fold
include Binary
  def initialize(title,server,tag,to)
    @title = title
    @server = server
    @tag = tag
    @to = to
    @expert_mode = false
    @text_alloc = nil
    @where = server.where[0]['val']
    @sign = server.signature.sign[0]['val']
    @mime_files = {}
    @mime_selected = nil
    # toplevel
    @window = Gtk::Window.new(Gtk::WINDOW_TOPLEVEL)
    @window.realize
    @window.set_policy(true,true,0)
    @window.set_title(title)
    @window.signal_connect("destroy") { quit }
    # text binary pixmap
    @text_pix,@text_msk = Gdk::Pixmap::create_from_xpm(
      @window.window,nil,OS::LIB+"/gtk/images/seen.xpm")
    @binary_pix,@binary_msk = Gdk::Pixmap::create_from_xpm(
      @window.window,nil,OS::LIB+"/gtk/images/unseen.xpm")
    # vbox
    vbox = Gtk::VBox.new(false,0)
    hbox1 = Gtk::HBox.new(false,0)
    hbox1.show
    # toolbar
    tbar = Gtk::Toolbar.new(Gtk::ORIENTATION_HORIZONTAL,Gtk::TOOLBAR_ICONS)
    tbar.set_button_relief(Gtk::RELIEF_NONE)
    tbar.set_space_style(Gtk::Toolbar::SPACE_LINE)
    tbar.set_space_size(5)
    tbar.append_item(nil,$rc2['button']['send'],nil,
      Gtklib.pixmap("send.xpm",@window.window),nil) { send }
    tbar.append_space()
    # where selector
    @direct_menu = Gtk::OptionMenu.new
    @direct_menu.set_relief(Gtk::RELIEF_NONE)
    @direct_menu.set_usize(80,0)
    menu = Gtk::Menu.new
    menu.show
    server.where.each {|v|
      item = Gtk::MenuItem.new(v['nam'])
      item.child
      item.show
      item.signal_connect("activate",v) {|w,v| @where = v['val'] }
      menu.append(item)
    }
    @direct_menu.set_menu(menu)
    @direct_menu.show
    tbar.append_widget(@direct_menu,nil,nil)
    #
    tbar.append_space()
    tbar.append_item(nil,$rc2['button']['cut'],nil,
      Gtklib.pixmap("cut.xpm",@window.window),nil) { cut }
    tbar.append_item(nil,$rc2['button']['copy'],nil,
      Gtklib.pixmap("copy.xpm",@window.window),nil) { copy }
    tbar.append_item(nil,$rc2['button']['paste'],nil,
      Gtklib.pixmap("paste.xpm",@window.window),nil) { paste }
    tbar.append_item(nil,$rc2['button']['pasterep'],nil,
      Gtklib.pixmap("pasterep.xpm",@window.window),nil) { pasterep }
    #
    tbar.append_space()
    tbar.append_item(nil,$rc2['button']['instext'],nil,
      Gtklib.pixmap("load.xpm",@window.window),nil) { load }
    tbar.append_item(nil,$rc2['button']['attach'],nil,
      Gtklib.pixmap("mimet.xpm",@window.window),nil) { attach }
    tbar.append_item(nil,$rc2['button']['detach'],nil,
      Gtklib.pixmap("mimeb.xpm",@window.window),nil) { detach }
    #
    tbar.append_space()
    tbar.append_item(nil,$rc2['button']['expart'],nil,
      Gtklib.pixmap("expert.xpm",@window.window),nil) { expert }
    tbar.show_all
    hbox1.pack_start(tbar,false,false,0)
    # toolbar
    tbar = Gtk::Toolbar.new(Gtk::ORIENTATION_HORIZONTAL,Gtk::TOOLBAR_ICONS)
    tbar.set_button_relief(Gtk::RELIEF_NONE)
    tbar.set_space_style(Gtk::Toolbar::SPACE_LINE)
    tbar.set_space_size(5)
    # signature button
    @signature_menu = Gtk::OptionMenu.new
    @signature_menu.set_relief(Gtk::RELIEF_NONE)
    @signature_menu.set_usize(120,0)
    menu = Gtk::Menu.new
    menu.show
    server.signature.sign.each {|v|
      item = Gtk::MenuItem.new(v['nam'])
      item.child
      item.show
      item.signal_connect("activate",v) {|w,v| @sign = v['val'] }
      menu.append(item)
    }
    @signature_menu.set_menu(menu)
    @signature_menu.show
    tbar.append_widget(@signature_menu,nil,nil)
    tbar.append_space()
    tbar.append_item(nil,$rc2['button']['close'],nil,
      Gtklib.pixmap("cancel.xpm",@window.window),nil) { quit }
    tbar.show_all
    hbox1.pack_end(tbar,false,false,0)
    vbox.pack_start(hbox1,false,false,0)
    # H-separator
    sep = Gtk::HSeparator.new
    sep.show
    vbox.pack_start(sep,false,false,0)
    # hbox
    hbox = Gtk::HBox.new(false,0)
    hbox.show
    # normal header
    normal = Gtk::Table.new(2,4,false)
    normal.border_width(0)
    # subject
    subject_menu = Gtk::MenuBar.new
    item = Gtk::MenuItem.new("Subject:    ")
    item.show
    subject_menu.append(item)
    item.set_submenu(makemenutree(server.template) {|v| select_template(v)})
    @subject_entry = Gtk::Entry.new
    normal.attach(subject_menu,  0,1,0,1,Gtk::SHRINK,Gtk::SHRINK)
    normal.attach(@subject_entry,1,2,0,1,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK)
    # to
    to_menu = Gtk::MenuBar.new
    item = Gtk::MenuItem.new("To:         ")
    item.show
    to_menu.append(item)
    item.set_submenu(makemenutree(server.address.adr) {|v| select_to(v)})
    @to_entry = Gtk::Entry.new
    normal.attach(to_menu,  0,1,1,2,Gtk::SHRINK,Gtk::SHRINK)
    normal.attach(@to_entry,1,2,1,2,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK)
    # cc
    cc_menu = Gtk::MenuBar.new
    item = Gtk::MenuItem.new("Cc:         ")
    item.show
    cc_menu.append(item)
    item.set_submenu(makemenutree(server.address.adr) {|v| select_cc(v)})
    @cc_entry = Gtk::Entry.new
    normal.attach(cc_menu,  0,1,2,3,Gtk::SHRINK,Gtk::SHRINK)
    normal.attach(@cc_entry,1,2,2,3,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK)
    # bcc
    bcc_menu = Gtk::MenuBar.new
    item = Gtk::MenuItem.new("Bcc:        ")
    item.show
    bcc_menu.append(item)
    item.set_submenu(makemenutree(server.address.adr) {|v| select_bcc(v)})
    @bcc_entry = Gtk::Entry.new
    normal.attach(bcc_menu,  0,1,3,4,Gtk::SHRINK,Gtk::SHRINK)
    normal.attach(@bcc_entry,1,2,3,4,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK)
    normal.show_all
    hbox.pack_start(normal,true,true,0)
    # signature
    vbox2 = Gtk::VBox.new(false,0)
    vbox2.show
    # text/base64 file
    scroll = Gtk::ScrolledWindow.new(nil,nil)
    scroll.set_usize(150,-1)
    scroll.show
    @mime_list = Gtk::CList.new(["","Files"])
    @mime_list.column_titles_hide    
    @mime_list.set_row_height(16)
    @mime_list.set_column_width(0,16)
    @mime_list.set_column_width(1,7 * 128)
    @mime_list.set_selection_mode(1)
    @mime_list.show
    @mime_list.signal_connect("select_row") {|w,r,c,b| @mime_selected = r }
    scroll.add_with_viewport(@mime_list)
    vbox2.pack_start(scroll,true,true,0) 
    hbox.pack_start(vbox2,false,false,0)
    vbox.pack_start(hbox,false,false,0)
    # H-separator
    sep = Gtk::HSeparator.new
    sep.show
    vbox.pack_start(sep,false,false,0)
    # expert
    @expert = Gtk::Table.new(2,4,false)
    @expert.border_width(0)
    # from
    from_menu = Gtk::MenuBar.new
    item = Gtk::MenuItem.new("From:       ")
    item.show
    from_menu.append(item)
    @from_entry = Gtk::Entry.new
    @from_entry.set_text(server.rc['smtp']['from'])
    @expert.attach(from_menu,  0,1,0,1,Gtk::SHRINK,Gtk::SHRINK)
    @expert.attach(@from_entry,1,2,0,1,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK)
    # reply_to
    reply_to_menu = Gtk::MenuBar.new
    item = Gtk::MenuItem.new("Reply-to:   ")
    item.show
    reply_to_menu.append(item)
    @reply_to_entry = Gtk::Entry.new
    @expert.attach(reply_to_menu,  0,1,1,2,Gtk::SHRINK,Gtk::SHRINK)
    @expert.attach(@reply_to_entry,1,2,1,2,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK)
    # in_reply_to
    in_reply_to_menu = Gtk::MenuBar.new
    item = Gtk::MenuItem.new("In-reply-to:")
    item.show
    in_reply_to_menu.append(item)
    @in_reply_to_entry = Gtk::Entry.new
    @expert.attach(in_reply_to_menu,  0,1,2,3,Gtk::SHRINK,Gtk::SHRINK)
    @expert.attach(@in_reply_to_entry,1,2,2,3,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK)
    # addition
    addition_menu = Gtk::MenuBar.new
    item = Gtk::MenuItem.new("Addition:   ")
    item.show
    addition_menu.append(item)
    @addition_entry = Gtk::Entry.new
    @expert.attach(addition_menu,  0,1,3,4,Gtk::SHRINK,Gtk::SHRINK)
    @expert.attach(@addition_entry,1,2,3,4,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK)
    if(@expert_mode) then
      @expert.show_all
    end
    vbox.pack_start(@expert,false,false,0)
    # text window
    hbox = Gtk::HBox.new(false,0)
    vadj = Gtk::Adjustment.new(0,0,0,0,0,0)
    @text = Gtk::Text.new(nil,vadj)
    @text.set_editable(true)
    @text.set_usize(82 * 7,25 * 14)
    @text.signal_connect("size_allocate") {|w,a| @text_alloc = a }
    @text.signal_connect_after("key_press_event") {|w,e|
      # text folding
      if(e.keyval == Gdk::GDK_Return) then
	cur = @text.get_point
	ptr = now = cur - 1
	len = 0
	while(ptr != 0 and @text.get_chars(ptr - 1,ptr) != "\n")
	  ptr -= 1
	  len += 1
	end
	if(len != 0) then
	  str = @text.get_chars(ptr,now)
	  text = fold(str,$rc2['foldchar'],$rc['width'] - 2)
	  @text.freeze
	  @text.set_point(now)
	  @text.backward_delete(len)
	  @text.insert_text(text,ptr)
	  @text.set_point(cur)
	  @text.thaw
	end
      end
      true
    }
    vbar = Gtk::VScrollbar.new(vadj)
    hbox.pack_start(@text,true,true,0)
    hbox.pack_start(vbar,false,false,0)
    hbox.show_all
    vbox.pack_start(hbox,true,true,0)
    vbox.show
    @window.add(vbox)
    # dummy text window for clipboard
    @clip = Gtk::Text.new(nil,nil)
    @clip.set_editable(true)
    @clip.hide
    # setup
    case @title
    when "Send"
      setup_send("send",to)
    when "Reply"
      setup_reply("reply")
    when "Transfer"
      setup_transfer("transfer")
    when "Resend"
      setup_resend
    end
    # launch editor
    if($rc['editor'] != "self") then
      launch_editor()
    end
    @window.show
    @subject_entry.grab_focus
  end
  # make menu tree
  def makemenutree(tree)
    menu = Gtk::Menu.new
    menu.show
    tree.each {|v|
      item = Gtk::MenuItem.new(v['nam'])
      item.show
      menu.append(item)
      if(v['cld']) then
	item.set_submenu(makemenutree(v['cld']))
      else
	item.signal_connect("activate",v) {|w,v|
	  yield(v)
	}
      end
    }
    menu
  end
  # busy
  def busy
  end
  # idle
  def idle
  end
  # cut
  def cut
    @text.cut_clipboard()
  end
  # copy
  def copy
    @text.copy_clipboard()
  end
  # paste
  def paste
    @text.paste_clipboard()
  end
  # insert text
  def load
    fs = Gtk::FileSelection.new('Insert')
    fs.show
    fs.show_all
    fs.grab_add
    loop = true
    flag = false
    fs.ok_button.signal_connect("clicked") {
      flag = true
      loop = false
    }
    fs.cancel_button.signal_connect("clicked") {
      flag = false
      loop = false
    }
    while(loop)
      Gtk.main_iteration
    end
    fs.grab_remove
    fs.hide
    if(flag) then
      name = fs.get_filename
      base = File.basename(name)
      open(OS.path(name)) {|fd|
	@text.freeze()
	fd.each_line {|line|
	  @text.insert(nil,nil,nil,line)
	}
	@text.thaw()
      }
    end
    fs.destroy
  end
  # attach MIME part
  def attach
    fs = Gtk::FileSelection.new('Attach')
    fs.show
    fs.show_all
    fs.grab_add
    loop = true
    flag = false
    fs.ok_button.signal_connect("clicked") {
      flag = true
      loop = false
    }
    fs.cancel_button.signal_connect("clicked") {
      flag = false
      loop = false
    }
    while(loop)
      Gtk.main_iteration
    end
    fs.grab_remove
    fs.hide
    if(flag) then
      name = fs.get_filename
      base = File.basename(name)
      open(OS.path(name)) {|fd|
	fd.binmode
	if(binary?(fd.read(512))) then
	  @mime_list.append(["",base])
	  @mime_list.set_pixmap(@mime_list.rows()-1,0,@binary_pix,@binary_msk)
	  @mime_files["[B]"+base] = name
	else
	  @mime_list.append(["",base])
	  @mime_list.set_pixmap(@mime_list.rows()-1,0,@text_pix,@text_msk)
	  @mime_files["[T]"+base] = name
	end
      }
    end
    fs.destroy
  end
  # detach MIME part
  def detach
    if(@mime_selected) then
      base = @mime_list.get_text(@mime_selected,1)
      @mime_list.remove_row(@mime_selected)
      @mime_files.delete_if {|k,v| k == "[B]"+base }
      @mime_files.delete_if {|k,v| k == "[T]"+base }
    end
  end
  # paste with reply mark
  def pasterep
#    @clip.set_point(0)
    @clip.position= 0
    @clip.forward_delete(@clip.get_length)
    @clip.paste_clipboard()
    text = @clip.get_chars(0,@clip.get_length)
    text.each_line {|line|
      @text.insert(nil,nil,nil,$rc['reply']+line)
    }
  end
  # switch expert mode
  def expert
    if(@expart_mode) then
      @expert.hide_all
      @expart_mode = false
      @text.set_usize(@text_alloc.width,@text_alloc.height)
    else
      @expert.show_all
      @expart_mode = true
      @text.set_usize(@text_alloc.width,@text_alloc.height)
    end
  end
  # auto signature
  def auto_signature(name)
    @server.signature.sign.each_index {|i|
      if(@server.signature.sign[i]['nam'] == name) then
	@signature_menu.set_history(i)
	@sign = @server.signature.sign[i]['val']
      end
    }
  end
  # auto where
  def auto_where(name)
    @server.where.each_index {|i|
      if(@server.where[i]['nam'] == name) then
	@direct_menu.set_history(i)
	@where = @server.where[i]['val']
      end
    }
  end
  # template select action
  def select_template(v)
    case @title
    when "Send"
      setup_send(v['nam'],@to)
    when "Reply"
      setup_reply(v['nam'])
    when "Transfer"
      setup_transfer(v['nam'])
    when "Resend"
      setup_resend
    end
  end
  # to select action
  def select_to(v)
    if(@to_entry.get_text == "") then
      @to_entry.set_text(v['val'])
    else
      @to_entry.append_text(","+v['val'])
    end
    auto_signature(v['nam'])
  end
  # cc select action
  def select_cc(v)
    if(@cc_entry.get_text == "") then
      @cc_entry.set_text(v['val'])
    else
      @cc_entry.append_text(","+v['val'])
    end
  end
  def select_bcc(v)
    if(@bcc_entry.get_text == "") then
      @bcc_entry.set_text(v['val'])
    else
      @bcc_entry.append_text(","+v['val'])
    end
  end
  # text
  def text
    @text.get_chars(0,@text.get_length)
  end
  # setup template text
  def setup(tmp)
    hdr = nil
    hdr = @tag.header[0] if(@tag)
    temp = @server.conf[tmp]
    hmode = true
    temp.each_line {|line|
      if(hmode) then
	if(line =~ /^$/) then
	  hmode = false
	else
	  case line
	  when /^Subject: (.*)$/i
	    @subject_entry.set_text($1)
	  when /^To: (.*)$/i
	    @to_entry.set_text($1)
	    auto_signature(@server.address.rev[$1])
	  when /^Cc: (.*)$/i
	    @cc_entry.set_text($1)
	  when /^Bcc: (.*)$/i
	    @bcc_entry.set_text($1)
	  when /^Reply-To: (.*)$/i
	    @reply_to_entry.set_text($1)
	  else
	    @addition_entry.set_text(line)
	  end
	end
      else
	if(hdr) then
	  hdr.each {|k,v|
	    if(line =~ /%#{k}/i) then
	      line.gsub!(/%#{k}/i,v)
	    end
	  }
	  if(line =~ /%nickname/i) then
	    from = hdr['From']
	    if(from =~ /\((.*?)\)/) then
	      nickname = $1
	    else
	      if(from =~ /(.*?)<.*?>/) then
		nickname = $1.strip
	      else
		nickname = @server.address.rev[from.strip]
		if(nickname == nil) then
		  nickname = from
		end
	      end
	    end
	    line.gsub!(/%nickname/i,nickname)
	  end
	end
	@text.insert(nil,nil,nil,line)
      end
    }
  end
  # setup new mail
  def setup_send(temp,to)
    busy()
    @from_entry.set_text(@server.rc['smtp']['from'])
    @bcc_entry.set_text(@server.rc['smtp']['bcc'])
    if(to) then
      @to_entry.set_text(to)
      auto_signature(to)
    end
    @text.freeze
    @text.position= 0
    @text.forward_delete(@text.get_length)
    setup(temp)
    @text.thaw
    @text.grab_focus
    idle()
  end
  # setup_reply
  def setup_reply(temp)
    busy()
    hdr = @tag.header()
    subj = hdr[0]['Subject'].dup
    #begin pached by yasuyuki
    if($CACHE.rc['mlcollapse']) then
      subj.sub!(/#{$rc2['hdrpat']}/i) { "" }
    end
    #end
    subj.sub!(/#{$rc2['repat']}/i) { "" }
    @subject_entry.set_text("Re: "+subj)
    @from_entry.set_text(@server.rc['smtp']['from'])
    if(hdr[0]['Reply-To']) then
      from = hdr[0]['Reply-To'].dup
    else
      from = hdr[0]['From'].dup
    end
    if(from =~ /<(.*?)>/) then
      from = $1
    end
    from.sub!(/(\(.*?\))/) { "" }
    @to_entry.set_text(from.strip)
    auto_signature(@server.address.rev[from.strip])
    @bcc_entry.set_text(@server.rc['smtp']['bcc'])
    # copy Cc:
    if(hdr[0]['Cc']) then
      @cc_entry.set_text(hdr[0]['Cc'])
    end
    @in_reply_to_entry.set_text(hdr[0]['Message-Id'])
    @text.freeze
    @text.position= 0
    @text.forward_delete(@text.get_length)
    setup(temp)
    body = @tag.body(0)
    body.each_line {|line|
      @text.insert(nil,nil,nil,$CACHE.rc['reply']+line)
    }
    @text.thaw
    @text.grab_focus
    idle()
  end
  # setup transfer
  def setup_transfer(temp)
    busy()
    hdr = @tag.header
    subj = hdr[0]['Subject'].dup
    #begin pached by yasuyuki
    if($CACHE.rc['mlcollapse']) then
      subj.sub!(/#{$rc2['hdrpat']}/i) { "" }
    end
    #end
    subj.sub!(/#{$rc2['repat']}/i) { "" }
    @subject_entry.set_text("Fw: "+subj)
    @from_entry.set_text(@server.rc['smtp']['from'])
    @bcc_entry.set_text(@server.rc['smtp']['bcc'])
    @in_reply_to_entry.set_text(hdr[0]['Message-Id'])
    # reply text
    @text.freeze
    @text.position= 0
    @text.forward_delete(@text.get_length)
    setup(temp)
    $CACHE.rc['begin'].each_line {|line|
      @text.insert(nil,nil,nil,line)
    }
    body = @tag.body(0)
    body.each_line {|line|
      @text.insert(nil,nil,nil,line)
    }
    $CACHE.rc['end'].each_line {|line|
      @text.insert(nil,nil,nil,line)
    }
    @text.thaw
    @text.grab_focus
    hdr.each_index {|i|
      if(i != 0) then
	if(hdr[i]['Content-Type']) then
	  name = @tag.save(i)
	  base = File.basename(name)
	  open(OS.path(name)) {|fd|
	    fd.binmode
	    if(binary?(fd.read(512))) then
	      @mime_list.append(["",base])
	      @mime_list.set_pixmap(
		@mime_list.rows()-1,0,@binary_pix,@binary_msk)
	      @mime_files["[B]"+base] = name
	    else
	      @mime_list.append(["",base])
	      @mime_list.set_pixmap(@mime_list.rows()-1,0,@text_pix,@text_msk)
	      @mime_files["[T]"+base] = name
	    end
	  }
	end
      end
    }
    idle()
  end
  # setup resend
  def setup_resend
    busy()
    hdr = @tag.header
    @subject_entry.set_text(hdr[0]['Subject'])
    @from_entry.set_text(@server.rc['smtp']['from'])
    @bcc_entry.set_text(@server.rc['smtp']['bcc'])
    if(@tag.folder.name == @server.config.name) then
      auto_where("config")
      auto_signature("None")
    end
    @in_reply_to_entry.set_text(hdr[0]['In-Reply-To']) if(hdr[0]['In-Reply-To'])
    # reply text
    @text.freeze
    @text.position= 0
    @text.forward_delete(@text.get_length)
    body = @tag.body(0)
    body.each_line {|line|
      @text.insert(nil,nil,nil,line)
    }
    @text.thaw
    @text.grab_focus
    hdr.each_index {|i|
      if(i != 0) then
	if(hdr[i]['Content-Type']) then
	  name = @tag.save(i)
	  base = File.basename(name)
	  open(OS.path(name)) {|fd|
	    fd.binmode
	    if(binary?(fd.read(512))) then
	      @mime_list.append(["",base])
	      @mime_list.set_pixmap(
		@mime_list.rows()-1,0,@binary_pix,@binary_msk)
	      @mime_files["[B]"+base] = name
	    else
	      @mime_list.append(["",base])
	      @mime_list.set_pixmap(@mime_list.rows()-1,0,@text_pix,@text_msk)
	      @mime_files["[T]"+base] = name
	    end
	  }
	end
      end
    }
    idle()
  end
  # send
  def send
    hdr = {}
    hdr['Subject'] = @subject_entry.get_text()
    hdr['From'] = @from_entry.get_text()
    hdr['To'] = @to_entry.get_text()
    hdr['Cc'] = @cc_entry.get_text()
    hdr['Bcc'] = @bcc_entry.get_text()
    hdr['Reply-To'] = @reply_to_entry.get_text()
    hdr['In-Reply-To'] = @in_reply_to_entry.get_text()
    add = @addition_entry.get_text()
    if(add =~ /(.*?):\s+(.*)/) then
      hdr[$1] = $2
    end
    text = @text.get_chars(0,@text.get_length)
    case @where
    when ""
      if(hdr['To'].gsub(/ /,"") != "") then
	busy()
	@server.send(hdr,text,@sign,@mime_files)
	idle()
	quit
      else
	SGDialog.new("no_to_error")
      end
    when "outbox"
      if(hdr['To'].gsub(/ /,"") != "") then
	if(@server.outbox) then
	  busy()
	  @server.outbox.store(hdr,text,@sign,@mime_files)
	  idle()
	  quit
	end
      else
	SGDialog.new("no_to_error")
      end
    when "draft"
      if(@server.draft) then
	busy()
	@server.draft.store(hdr,text,@sign,@mime_files)
	idle()
	quit
      end
    when "config"
      if(@server.config) then
	busy()
	@server.config.store(hdr,text,@sign,@mime_files)
	if(@tag) then
	  @server.config.remove(@tag['uid'])
	end
	@server.getconfig()
	idle()
	quit
      end
    end
  end
  # launch editor
  def launch_editor
    block = true
    n = 0
    while(true)
      file = OS.path(OS::CACHE+"/tmp/#{$$}#{n}.txt")
      if(not test(?e,file)) then
	break
      end
      n += 1
    end
    open(file,"w") {|fd|
      fd.puts "Subject: "+@subject_entry.get_text()
      fd.puts "From: "+@from_entry.get_text()
      fd.puts "To: "+@to_entry.get_text()
      fd.puts "Cc: "+@cc_entry.get_text()
      fd.puts "Bcc: "+@bcc_entry.get_text()
      fd.puts "Reply-To: "+@reply_to_entry.get_text()
      fd.puts "In-Reply-To: "+@in_reply_to_entry.get_text()
      fd.puts "----Do not delete this line----"
      fd.puts @text.get_chars(0,@text.get_length)
      fd.chmod(0600)
    }
    thread = Thread.start {
      PTY.protect_signal {
	fork() {
	  exec($rc['editor'],file)
	}
	Process.wait
      }
      block = false
    }
    while(block)
      Gtk.main_iteration
    end
    open(file) {|fd|
      mode = "header"
      fd.each_line {|line|
	case mode
	when "header"
	  line.chop!
	  if(line =~ /^([\w-]+):\s(.*)$/) then
	    name = $1
	    body = $2
	    case name
	    when "Subject"
	      @subject_entry.set_text(body)
	    when "From"
	      @from_entry.set_text(body)
	    when "To"
	      @to_entry.set_text(body)
	    when "Cc"
	      @cc_entry.set_text(body)
	    when "Bcc"
	      @bcc_entry.set_text(body)
	    when "Reply-To"
	      @reply_to_entry.set_text(body)
	    when "In-Reply-To"
	      @in_reply_to_entry.set_text(body)
	    end
	  else
	    if(line == "----Do not delete this line----") then
	      mode = "body"
	      @text.set_point(0)
	      @text.forward_delete(@text.get_length)
	    end
	  end
	when "body"
	  @text.insert(nil,nil,nil,line)
	end
      }
    }
    File.unlink(file)
  end
  # quit
  def quit
    @window.destroy
  end
end
