#
# config editor
#

class Config_window < Gtk::Window
  def initialize
    super(Gtk::WINDOW_TOPLEVEL)
    set_title("Config")
    set_usize(580,400)
    realize
    widget = Gtk::VBox.new(false,0)
    widget.show
    # tool bar
    tbox = Gtk::HBox.new(false,0)
    tbox.show
    # tool bar
    tbar = Gtk::Toolbar.new(Gtk::ORIENTATION_HORIZONTAL,Gtk::TOOLBAR_ICONS)
    tbar.show
    tbar.set_button_relief(Gtk::RELIEF_NONE)
    tbar.set_space_style(Gtk::Toolbar::SPACE_LINE)
    tbar.set_space_size(5)
    tbar.append_item(nil,$rc2['button']['save'],nil,
      Gtklib.pixmap("save.xpm",window),nil) { save }
    tbar.append_space()
    tbar.append_item(nil,$rc2['button']['copy'],nil,
      Gtklib.pixmap("copy.xpm",window),nil) { copy }
    tbox.pack_start(tbar,false,false,0)
    # right tool bar
    tbar = Gtk::Toolbar.new(Gtk::ORIENTATION_HORIZONTAL,Gtk::TOOLBAR_ICONS)
    tbar.show
    tbar.set_button_relief(Gtk::RELIEF_NONE)
    tbar.set_space_style(Gtk::Toolbar::SPACE_LINE)
    tbar.set_space_size(5)
    tbar.append_item(nil,$rc2['button']['close'],nil,
      Gtklib.pixmap("cancel.xpm",window),nil) { quit }
    tbox.pack_end(tbar,false,false,0)
    widget.pack_start(tbox,false,false,0)
    # text box
    hbox = Gtk::HBox.new(false,0)
    hbox.show
    vadj = Gtk::Adjustment.new(0,0,0,0,0,0)
    vbar = Gtk::VScrollbar.new(vadj)
    vbar.show
    @text = Gtk::Text.new(nil,vadj)
    @text.show
    @text.set_editable(true)
    hbox.pack_start(@text,true,true,0)
    hbox.pack_start(vbar,false,false,0)
    widget.pack_start(hbox,true,true,0)
    add widget
    open(OS.path(OS::RC)) {|fd|
      fd.each_line {|line|
	@text.insert(nil,nil,nil,line)
      }
    }
    show
  end
  # copy
  def copy
    @text.copy_clipboard()
  end
  # save
  def save
    text = @text.get_chars(0,@text.get_length)
    open(OS.path(OS::RC),"w") {|fd|
      fd.write(text)
    }
    destroy
  end
  # quit
  def quit
    destroy
  end
end
