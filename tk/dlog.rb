#
# SGmail entry dialog
#

class MboxEntry < SGFrame
attr :entry
attr :label
  def initialize(top,par,msg)
    super(par)
    borderwidth 5
    relief 'flat'
    @label = SGLabel.new(self,msg) {
      pack('side'=>'left')
    }
    @entry = TkEntry.new(self) {
      borderwidth 1
      width 20
      foreground $tkrc['entry_color'][0]
      background $tkrc['entry_color'][1]
      font FONT::TEXT
      bind "KeyPress-Return",proc { top.quit(true) }
      pack('side'=>'left')
    }
  end
end

class MboxButtons < SGFrame
  def initialize(top,par)
    @top = top
    super(par)
    borderwidth 1
    TkFrame.new(self) {
      borderwidth 5
      ok = TkButton.new(self) {
	text "  OK  "
	relief 'groove'
	borderwidth 4
	foreground $tkrc['button_color'][0]
	font FONT::MENU
	command proc { top.quit(true) }
	bind "Return",proc { top.quit(true) }
	pack('side'=>'left','padx'=>'1m','pady'=>'1m')
      }
      cancel = TkButton.new(self) {
	text "Cancel"
	foreground $tkrc['button_color'][0]
	font FONT::MENU
	command proc { top.quit(false) }
	bind "Return",proc { top.quit(false) }
	pack('side'=>'left','padx'=>'1m','pady'=>'1m')
      }
      pack('side'=>'top')
    }
  end
end

class MboxCreateFrame < SGFrame
attr :entry
  def initialize(top,par)
    super(par)
    borderwidth 1
    @entry = MboxEntry.new(top,self,$rc2['dialog']['mbox_name'])
    @entry.pack('side'=>'top')
  end
end

class MboxCreateDialog < TkToplevel
attr :value
  def initialize(value)
    @value = nil
    @entry = nil
    super()
    title "Create mailbox"
    @frame = MboxCreateFrame.new(self,self)
    @frame.entry.entry.value = value
    buttons = MboxButtons.new(self,self)
    @frame.pack('side'=>'top','fill'=>'both')
    buttons.pack('side'=>'top','fill'=>'x')
    @frame.entry.entry.focus
    withdraw
    update
    x = TkWinfo.rootx($main) + TkWinfo.width($main) / 2
    y = TkWinfo.rooty($main) + TkWinfo.height($main) / 2
    x -= TkWinfo.reqwidth(self) / 2
    y -= TkWinfo.reqheight(self) / 2
    geometry "+#{x}+#{y}"
    deiconify
    grab
    wait_destroy
  end
  def quit(flag)
    if(flag) then
      @value = @frame.entry.entry.value
    end
    destroy
  end
end

class MboxDeleteFrame < SGFrame
attr :entry
  def initialize(top,par)
    super(par)
    borderwidth 1
    @entry = MboxEntry.new(top,self,$rc2['dialog']['mbox_name'])
    @entry.pack('side'=>'top')
  end
end

class MboxDeleteDialog < TkToplevel
attr :value
  def initialize(value)
    @value = nil
    super()
    title "Delete mailbox"
    @frame = MboxDeleteFrame.new(self,self)
    buttons = MboxButtons.new(self,self)
    @frame.pack('side'=>'top','fill'=>'both')
    buttons.pack('side'=>'top','fill'=>'x')
    @frame.entry.entry.value = value
    @frame.entry.entry.focus
    withdraw
    update
    x = TkWinfo.rootx($main) + TkWinfo.width($main) / 2
    y = TkWinfo.rooty($main) + TkWinfo.height($main) / 2
    x -= TkWinfo.reqwidth(self) / 2
    y -= TkWinfo.reqheight(self) / 2
    geometry "+#{x}+#{y}"
    deiconify
    grab
    wait_destroy
  end
  def quit(flag)
    if(flag) then
      @value = @frame.entry.entry.value
    end
    destroy
  end
end

class MboxRenameFrame < SGFrame
attr :entry
  def initialize(top,par)
    super(par)
    borderwidth 1
    @entry = MboxEntry.new(top,self,$rc2['dialog']['mbox_name'])
    @entry.pack('side'=>'top')
  end
end

class MboxRenameDialog < TkToplevel
attr :value
  def initialize(value)
    @value = nil
    @entry = nil
    super()
    title "Rename mailbox"
    @frame = MboxRenameFrame.new(self,self)
    @frame.entry.entry.value = value
    buttons = MboxButtons.new(self,self)
    @frame.pack('side'=>'top','fill'=>'both')
    buttons.pack('side'=>'top','fill'=>'x')
    @frame.entry.entry.focus
    withdraw
    update
    x = TkWinfo.rootx($main) + TkWinfo.width($main) / 2
    y = TkWinfo.rooty($main) + TkWinfo.height($main) / 2
    x -= TkWinfo.reqwidth(self) / 2
    y -= TkWinfo.reqheight(self) / 2
    geometry "+#{x}+#{y}"
    deiconify
    grab
    wait_destroy
  end
  def quit(flag)
    if(flag) then
      @value = @frame.entry.entry.value
    end
    destroy
  end
end

class MboxRename2Frame < SGFrame
attr :entry1
attr :entry2
  def initialize(top,par)
    super(par)
    borderwidth 1
    @entry1 = MboxEntry.new(top,self,$rc2['dialog']['mbox_src'])
    @entry1.pack('side'=>'top')
    @entry2 = MboxEntry.new(top,self,$rc2['dialog']['mbox_dst'])
    @entry2.pack('side'=>'top')
  end
end

class MboxRename2Dialog < TkToplevel
attr :src
attr :dst
  def initialize(src,dst)
    @src = nil
    @dst = nil
    super()
    title "Rename mailbox"
    @frame = MboxRename2Frame.new(self,self)
    @frame.entry1.entry.value = src
    @frame.entry2.entry.value = dst
    buttons = MboxButtons.new(self,self)
    @frame.pack('side'=>'top','fill'=>'both')
    buttons.pack('side'=>'top','fill'=>'x')
    @frame.entry1.entry.focus
    withdraw
    update
    x = TkWinfo.rootx($main) + TkWinfo.width($main) / 2
    y = TkWinfo.rooty($main) + TkWinfo.height($main) / 2
    x -= TkWinfo.reqwidth(self) / 2
    y -= TkWinfo.reqheight(self) / 2
    geometry "+#{x}+#{y}"
    deiconify
    grab
    wait_destroy
  end
  def quit(flag)
    if(flag) then
      @src = @frame.entry1.entry.value
      @dst = @frame.entry2.entry.value
    end
    destroy
  end
end

class PasswdFrame < SGFrame
attr :user
attr :pass
  def initialize(top,par,user)
    super(par)
    borderwidth 1
    @user = MboxEntry.new(top,self,$rc2['dialog']['userid'])
    @user.entry.value = user
    @user.pack('side'=>'top')
    @pass = MboxEntry.new(top,self,$rc2['dialog']['passwd'])
    @pass.entry.show '*'
    @pass.pack('side'=>'top')
  end
end

class PasswdDialog < TkToplevel
attr :u
attr :p
  def initialize(user)
    @u = ""
    @p = ""
    super()
    title "Password"
    @frame = PasswdFrame.new(self,self,user)
    buttons = MboxButtons.new(self,self)
    @frame.pack('side'=>'top','fill'=>'both')
    buttons.pack('side'=>'top','fill'=>'x')
    if(@frame.user.entry.value == "") then
      @frame.user.entry.focus
    else
      @frame.pass.entry.focus
    end
    withdraw
    update
    x = TkWinfo.rootx($main) + TkWinfo.width($main) / 2
    y = TkWinfo.rooty($main) + TkWinfo.height($main) / 2
    x -= TkWinfo.reqwidth(self) / 2
    y -= TkWinfo.reqheight(self) / 2
    geometry "+#{x}+#{y}"
    deiconify
    grab
    wait_destroy
  end
  def quit(flag)
    @u = @frame.user.entry.value
    if(flag) then
      @p = @frame.pass.entry.value
    else
      @p = nil
    end
    destroy
  end
end

class SGDialog < TkToplevel
attr :value
  def initialize(msg,icon="error",button="OK",arg=nil)
    super()
    @value = nil
    top = self
    title OS.toutf8($rc2['dialog'][msg][0])
    SGFrame.new(self) {
      borderwidth 1
      TkLabel.new(self) {
        bitmap icon
        borderwidth 10
        pack('side'=>'left','fill'=>'y')
      }
      TkLabel.new(self) {
	if(arg) then
	  str = format($rc2['dialog'][msg][1],*arg)
	else
	  str = $rc2['dialog'][msg][1]
	end
        text OS.toutf8(str)
        borderwidth 10
        foreground $tkrc['label_color'][0]
        font FONT::MENU
        justify 'left'
        pack('side'=>'right','fill'=>'both')
      }
      pack('side'=>'top','fill'=>'both','expand'=>'yes')
    }
    defv = nil
    SGFrame.new(self) {
      borderwidth 1
      TkFrame.new(self) {
        borderwidth 5
        if(button.kind_of?(Array)) then
          len = 0
          button.each {|v|
            if(v.length > len) then len = v.length end
          }
          f = true
          button.each {|v|
            TkButton.new(self) {
              text OS.toutf8(v)
              width len
              borderwidth 1
              if(f) then
                relief 'groove'
                borderwidth 4
                defv = v
                f = false
              end
              foreground $tkrc['button_color'][0]
              font FONT::MENU
              top.setquit(self,v)
              pack('side'=>'left','padx'=>'1m')
            }
          }
        elsif(button.kind_of?(Hash)) then
          len = 0
          button.each {|k,v|
            if(k.length > len) then len = k.length end
          }
          button.each {|k,v|
            TkButton.new(self) {
              text OS.toutf8(k)
              width len
              borderwidth 1
              if(v == 0) then
                relief 'groove'
                borderwidth 4
                defv = v
              end
              foreground $tkrc['button_color'][0]
              font FONT::MENU
              top.setquit(self,v)
              pack('side'=>'left','padx'=>'1m')
            }
          }
        else
          defv = 0
          TkButton.new(self) {
            text OS.toutf8(button)
            relief 'groove'
            borderwidth 4
            foreground $tkrc['button_color'][0]
            font FONT::MENU
            top.setquit(self,0)
            pack('side'=>'left','padx'=>'1m')
          }
        end
        pack
      }
      pack('side'=>'bottom','fill'=>'x')
    }
    bind "KeyPress-Return",proc { quit(defv) }
    withdraw
    update
    x = TkWinfo.rootx($main) + TkWinfo.width($main) / 2
    y = TkWinfo.rooty($main) + TkWinfo.height($main) / 2
    x -= TkWinfo.reqwidth(self) / 2
    y -= TkWinfo.reqheight(self) / 2
    geometry "+#{x}+#{y}"
    deiconify
    grab
    wait_destroy
  end
  def setquit(btn,v)
    btn.command proc { quit(v) }
    btn.bind "Return",proc { quit(v) }
  end
  def quit(v)
    @value = v
    destroy
  end
end
