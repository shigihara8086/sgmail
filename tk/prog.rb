#
# recv command frame
#

class Progress < SGFrame
attr :mbox
attr :view
  def initialize(top,par)
    super(par)
    borderwidth 1
    @msg = SGButton.new(self,"Idle") {
      width $tkrc['mbox_width']
      padx 2
      pady 2
      foreground $tkrc['biff_nomail'][0]
      background $tkrc['biff_nomail'][1]
      command proc {
	if($main.widget.wlist.folder) then
	  $main.widget.wlist.open($main.widget.wlist.folder.server.inbox)
	end
      }
    }
    @canvas = TkCanvas.new(self) {
      borderwidth 1
      width 1
      height 1
      relief 'sunken'
      pack('side'=>'top','fill'=>'x')
    }
    h = TkWinfo.height(@canvas) - 2
    @bar = TkcRectangle.new(@canvas,2,2,0,0,
      'outline'=>'black',
      'fill'=>$tkrc['pbar_color'][0],'width'=>1)
    @msg.pack('side'=>'left','fill'=>'x')
    @canvas.pack('side'=>'right','fill'=>'both','expand'=>'yes')
  end
  # status
  def status(msg)
    if(msg == nil) then
      all,unseen,recent = $main.widget.wlist.status()
      if(all) then
	msg = format($rc2['button']['biff'],all,unseen,recent)
	if(recent != 0) then
	  @msg.foreground $tkrc['biff_recent'][0]
	  @msg.background $tkrc['biff_recent'][1]
	else
	  if(unseen != 0) then
	    @msg.foreground $tkrc['biff_unseen'][0]
	    @msg.background $tkrc['biff_unseen'][1]
	  else
	    @msg.foreground $tkrc['biff_nomail'][0]
	    @msg.background $tkrc['biff_nomail'][1]
	  end
	end
      else
	msg = ""
	@msg.foreground $tkrc['biff_nomail'][0]
	@msg.background $tkrc['biff_nomail'][1]
      end
      @msg.text(msg)
      @msg.update
      progress(0.0)
      $main.idle
    else
      if(msg) then
	$main.busy
	@msg.foreground $tkrc['biff_nomail'][0]
	@msg.background $tkrc['biff_nomail'][1]
	@msg.text(OS.toutf8(msg))
	@msg.update
      end
    end
  end
  # progress
  def progress(ratio)
    w = TkWinfo.width(@canvas) - 3
    h = TkWinfo.height(@canvas) - 3
    p = w * ratio
    @canvas.coords(@bar,2,2,p,h)
    @canvas.update
  end
end
