#
# SGmail initialization
#

$tkrc = RC.new(OS::LIB+"/rc/tk"+$rc['language']+"."+$rc['encoding'])
$info = "#{OS::PRG} Ver #{$Version}(OS:#{OS::SYS},Ruby:#{VERSION},Tk:#{Tk::TK_VERSION})"

#
# icon class
#

class ICON
  IMAGE = {}
  dir = OS::LIB+"/tk/images"
  dh = Dir.open(OS::path(dir))
  dh.each {|file|
    if(file == "." || file == "..") then next end
    path = dir+"/"+file
    case file
    when /\.gif/
      tag = file.sub(/\.gif/,"")
      IMAGE[tag] = TkPhotoImage.new { file OS.path(path) }
    when /\.xbm/
      tag = file.sub(/\.xbm/,"")
      IMAGE[tag] = TkBitmapImage.new { file OS.path(path) }
    end
  }
end

#
# font class
#

class FONT
  case $rc['language']
  when "ja"
    MENU = TkFont.new(Tk::toUTF8($tkrc['menu_font'][0],'shiftjis'),
	   Tk::toUTF8($tkrc['menu_font'][1],'shiftjis'),$tkrc['menu_font'][2])
    LIST = TkFont.new(Tk::toUTF8($tkrc['list_font'][0],'shiftjis'),
	   Tk::toUTF8($tkrc['list_font'][1],'shiftjis'),$tkrc['list_font'][2])
    TEXT = TkFont.new(Tk::toUTF8($tkrc['text_font'][0],'shiftjis'),
	   Tk::toUTF8($tkrc['text_font'][1],'shiftjis'),$tkrc['text_font'][2])
  when "us"
    MENU = TkFont.new(Tk::toUTF8($tkrc['menu_font'][0],'shiftjis'))
    LIST = TkFont.new(Tk::toUTF8($tkrc['list_font'][0],'shiftjis'))
    TEXT = TkFont.new(Tk::toUTF8($tkrc['text_font'][0],'shiftjis'))
  end
end
