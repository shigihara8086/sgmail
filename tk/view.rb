#
# recv command frame
#

class View_cmdbar < SGFrame
attr :search
  def initialize(top,par,reply)
    @top = top
    super(par)
    borderwidth 1
    if(reply) then
      # reply button
      SGImagebutton.new(self,"reply",$rc2['button']['reply']) {
	command proc { top.reply unless($main.busy?) }
	pack('side'=>'left')
      }
      # transfer button
      SGImagebutton.new(self,"xfer",$rc2['button']['xfer']) {
	command proc { top.transfer unless($main.busy?) }
	pack('side'=>'left')
      }
      # resend button
      SGImagebutton.new(self,"send",$rc2['button']['resend']) {
	command proc { top.resend unless($main.busy?) }
	pack('side'=>'left')
      }
      #
      TkLabel.new(self) {
	image ICON::IMAGE['separater']
	pack('side'=>'left')
      }
    end
    # save
    SGImagebutton.new(self,"save",$rc2['button']['save']) {
      command proc { top.save unless($main.busy?) }
      pack('side'=>'left')
    }
    # print
    SGImagebutton.new(self,"print",$rc2['button']['print']) {
      command proc { top.print unless($main.busy?) }
      pack('side'=>'left')
    }
    # copy
    SGImagebutton.new(self,"copy",$rc2['button']['copy']) {
      command proc { top.body.copy unless($main.busy?) }
      pack('side'=>'left')
    }
    # quit
    if($rc['style'] == "tiny" || reply == false) then
      SGImagebutton.new(self,"cancel",$rc2['button']['close']) {
	command proc { top.par.withdraw unless($main.busy?) }
	pack('side'=>'right')
      }
    end
  end
end

#
# recv header frame
#

class View_header < SGFrame
attr :ybar
attr :list
  def initialize(top,par)
    @top = top
    super(par)
    background 'white'
    @ybar = SGScrollbar.new(self) {
      pack('side'=>'right','fill'=>'y')
    }
    @list = SGText.new(self) {
      foreground $tkrc['header_color'][0]
      background $tkrc['header_color'][1]
      width $tkrc['header_width']
      height $tkrc['header_height']
      wrap 'none'
      state 'disabled'
      pack('side'=>'left','fill'=>'x','expand'=>'yes')
    }
    @cursor = TkTextMarkInsert.new(@list,1.0)
    @ybar.command proc{|idx| @list.yview *idx }
    @list.yscroll proc {|idx| @ybar.set *idx }
    @list.bind "Up",proc { @list.yview 'scroll','-1','units' }
    @list.bind "Down",proc { @list.yview 'scroll','1','units' }
    # wheelmouse by akkey
    @list.bind "ButtonPress-4",proc { @list.yview 'scroll','-1','units' }
    @list.bind "ButtonPress-5",proc { @list.yview 'scroll','1','units' }
    #
  end
  # busy
  def busy
    cursor "watch"
    @list.cursor "watch"
    update
  end
  # idle
  def idle
    cursor ""
    @list.cursor ""
    update
  end
  # clear header
  def clear
    @list.state 'normal'
    @list.delete '0.0','end'
    @list.state 'disabled'
  end
  # show header
  def disp(tag,part)
    if(tag) then
      if(part == 0) then
	h = tag.header[0]
	@list.state 'normal'
	@list.delete '0.0','end'
	@list.insert 'end',OS.toutf8(format("Subject         :%s\n",h['Subject']))
	@list.insert 'end',OS.toutf8(format("From            :%s\n",h['From']))
	@list.insert 'end',OS.toutf8(format("Date            :%s\n",h['Date']))
	@list.insert 'end',OS.toutf8(format("To              :%s\n",h['To']))
	@list.insert 'end',OS.toutf8(format("Message-Id      :%s\n",h['Message-Id']))
	@list.insert 'end',OS.toutf8(format("In-Reply-To     :%s\n",h['In-Reply-To']))
	h.each {|key,val|
	  if(key != 'Received') then
	    case key
	    when 'Subject','Date','From','To','Message-Id','In-Reply-To'
	    else
	      @list.insert 'end',OS.toutf8(format("%-16.16s:%s\n",key,val))
	    end
	  end
	}
	if(h['Received'] != nil) then
	  h['Received'].each {|line|
	    @list.insert 'end',OS.toutf8(format("%-16.16s:%s\n",'Received',line))
	  }
	end
	@cursor.set 1.0
	@list.state 'disabled'
      else
	@list.state 'normal'
	@list.delete '0.0','end'
	h = tag.header[part]
	h.each {|key,val|
	  @list.insert 'end',OS.toutf8(format("%-16.16s:%s\n",key,val))
	}
	@cursor.set 1.0
	@list.state 'disabled'
      end
    end
  end
end

#
# recieve text
#

class View_text < SGText
  def initialize(par)
    super(par)
    foreground $tkrc['viewer_color'][0]
    background $tkrc['viewer_color'][1]
    setgrid 'yes'
    width $tkrc['text_width']
    height $tkrc['text_height']
    state 'disabled'
    font FONT::TEXT
  end
  # serach reg-exp string
  def regexp_search(pattern,start)
    @len = TkVariable.new
    pos = tk_send('search','-regexp','-count',@len,'--',pattern,start)
    length = @len.to_i
    line,char = String(pos).split(/\./)
    if(line == nil || char == nil) then
      return false
    else
      endp = length + char.to_i
      return ["#{line}.#{char}","#{line}.#{endp}"]
    end
  end
  # search fixed string
  def normal_search(pattern,start)
    @len = TkVariable.new
    pos = tk_send('search','-count',@len,'--',pattern,start)
    length = @len.to_i
    line,char = String(pos).split(/\./)
    if(line == nil || char == nil) then
      return false
    else
      endp = length + char.to_i
      return ["#{line}.#{char}","#{line}.#{endp}"]
    end
  end
end

class View_ybar < SGScrollbar
  def initialize(par)
    super(par)
  end
  def get
    ary1 = tk_send('get').split
    ary2 = []
    for i in ary1
      ary2.push number(i)
    end
    ary2
  end
end

class View_body < SGFrame
attr :ybar
attr :text
  def initialize(top,par)
    @top = top
    @par = par
    @findstart = '1.0'
    @findpos = []
    @findtag = nil
    @ontag = false
    @repl_tag = []
    @http_tag = []
    @mail_tag = []
    @find_tag = []
    super(par)
    @ybar = View_ybar.new(self) {
      pack('side'=>'right','fill'=>'y')
    }
    @text = View_text.new(self) {
      pack('side'=>'left','fill'=>'both','expand'=>'yes')
    }
    @cursor = TkTextMarkInsert.new(@text,1.0)
    @ybar.command proc{|idx| @text.yview *idx }
    @text.yscroll proc {|idx| @ybar.set *idx }
    @text.bind "Double-ButtonPress-1",proc { @top.hideview }
    @text.bind "ButtonPress-1",proc { @text.focus }
    @text.bind "Return",proc { @top.hideview }
    @text.bind "Up",proc { @text.yview 'scroll','-1','units' }
    @text.bind "Down",proc { @text.yview 'scroll','1','units' }
    # add by akkey for Wheel mouse 1999/11/27
    @text.bind "ButtonPress-5",proc { @text.yview 'scroll','5','units' }
    @text.bind "ButtonPress-4",proc { @text.yview 'scroll','-5','units' }
    #
    if($tkrc['half_scroll']) then
      @scroll = $tkrc['text_height'] / 2
    else
      @scroll = $tkrc['text_height'] - 2
    end
    @text.bind "space",proc {
      pos = @ybar.get
      if(pos[1] == 1.0) then
        top.findnext
      else
        @text.yview 'scroll',@scroll,'units'
      end
    }
  end
  # busy
  def busy
    cursor "watch"
    @text.cursor "watch"
  end
  # idle
  def idle
    cursor ""
    @text.cursor "xterm"
  end
  # send mail
  def send(to)
    @par.send(to)
  end
  # copy
  def copy
    begin
      sel = TkTextTagSel.new(@text)
      text = @text.get(sel.first,sel.last)
      TkClipboard.set(text)
    rescue
    end
  end
  # clear mail text
  def clear
    @text.state 'normal'
    @text.delete '0.0','end'
    @text.state 'disabled'
  end
  # show recieved mail text
  def disp(tag,part)
    body = tag.body(part)
    @text.state 'normal'
    @text.delete '0.0','end'
    @text.insert 'end',OS.toutf8(body)
    if(@findtag != nil) then
      @findtag.remove(@findpos[0],@findpos[1])
    end
    @findstart = '1.0'
    @findtag = nil
    @cursor.set 1.0
    @sp = []
    @ep = []
    # search reply lines
    @repl_tag.each_index {|i|
      if(@repl_tag[i]) then
        @repl_tag[i].destroy
      end
    }
    @repl_tag = []
    @start = '1.0'
    count = 0
    while(
      @pos = @text.regexp_search(OS::toutf8($rc2['replypat']),@start))
      if(count != 0) then
        if(@sp[0] == @pos[0]) then
          break
        end
      end
      @sp[count] = @pos[0]
      @ep[count] = @pos[1]
      @repl_tag[count] = TkTextTag.new(@text)
      @repl_tag[count].add(@pos[0],@pos[1])
      @repl_tag[count].configure(
        'foreground'=>$tkrc['reply_color'][0],
        'background'=>$tkrc['viewer_color'][1])
      @start = @pos[1]
      count += 1
    end
    # search URL
    @http_tag.each_index {|i|
      if(@http_tag[i]) then
        @http_tag[i].destroy
      end
    }
    @http_tag = []
    @http = []
    @start = '1.0'
    count = 0
    while(
    @pos = @text.regexp_search(OS::toutf8($rc2['urlpat']),@start))
      if(count != 0) then
        if(@sp[0] == @pos[0]) then
          break
        end
      end
      @sp[count] = @pos[0]
      @ep[count] = @pos[1]
      @http_tag[count] = TkTextTag.new(@text)
      @http_tag[count].add(@pos[0],@pos[1])
      @http_tag[count].configure(
        'foreground'=>$tkrc['url_color'][0],
        'background'=>$tkrc['viewer_color'][1])
      @http[count] = OS::fromutf8(@text.get(@sp[count],@ep[count]))
      @start = @pos[1]
      count += 1
    end
    @http_tag.each_index {|i|
      @http_tag[i].bind "Enter",proc {
        @ontag = true
        if(@http_tag[i]) then
          @http_tag[i].configure('background'=>'lightgray')
        end
        @text.cursor ''
      }
      @http_tag[i].bind "Leave",proc {
        if(@http_tag[i]) then
          @http_tag[i].configure('background'=>$tkrc['viewer_color'][1])
        end
        @text.cursor 'xterm'
        @ontag = false
      }
      @http_tag[i].bind "ButtonPress-1",proc {
	OS.netscape($rc['html'],"#{@http[i]}")
      }
    }
    # search mail address
    @mail_tag.each_index {|i|
      if(@mail_tag[i]) then
        @mail_tag[i].destroy
      end
    }
    @mail_tag = []
    @mail = []
    @start = '1.0'
    count = 0
    while(
      @pos = @text.regexp_search(OS::toutf8($rc2['mailpat']),@start))
      if(count != 0) then
        if(@sp[0] == @pos[0]) then
          break
        end
      end
      @sp[count] = @pos[0]
      @ep[count] = @pos[1]
      @mail_tag[count] = TkTextTag.new(@text)
      @mail_tag[count].add(@pos[0],@pos[1])
      @mail_tag[count].configure(
        'foreground'=>$tkrc['mailto_color'][0],
        'background'=>$tkrc['viewer_color'][1])
      @mail[count] = OS::fromutf8(@text.get(@sp[count],@ep[count]))
      @start = @pos[1]
      count += 1
    end
    @mail_tag.each_index {|i|
      @mail_tag[i].bind "Enter",proc {
        @ontag = true
        if(@mail_tag[i]) then
          @mail_tag[i].configure('background'=>'lightgray')
        end
        @text.cursor ''
      }
      @mail_tag[i].bind "Leave",proc {
        if(@mail_tag[i]) then
          @mail_tag[i].configure('background'=>$tkrc['viewer_color'][1])
        end
        @text.cursor 'xterm'
        @ontag = false
      }
      @mail_tag[i].bind "ButtonPress-1",proc { send("#{@mail[i]}") }
    }
    @text.state 'disabled'
  end
  # seardch text
  def search(pat)
    # search URL
    @find_tag.each_index {|i|
      if(@find_tag[i]) then
        @find_tag[i].destroy
      end
    }
    @find_tag = []
    @find = []
    @start = '1.0'
    count = 0
    while(@pos = @text.normal_search(OS::toutf8(pat),@start))
      if(count != 0) then
        if(@sp[0] == @pos[0]) then
          break
        end
      end
      @sp[count] = @pos[0]
      @ep[count] = @pos[1]
      @find_tag[count] = TkTextTag.new(@text)
      @find_tag[count].add(@pos[0],@pos[1])
      @find_tag[count].configure(
	'foreground'=>$tkrc['search_color'][0],
	'background'=>$tkrc['search_color'][1])
      @find[count] = OS::fromutf8(@text.get(@sp[count],@ep[count]))
      @start = @pos[1]
      count += 1
    end
  end
end

#
# MIME icon frame
#

class View_mime < SGFrame
  attr :mime
  attr :xbar
  def initialize(top,par)
    @top = top
    @button = []
    super(par)
    newtext()
    @text.state 'disabled'
  end
  # busy
  def busy
    cursor "watch"
  end
  # idle
  def idle
    cursor ""
  end
  # make new text area
  def newtext
    @xbar = SGScrollbar.new(self) {
      orient 'horizontal'      
      pack('side'=>'bottom','fill'=>'x')
    }
    @text = TkText.new(self) {
      height 3
      wrap 'none'
      cursor ""
      foreground $tkrc['mime_color'][0]
      background $tkrc['mime_color'][1]
      font FONT::TEXT
      pack('side'=>'top','fill'=>'x','expand'=>'yes')
    }
    @xbar.command proc{|idx| @text.xview *idx }
    @text.xscroll proc {|idx| @xbar.set *idx }
  end
  # save to file
  def save_file(imap,uid,part,header)
  end
  # process bind to button
  def buttonproc(button,tag,part)
    button.command proc {
      hdr = tag.header
      case hdr[part]['Content-Type']
      when /^text\/html/i
	file = tag.save(part)
	OS.netscape($rc['html'],file)
      when /^image\//i
	file = tag.save(part)
	OS.netscape($rc['image'],file)
      when /^text\//i,/^message\//i
	view = View_attach.new
	view.disp(tag,part)
      else
	if(hdr[part]['Content-Disposition'] =~ /filename\s*=\s*\"(.*?)\"/) then
	  name = $1
	else
	  if(hdr[part]['Content-Type'] =~ /name=\"(.*?)\"/) then
	    name = $1
	  else
	    name = "Untitled.txt"
	  end
	end
	file,ext = name.split(/\./,2)
	if(ext) then
	  name = Tk.getSaveFile(
	    'filetypes'=>"{{Text Files} {."+ext+"}} {{All Files} {*}}",
            'initialdir'=>OS::HOME,
            'initialfile'=>file+"."+ext,
            'title'=>$rc2['dialog']['save'][0])
	else
	  name = Tk.getSaveFile(
            'filetypes'=>"{{All Files} {*}}",
            'initialdir'=>OS::HOME,
            'initialfile'=>file,
            'title'=>$rc2['dialog']['save'][0])
	end
	if(name != "") then
	  file = tag.save(part,name)
	end
      end
    }
  end
  # clear MIME icons
  def clear
    @text.unpack()
    @xbar.unpack()
    newtext()
    @text.state 'disabled'
  end
  # disp MIME icons
  def disp(tag)
    @tag = tag
    @text.unpack()
    @xbar.unpack()
    newtext()
    hdr = tag.header
    hdr.each_index {|i|
      if(i == 0) then next end
      if(hdr[i]['Content-Type'] == nil) then break end
      @button[i] = TkButton.new(self) {
        borderwidth 0
        relief 'flat'
        takefocus 0
        pack('side'=>'left')
      }
      if(hdr[i]['Content-Type']) then
        type,charset = hdr[i]['Content-Type'].split(/;/)
      else
        type = "text/unknown"
      end
      case type
      when /^text\/html/
        @button[i].image ICON::IMAGE['html']
      when /^text\//,/^message\//
        @button[i].image ICON::IMAGE['paper']
      when /^image\//
        @button[i].image ICON::IMAGE['image']
      else
        @button[i].image ICON::IMAGE['unknown']
      end
      TkTextWindow.new(@text,'end','window'=>@button[i])
      if(hdr[i]['Content-Type'] =~ /name=\"(.*?)\"/) then
        name = $1
        @text.insert 'end',OS.toutf8(name)
      else
        name = "unknown"
        @text.insert 'end',OS.toutf8(type)
      end
      buttonproc(@button[i],tag,i)
    }
    @text.state 'disabled'
  end
end

#
# mail body (frame mode)
#

class View_frame < SGFrame
attr :par
attr :body
  def initialize(top,par,have_mime = true)
    super(par)
    @par = par
    @have_mime = have_mime
    @tag = nil
    @cbar = View_cmdbar.new(self,self,@have_mime)
    @header = View_header.new(top,self)
    @body = View_body.new(top,self)
    @mime = View_mime.new(top,self) if(@have_mime)
    @cbar.pack('side'=>'top','fill'=>'x')
    @header.pack('side'=>'top','fill'=>'x')
    @body.pack('side'=>'top','fill'=>'both','expand'=>'yes')
    @mime.pack('side'=>'top','fill'=>'x') if(@have_mime)
  end
  # busy
  def busy
    @header.busy
    @body.busy
    @mime.busy
  end
  # idle
  def idle
    @header.idle
    @body.idle
    @mime.idle
  end
  # clear mail view
  def clear
    @header.clear()
    @body.clear()
    @mime.clear() if(@have_mime)
  end
  # show mail view
  def disp(tag,part)
    @tag = tag
    @header.disp(tag,part)
    @body.disp(tag,part)
    @mime.disp(tag) if(@have_mime)
    @body.text.focus
  end
  # send
  def send(to)
    send = Send.new("Send",@tag.folder.server,@tag,to)
  end
  # reply
  def reply
    send = Send.new("Reply",@tag.folder.server,@tag,nil)
  end
  # transfer
  def transfer
    send = Send.new("Transfer",@tag.folder.server,@tag,nil)
  end
  # resend
  def resend
    send = Send.new("Resend",@tag.folder.server,@tag,nil)
  end
  # save
  def save
    file = "Untitled"
    ext = "txt"
    name = Tk.getSaveFile(
      'filetypes'=>"{{Text Files} {."+ext+"}} {{All Files} {*}}",
      'initialdir'=>OS::HOME,
      'initialfile'=>file+"."+ext,
      'title'=>$rc2['dialog']['save'][0])
    if(name != "") then
      fd = open(OS.path(name),"w")
      fd.write(OS.fromutf8(@body.text.value))
      fd.close
    end
  end
  # print
  def print
    busy()
    hdr = @tag.header
    if(fd = open("|"+$rc['lpr'],"w")) then
      fd.puts format("Subject: %s\n",hdr[0]['Subject'])
      fd.puts format("From:    %s\n",hdr[0]['From'])
      fd.puts format("Date:    %s\n",hdr[0]['Date'])
      fd.puts format("To:      %s\n",hdr[0]['To'])
      fd.puts ""
      @tag.body(0).each_line {|line|
	fd.puts(line)
      }
      fd.close
    end
    idle()
  end
  # search
  def search(pat)
    @body.search(pat)
  end
end

#
# mail body (toplevel mode)
#

class View_window < TkToplevel
  def initialize(top)
    super()
    @top = top
    @hide = true
    title("Mail text")
    bind "Destroy",proc { top.remake }
    @view = View_frame.new(top,self)
    @view.pack('side'=>'top','fill'=>'both','expand'=>'yes')
    withdraw
  end
  # busy
  def busy
    @view.body.busy
  end
  # idle
  def idle
    @view.body.idle
  end
  # clear mail view
  def clear
    @view.clear
  end
  # show mail view
  def disp(tag,part)
    @view.disp(tag,part)
    if(@hide) then
      x = TkWinfo.rootx($main) - 4
      y = TkWinfo.rooty($main) + TkWinfo.height($main) + 4
      geometry "+#{x}+#{y}"
      @hide = false
    end
    deiconify
  end
  # search
  def search(pat)
    @view.search(pat)
  end
end

#
# mail body (toplevel mode)
#

class View_attach < TkToplevel
  def initialize
    super()
    title("Mail text")
    @view = View_frame.new(self,self,false)
    @view.pack('side'=>'top','fill'=>'both','expand'=>'yes')
  end
  # clear mail view
  def clear
    @view.clear
  end
  # show mail view
  def disp(tag,part)
    @view.disp(tag,part)
  end
  # withdraw
  def withdraw
    if($help) then
      $help.destroy
      $help = nil
    end
    $main.settimer(0,nil)
    destroy
  end
end
