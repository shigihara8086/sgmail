#
# logo window
#

class Logo < TkFrame
  def initialize
    $tkroot = TkRoot.new
    super($tkroot)
    msg  = "Ver #{$Version} (C) by SHIGIHARA,Atsuhiro."
    @banner = TkLabel.new(self) {
      image ICON::IMAGE['banner']
      pack
    }
    @text = TkLabel.new(self) {
      background "white"
      font FONT::TEXT
      text msg
      pack('fill'=>'x','expand'=>'yes')
    }
    pack
    tkroot = TkRoot.new
    tkroot.title "#{OS::PRG} Ver #{$Version}"
  end
end
