#
# recv command frame
#

class List_cmdbar < SGFrame
attr :target
attr :text
  def initialize(top,par)
    @top = top
    super(par)
    borderwidth 1
    # set refile
    SGImagebutton.new(self,"mark",$rc2['button']['refile']) {
      command proc { par.list.refile unless($main.busy?) }
      pack('side'=>'left')
    }
    # set refile with fource
    SGImagebutton.new(self,"fource",$rc2['button']['fource']) {
      command proc { par.list.refile_fource unless($main.busy?) }
      pack('side'=>'left')
    }
    # reset refile
    SGImagebutton.new(self,"unmark",$rc2['button']['unrefile']) {
      command proc { par.list.refile_undo unless($main.busy?) }
      pack('side'=>'left')
    }
    # execute refile
    SGImagebutton.new(self,"move",$rc2['button']['dorefile']) {
      command proc { par.list.refile_exec unless($main.busy?) }
      pack('side'=>'left')
    }
    # seen all mail
    SGImagebutton.new(self,"seen",$rc2['button']['seen']) {
      command proc { par.list.seen_all unless($main.busy?) }
      pack('side'=>'left')
    }
    # sep
    TkLabel.new(self) { image ICON::IMAGE['separater']; pack('side'=>'left') }
    # search target
    w = [
      { 'nam'=>"Subject",'val'=>"SUBJECT" },
      { 'nam'=>"From",'val'=>"FROM" },
      { 'nam'=>"Body",'val'=>"BODY" }
    ]
    @target = w[0]['val']
    SGSelectbutton.new(self,w,false,proc {|k,v| @target = v }) {
      width 8
      pack('side'=>'left')
    }
    # search string
    @text = TkEntry.new(self) {
      borderwidth 1
      foreground $tkrc['entry_color'][0]
      background $tkrc['entry_color'][1]
      font FONT::MENU
      width 12
      pack('side'=>'left')
    }
    # search
    SGImagebutton.new(self,"search",$rc2['button']['search']) {
      command proc { par.list.search unless($main.busy?) }
      pack('side'=>'left')
    }
    # quit
    SGImagebutton.new(self,"cancel",$rc2['button']['exit']) {
      command proc { top.quit }
      pack('side'=>'right')
    }
    # sep
    TkLabel.new(self) { image ICON::IMAGE['separater']; pack('side'=>'right') }
    # edit .sgmailrc
    SGImagebutton.new(self,"config",$rc2['button']['sgrc_conf']) {
      command proc { $main.widget.wmbox.rc_config unless($main.busy?) }
      pack('side'=>'right')
    }
  end
end

#
# popup tree
#

class List_PopupTree < TkMenu
  def initialize(x,y,tree,cb)
    super()
    @tree = tree
    @cb = cb
    tearoff 'off'
    borderwidth 1
    foreground $tkrc['menu_color'][0]
    background $tkrc['menu_color'][1]
    font FONT::MENU
    setmenu_sub(self,@tree)
    post(x - 10,y - 10)
    activate 0
    if(OS::SYS != "win") then
      focus
      grab
    end
  end
  def setmenu_subsub(menu,name,value)
    menu.add 'command','label'=>OS.toutf8(name),'command'=>proc {
      @cb.call(name,value) if(@cb)
    }
  end
  def setmenu_sub(menu,tree)
    tree.each {|item|
      if(item['cld'] and item['cld'] != []) then
        child = TkMenu.new(menu) {
          tearoff 'off'
          borderwidth 1
          foreground $tkrc['menu_color'][0]
          background $tkrc['menu_color'][1]
          font FONT::MENU
        }
        menu.add 'cascade','label'=>OS.toutf8(item['nam']),'menu'=>child
        setmenu_sub(child,item['cld'])
      else
        setmenu_subsub(menu,item['nam'],item['val'])
      end
    }
  end
end

#
# recv subject list frame
#

class List_box < SGFrame
attr :list
  def initialize(top,par,width,height,cb)
    @top = top
    super(par)
    # button
    @button = SGButton.new(self,"") {
      relief 'raised'
      padx 2
      pady 2
      takefocus 0
      anchor 'w'
      command cb
      pack('side'=>'top','fill'=>'x')
    }
    # From list
    @list = SGListbox.new(self) {
      selectborderwidth 0
      foreground $tkrc['from_color'][0]
      background $tkrc['from_color'][1]
      selectforeground $tkrc['select_color'][0]
      selectbackground $tkrc['select_color'][1]
      width width
      height height
      if($rc['style'] == "tiny") then
	pack('side'=>'top','fill'=>'both','expand'=>'yes')
      else
	pack('side'=>'top','fill'=>'both')
      end
    }
    @xbar = SGScrollbar.new(self) {
      orient 'horizontal'
      takefocus 0
      pack('side'=>'bottom','fill'=>'x')
    }
    @xbar.command proc {|idx| @list.xview *idx }
    @list.xscroll proc {|idx| @xbar.set *idx }
  end
  # set button text
  def title(str)
    @button.text str
  end
end

class List_list < SGFrame
attr :focused
attr :folder
  def initialize(top,par)
    super(par)
    @top = top
    @par = par
    @now = nil
    @focused = nil
    @folder = nil
    @tags = nil
    @moveto = "trash"
    @unseen = 0
    @from_box = nil
    @date_box = nil
    @list_box = nil
    @search_target = nil
    @search_list = []
    @search_string = nil
    @ybar = SGScrollbar.new(self) {
      orient 'vertical'
      takefocus 0
      pack('side'=>'right','fill'=>'y')
    }
    mbheader = Proc.new {
      if(@focused) then
	@now = @focused.curselection[0]
	if(@now) then
#	  $main.widget.wheader.disp(@tags[@now])
	end
      end
    }
    mbtext = Proc.new {
      if(@focused) then
	@now = @focused.curselection[0]
	if(@now) then
	  if(@tags[@now]['see'] == false) then
	    @tags[@now]['see'] = true
	    @unseen -= 1
	    update(@now)
	    $main.widget.wprog.status(nil)
	  end
	  $main.widget.wview.disp(@tags[@now],0)
	end
      end
    }
    from_button = Proc.new {
      if($CACHE.rc['sort'] != "from") then
	if($main.busy? == false and @folder != nil) then
	  @from_box.title("[From:#{@moveto}]")
	  @date_box.title("Date")
	  @list_box.title("Subject")
	  folder = @folder
	  $CACHE.sort("from")
	  open(folder)
	end
      end
    }
    date_button = Proc.new {
      if($CACHE.rc['sort'] != "date") then
	if($main.busy? == false and @folder != nil) then
	  @from_box.title("From:#{@moveto}")
	  @date_box.title("[Date]")
	  @list_box.title("Subject")
	  folder = @folder
	  $CACHE.sort("date")      
	  open(folder)
	end
      end
    }
    list_button = Proc.new {
      if($CACHE.rc['sort'] != "msgid") then
	if($main.busy? == false and @folder != nil) then
	  @from_box.title("From:#{@moveto}")
	  @date_box.title("Date")
	  @list_box.title("[Subject]")
	  folder = @folder
	  $CACHE.sort("msgid")
	  open(folder)
	end
      end
    }
    set_moveto = Proc.new {|nam,val|
      if(val) then
	@moveto = val.box
	if($CACHE.rc['sort'] == "from") then
	  @from_box.title("[From:#{@moveto}]")
	else
	  @from_box.title("From:#{@moveto}")
	end
      end
    }
    mbmove_mouse = Proc.new {
      if(@focused) then
	@now = @focused.curselection[0]
	if(@now) then
	  if(@tags[@now]['mov']) then
	    @tags[@now]['mov'] = nil
	  else
	    @tags[@now]['mov'] = @moveto
	  end
	  update(@now)
	end
      end
    }
    mbmove_key = Proc.new {
      if(@focused) then
	@now = @focused.curselection[0]
	if(@now) then
	  if(@tags[@now]['mov']) then
	    @tags[@now]['mov'] = nil
	  else
	    @tags[@now]['mov'] = @moveto
	  end
	  update(@now)
	  last = @tags.size
	  if(@now < last - 1) then
	    @now += 1
	    moveto = Float(@now) / Float(last)
	    @from.yview 'moveto',moveto
	    @date.yview 'moveto',moveto
	    @list.yview 'moveto',moveto
	    @from.selection_clear(0,'end')
	    @from.selection_set @now,@now
	    @from.activate @now
#	    $main.widget.wheader.disp(@tags[@now])
	  end
	end
      end
    }
    @from_box = List_box.new(top,self,
      $tkrc['from_width']+3,$tkrc['list_height'],from_button)
    if($CACHE.rc['sort'] == "from") then
      @from_box.title("[From:#{@moveto}]")
    else
      @from_box.title("From:#{@moveto}")
    end
    @from_box.pack('side'=>'left','fill'=>'y')
    @from = @from_box.list
    @from.bind "Double-ButtonRelease-1",mbmove_mouse
    @from.bind "Return",mbmove_key
    @from.bind "KeyRelease-Up",mbheader
    @from.bind "KeyRelease-Down",mbheader
    # wheelmouse by akkey
    @from.bind "ButtonPress-4",proc {
      @date.yview 'scroll','-1','units'
      @list.yview 'scroll','-1','units'
    }
    @from.bind "ButtonPress-5",proc {
      @date.yview 'scroll','1','units'
      @list.yview 'scroll','1','units'
    }
    #
    @from.bind "ButtonRelease-1",proc {|evt|
      @from.focus
      @focused = @from
      nst = @from.nearest(evt)
      @from.selection_clear(0,'end')
      @from.selection_set(nst,nst)
      @from.activate(nst)
      @now = nst
      if(@tags) then
#	$main.widget.wheader.disp(@tags[@now])
      end
    },"%y"
    @from.bind "ButtonPress-3",proc {|x,y|
      if(@folder and @tags.size != 0) then
	$main.widget.wmbox.list.tree.each {|v|
	  if(v['nam'] == @folder.server.name) then
	    List_PopupTree.new(x,y,v['cld'],set_moveto)
	  end
	}
      end
    },"%X %Y"
    @from.bind "FocusIn",proc {
      @focused = @from
      if(@now != nil) then
        @from.selection_anchor(@now)
	@from.selection_clear(0,'end')
        @from.selection_set(@now,@now)
        @from.activate(@now)
      end
    }
    @from.bind "FocusOut",proc {
      @now = @from.curselection[0]
    }
    @date_box = List_box.new(top,self,10,$tkrc['list_height'],date_button)
    if($CACHE.rc['sort'] == "date") then
      @date_box.title("[Date]")
    else
      @date_box.title("Date")
    end
    @date_box.pack('side'=>'left','fill'=>'y')
    @date = @date_box.list
    @date.bind "Double-ButtonRelease-1",mbtext
    @date.bind "Return",mbtext
    @date.bind "KeyRelease-Up",mbheader
    @date.bind "KeyRelease-Down",mbheader
    # wheelmouse by akkey
    @date.bind "ButtonPress-4",proc {
      @from.yview 'scroll','-1','units'
      @list.yview 'scroll','-1','units'
    }
    @date.bind "ButtonPress-5",proc {
      @from.yview 'scroll','1','units'
      @list.yview 'scroll','1','units'
    }
    #
    @date.bind "ButtonRelease-1",proc {|evt|
      @date.focus
      @focused = @date
      nst = @date.nearest(evt)
      @date.selection_clear(0,'end')
      @date.selection_set(nst,nst)
      @date.activate(nst)
      @now = nst
      if(@tags) then
#	$main.widget.wheader.disp(@tags[@now])
      end
    },"%y"
#    @date.bind "ButtonPress-3",proc {|x,y|
#      SGColorPopup.new(x,y,'from_color',@date)
#    },"%X %Y"
    @date.bind "FocusIn",proc {
      @focused = @date
      if(@now != nil) then
        @date.selection_anchor(@now)
	@date.selection_clear(0,'end')
        @date.selection_set(@now,@now)
        @date.activate(@now)
      end
    }
    @date.bind "FocusOut",proc {
      @now = @date.curselection[0]
    }
    @list_box = List_box.new(top,self,36,$tkrc['list_height'],list_button)
    if($CACHE.rc['sort'] == "msgid") then
      @list_box.title("[Subject]")
    else
      @list_box.title("Subject")
    end
    @list_box.pack('side'=>'left','fill'=>'both','expand'=>'yes')
    @list = @list_box.list
    @list.bind "Double-ButtonRelease-1",mbtext
    @list.bind "Return",mbtext
    @list.bind "KeyRelease-Up",mbheader
    @list.bind "KeyRelease-Down",mbheader
    # wheelmouse by akkey
    @list.bind "ButtonPress-4",proc {
      @from.yview 'scroll','-1','units'
      @date.yview 'scroll','-1','units'
    }
    @list.bind "ButtonPress-5",proc {
      @from.yview 'scroll','1','units'
      @date.yview 'scroll','1','units'
    }
    #
    @list.bind "ButtonRelease-1",proc {|evt|
      @list.focus
      @focused = @list
      nst = @list.nearest(evt)
      @list.selection_clear(0,'end')
      @list.selection_set(nst,nst)
      @list.activate(nst)
      @now = nst
      if(@tags) then
#	$main.widget.wheader.disp(@tags[@now])
      end
    },"%y"
#    @list.bind "ButtonPress-3",proc {|x,y|
#      SGColorPopup.new(x,y,'subject_color',@list)
#    },"%X %Y"
    @list.bind "FocusIn",proc {
      @focused = @list
      if(@now != nil) then
        @list.selection_anchor(@now)
	@list.selection_clear(0,'end')
        @list.selection_set(@now,@now)
        @list.activate(@now)
      end
    }
    @list.bind "FocusOut",proc {
      @now = @list.curselection[0]
    }
    # sync scroll point
    @ybar.command proc {|idx|
      @from.yview *idx
      @date.yview *idx
      @list.yview *idx
    }
    @from.yscroll proc {|idx|
      @ybar.set *idx
      @date.yview 'moveto',idx[0]
      @list.yview 'moveto',idx[0]
    }
    @date.yscroll proc {|idx|
      @ybar.set *idx
      @from.yview 'moveto',idx[0]
      @list.yview 'moveto',idx[0]
    }
    @list.yscroll proc {|idx|
      @ybar.set *idx
      @from.yview 'moveto',idx[0]
      @date.yview 'moveto',idx[0]
    }
  end
  # busy
  def busy
    cursor "watch"
    @from.cursor "watch"
    @date.cursor "watch"
    @list.cursor "watch"
  end
  # idle
  def idle
    cursor ""
    @from.cursor ""
    @date.cursor ""
    @list.cursor ""
  end
  # all/unseen/recent
  def status
    if(@folder) then
      [@tags.size,@unseen,@folder.server.recent]
    else
      [nil,nil,nil]
    end
  end
  # close
  def close
    if(@folder) then
      $main.busy()
#      $main.widget.wheader.clear
      $main.widget.wview.clear
      @from.delete '0','end'
      @date.delete '0','end'
      @list.delete '0','end'
      @folder.lclose
      $main.idle()
    end
    
  end
  # folder list
  def open(folder)
    $main.busy()
    close
    @folder = folder
    mlpat = $rc2['mlpat']
    @tags = []
    @now = 0
    @search_string = nil
    find = false
    i = 0
    # if inbox then send outbox mail
    if(@folder.box == "" and @folder.server.outbox != nil) then
      cnt = 0
      outbox = @folder.server.outbox
      outbox.lopen
      outbox.each_mail {|tag|
	if(cnt == 0) then
	  dlg = SGDialog.new("send_outbox","info",["OK","Cancel"])
	  if(dlg.value != "OK") then
	    break
	  end
	end
	hdr = tag.header()
	bdy = tag.body(0)
	files = {}
	hdr.each_index {|i|
	  if(i != 0) then
	    if(hdr[i]['Content-Type']) then
	      name = tag.save(i)
	      base = File.basename(name)
	      File.open(name) {|fd|
		if(Binary?.binary?(fd.read(512))) then
		  files["[B]"+base] = name
		else
		  files["[T]"+base] = name
		end
	      }
	    end
	  end
	}
	@folder.server.send(hdr[0],bdy,nil,files)
	tag['mov'] = "trash"
	tag.move
	cnt += 1
      }
      outbox.lclose
    end
    # folder list
    @folder.lopen
    $main.widget.wprog.status("Disp...")
    @folder.each_mail {|tag|
      @tags << tag
      see = tag['see'] ? "[O]" : "[-]"
      if(tag['mov']) then
	from = ">>"+tag['mov']
      else
	from = tag['from']
        rev = folder.server.address.rev[from]
	from = rev if(rev)
      end
      @from.insert 'end',OS.toutf8(format("%s%s",see,from))
      @date.insert 'end',OS.toutf8(tag['jst'])
      subj = tag['subject']
      if(folder.box != "" and subj =~ /#{mlpat}/) then
	subj = $2
      end
      @list.insert 'end',
	OS.toutf8(format("%s%s"," " * tag['ind'],subj))
      if(tag['see'] == false) then
	if(find == false) then
	  find = true
	  @now = i
	end
      end
      if(!find) then @now = i end
      i += 1
    }
    if(i != 0) then
      moveto = @now.to_f/i.to_f
      @from.yview 'moveto',moveto
      @date.yview 'moveto',moveto
      @list.yview 'moveto',moveto
      @list.selection_clear(0,'end')
      @list.selection_set @now,@now
      @list.activate @now
#      $main.widget.wheader.disp(@tags[@now])
    end
    @list.focus
    @unseen = 0
    @tags.each {|tag| @unseen += 1 if(not tag['see']) }
    $main.widget.wprog.status(nil)
    $main.idle()
  end
  # update subject list entry
  def update(i)
    see = @tags[i]['see'] ? "[O]" : "[-]"
    if(@tags[i]['mov']) then
      from = ">>"+@tags[i]['mov']
    else
      from = @tags[i]['from']
      rev = @folder.server.address.rev[from]
      from = rev if(rev)
    end
    @from.insert i,OS.toutf8(format("%s%s",see,from))
    @from.delete i + 1,i + 1
    @focused.selection_clear(0,'end')
    @focused.selection_set i,i
    @focused.activate i
  end
  # find next article
  def findnext
    now = @now + 1
    last = @tags.size
    while(now < last)
      if(@tags[now]['see'] == false) then
	@unseen -= 1
	@list.selection_clear 'active'
	moveto = Float(now) / Float(last)
	@from.yview 'moveto',moveto
	@date.yview 'moveto',moveto
	@list.yview 'moveto',moveto
	@list.selection_clear(0,'end')
	@list.selection_set now,now
	@list.activate now
	@now = now
	if(@focused) then
	  @now = @focused.curselection[0]
	  if(@tags[@now]['see'] == false) then
	    @tags[@now]['see'] = true
	    update(@now)
	    $main.widget.wprog.status(nil)
	  end
#	  $main.widget.wheader.disp(@tags[@now])
	  $main.widget.wview.disp(@tags[@now],0)
	end
	break
      end
      now += 1
    end
    if(now == last) then
      $main.hideview
    end
  end
  # refile
  def refile
    size = @tags.size
    count = 0
    $main.widget.wprog.status(format("(%d/%d/%d)",count,0,size))
    @tags.each_index {|i|
      hdr = @tags[i].header[0]
      if(dst = @folder.server.refile.find(@tags[i],@tags[i]['see'])) then
	@tags[i]['mov'] = dst
	update(i)
	count += 1
      end
      $main.widget.wprog.progress(i.to_f/size.to_f)
      $main.widget.wprog.status(format("(%d/%d/%d)",count,i+1,size))
    }
    $main.widget.wprog.status(nil)
  end
  # refile fource
  def refile_fource
    size = @tags.size
    count = 0
    $main.widget.wprog.status(format("(%d/%d/%d)",count,0,size))
    @tags.each_index {|i|
      if(dst = @folder.server.refile.find(@tags[i],true)) then
	@tags[i]['mov'] = dst
	update(i)
	count += 1
      end
      $main.widget.wprog.progress(i.to_f/size.to_f)
      $main.widget.wprog.status(format("(%d/%d/%d)",count,i+1,size))
    }
    $main.widget.wprog.status(nil)
  end
  # undo refile
  def refile_undo
    size = @tags.size
    count = 0
    $main.widget.wprog.status(format("(%d/%d/%d)",count,0,size))
    @tags.each_index {|i|
      if(@tags[i]['mov']) then
	@tags[i]['mov'] = nil
	update(i)
	count += 1
      end
      $main.widget.wprog.progress(i.to_f/size.to_f)
      $main.widget.wprog.status(format("(%d/%d/%d)",count,i+1,size))
    }
    $main.widget.wprog.status(nil)
  end
  # refile execute
  def refile_exec
    size = @tags.size
    count = 0
    $main.widget.wprog.status(format("(%d/%d/%d)",count,0,size))
    @folder.server.login {
      @folder.lupdate()
      @tags.each_index {|i|
	if(@tags[i]['mov']) then
	  @tags[i].move()
	  count += 1
	end
	$main.widget.wprog.progress(i.to_f/size.to_f)
	$main.widget.wprog.status(format("(%d/%d/%d)",count,i+1,size))
      }
      @folder.expunge()
      $main.widget.wprog.status(nil)
    }
    folder = @folder
    open(folder)
  end
  # set seen all
  def seen_all
    size = @tags.size
    count = 0
    $main.widget.wprog.status(format("(%d/%d/%d)",count,0,size))
    @tags.each_index {|i|
      if(@tags[i]['see'] == false) then
	@tags[i]['see'] = true
	update(i)
	count += 1
      end
      $main.widget.wprog.progress(i.to_f/size.to_f)
      $main.widget.wprog.status(format("(%d/%d/%d)",count,i+1,size))
    }
    @unseen = 0
    $main.widget.wprog.status(nil)
  end
  # search
  def search
    if(@folder) then
      changed = false
      if(@par.cbar.text.value == "" or
	 @search_string != @par.cbar.text.value or
	 @search_target != @par.cbar.target) then
	changed = true
      end
      if(changed and @par.cbar.text.value != "") then
	@search_target = @par.cbar.target
	@search_string = @par.cbar.text.value
	@search_list = @folder.find(@search_target,@search_string)
      end
      uid = @search_list.shift
      if(uid) then
	@tags.each_index {|now|
	  if(@tags[now]['uid'] == uid) then
	    @list.selection_clear 'active'
	    moveto = Float(now) / Float(@tags.size)
	    @from.yview 'moveto',moveto
	    @date.yview 'moveto',moveto
	    @list.yview 'moveto',moveto
	    @list.selection_clear(0,'end')
	    @list.selection_set now,now
	    @list.activate now
	    @now = now
	    if(@tags[@now]['see'] == false) then
	      @tags[@now]['see'] = true
	      @unseen -= 1
	      update(@now)
	      $main.widget.wprog.status(nil)
	    end
#	    $main.widget.wheader.disp(@tags[now])
	    $main.widget.wview.disp(@tags[now],0)
	    if(@par.cbar.target == "BODY")
	      $main.widget.wview.search(@search_string)
	    end
	    break
	  end
	}
      else
	if(@par.cbar.text.value != "") then
	  SGDialog.new("search_end","info","OK",@search_string)
	  @search_string = nil
	  @search_target = nil
	end
      end
    end
  end
  # biff
  def biff(ticks)
    if(not $main.busy?) then
      if(@folder) then
	server = @folder.server
	if(server.rc['imap']['biff'] != 0) then
	  if(ticks % server.rc['imap']['biff'] == 0) then
	    server.biff()
	    if(server.recent != 0) then
	      $main.widget.wprog.status(nil)
	    end
	  end
	end
      end
    end
  end
end

#
# recv control
#

class List < SGFrame
attr :list
attr :header
attr :cbar
  def initialize(top,par)
    super(par)
    @top = top
    @cbar = List_cmdbar.new(top,self)
    @list = List_list.new(top,self)
    @cbar.pack('side'=>'top','fill'=>'x')
    @list.pack('side'=>'top','fill'=>'both','expand'=>'yes')
  end
end
