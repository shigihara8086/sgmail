#
# send command frame
#

class Send_command < SGFrame
attr :sign
attr :signature
attr :where
attr :sendwhere
  def initialize(top,par,sign,where)
    @top = top
    @where = where[0]['val']
    @sign = sign.sign[0]['val']
    super(par)
    borderwidth 1
    # send
    SGImagebutton.new(self,"send",$rc2['button']['send']) {
      command proc { top.send() }
      pack('side'=>'left')
    }
    # sep
    TkLabel.new(self) { image ICON::IMAGE['separater']; pack('side'=>'left') }
    # where to send
    @sendwhere = SGSelectbutton.new(
      self,where,false,proc {|k,v| @where = v }) {
      width 8
      pack('side'=>'left')
    }
    # sep
    TkLabel.new(self) { image ICON::IMAGE['separater']; pack('side'=>'left') }
    # cut
    SGImagebutton.new(self,"cut",$rc2['button']['cut']) {
      command proc { top.editor.cut }
      pack('side'=>'left')
    }
    # copy
    SGImagebutton.new(self,"copy",$rc2['button']['copy']) {
      command proc { top.editor.copy }
      pack('side'=>'left')
    }
    # paste
    SGImagebutton.new(self,"paste",$rc2['button']['paste']) {
      command proc { top.editor.paste }
      pack('side'=>'left')
    }
    # paste with reply mark
    SGImagebutton.new(self,"pasterep",$rc2['button']['pasterep']) {
      command proc { top.editor.pasterep }
      pack('side'=>'left')
    }
    # insert text
    SGImagebutton.new(self,"load",$rc2['button']['instext']) {
      command proc { top.editor.load }
      pack('side'=>'left')
    }
    # sep
    TkLabel.new(self) { image ICON::IMAGE['separater']; pack('side'=>'left') }
    # attach MIME part
    SGImagebutton.new(self,"mimet",$rc2['button']['attach']) {
      command proc { top.header.mime.attach }
      pack('side'=>'left')
    }
    # dettach MIME part
    SGImagebutton.new(self,"mimeb",$rc2['button']['detach']) {
      command proc { top.header.mime.detach }
      pack('side'=>'left')
    }
    # sep
    TkLabel.new(self) { image ICON::IMAGE['separater']; pack('side'=>'left') }
    # expert mode
    SGImagebutton.new(self,"expt",$rc2['button']['expart']) {
      command proc { top.exptmode }
      pack('side'=>'left')
    }
    # exit
    SGImagebutton.new(self,"cancel",$rc2['button']['close']) {
      command proc { top.quit }
      pack('side'=>'right')
    }
    # sep
    TkLabel.new(self) { image ICON::IMAGE['separater']; pack('side'=>'right') }
    # signature
    @signature = SGSelectbutton.new(
      self,sign.sign,false,proc {|k,v| @sign = v }) {
      width 18
      pack('side'=>'right')
    }
  end
end

#
# send header frame
#

class Send_normal_header < SGFrame
attr :subject
attr :to
attr :cc
attr :bcc
  def initialize(top,par,addr,tmp,tcb)
    super(par)
    @top = top
    borderwidth 1
    adr = addr ? addr.adr : nil
    @subject = SGSelectbox2.new(self,"Subject:    ",tmp,tcb)
    @to      = SGSelectbox.new(self,"To:         ",adr,
      proc {|k,v| @top.command.signature.select([k]) })
    @cc      = SGSelectbox.new(self,"Cc:         ",adr)
    @bcc = SGSelectbox.new(self,    "Bcc:        ",adr)
    @subject.pack('side'=>'top','fill'=>'x','expand'=>'yes')
    @to.pack('side'=>'top','fill'=>'x','expand'=>'yes')
    @cc.pack('side'=>'top','fill'=>'x','expand'=>'yes')
    @bcc.pack('side'=>'top','fill'=>'x','expand'=>'yes')
  end
end

#
# send expart header frame
#

class Send_expt_header < SGFrame
attr :from
#attr :bcc
attr :reply
attr :inreply
attr :addition
  def initialize(top,par,addr)
    super(par)
    borderwidth 1
    adr = addr ? addr.adr : nil
    @from = SGInputbox.new(self,    "From:       ","")
#    @bcc = SGSelectbox.new(self,    "Bcc:        ",adr)
    @reply = SGSelectbox.new(self,  "Reply-To:   ",adr)
    @inreply = SGSelectbox.new(self,"In-Reply-To:",adr)
    @addition = SGInputbox.new(self,"Addition:   ","")
    @from.pack('side'=>'top','fill'=>'x','expand'=>'yes')
#    @bcc.pack('side'=>'top','fill'=>'x','expand'=>'yes')
    @reply.pack('side'=>'top','fill'=>'x','expand'=>'yes')
    @inreply.pack('side'=>'top','fill'=>'x','expand'=>'yes')
    @addition.pack('side'=>'top','fill'=>'x','expand'=>'yes')
  end
end

#
# send control frame
#

class Send_smtp_header < SGFrame
attr :normal
attr :expt
  def initialize(top,par,addr,temp,tcb)
    super(par)
    @normal = Send_normal_header.new(top,self,addr,temp,tcb)
    @expt = Send_expt_header.new(top,self,addr)
    @normal.pack('side'=>'top','fill'=>'x','expand'=>'yes')
  end
end

#
# send MIME header frame
#

class Send_mime_header < SGFrame
include Binary
attr :files
  def initialize(top,par,box)
    @files = {}
    super(par)
    borderwidth 1
    @ybar = SGScrollbar.new(self) {
      orient 'vertical'
      pack('side'=>'right','fill'=>'y')
    }
    @list = SGListbox.new(self) {
      selectborderwidth 0
      background 'white'
      width 24
      height 1
      pack('side'=>'left','fill'=>'y','expand'=>'yes')
    }
    @ybar.command proc{|idx| @list.yview *idx }
    @list.yscroll proc {|idx| @ybar.set *idx }
    @list.bind "Double-ButtonRelease-1",proc { detach }
  end
  # mime attach
  def attach(name = nil)
    if(name == nil) then
      name = Tk.getOpenFile(
        'filetypes'=>"{{All Files} {*}}",
        'initialdir'=>OS::HOME,
        'title'=>$rc2['dialog']['open'][0])
    end
    if(name != "") then
      base = File.basename(name)
      open(OS.path(name)) {|fd|
	fd.binmode
	if(binary?(fd.read(512))) then
	  @list.insert 'end',OS.toutf8(format("[B]%s",base))
	  @files["[B]"+base] = name
	else
	  @list.insert 'end',OS.toutf8(format("[T]%s",base))
	  @files["[T]"+base] = name
	end
      }
    end
  end
  # mime detach
  def detach
    now = @list.curselection[0]
    if(now) then
      text = @list.get(now)
      @files[text] = nil
      @list.delete now,now
    end
  end
end

#
# send header frame
#

class Send_header < SGFrame
attr :smtp
attr :mime
  def initialize(top,par,addr,temp,tcb)
    super(par)
    @top = top
    @addr = addr
    @smtp = Send_smtp_header.new(top,self,addr,temp,tcb)
    @mime = Send_mime_header.new(top,self,addr)
    @smtp.pack('side'=>'left','fill'=>'x','expand'=>'yes')
    @mime.pack('side'=>'right','fill'=>'y')
  end
    # subject
  def subject
    @smtp.normal.subject.value
  end
  def subject=(val)
    @smtp.normal.subject.value = val
  end
  # from
  def from
    @smtp.expt.from.value
  end
  def from=(val)
    @smtp.expt.from.value = val
  end
  # to
  def to
    @smtp.normal.to.value
  end
  def to=(val)
    @smtp.normal.to.value = val
    key = @addr.rev[val]
    if(key) then
      @top.command.signature.select([key])
    end
  end
  # cc
  def cc
    @smtp.normal.cc.value
  end
  def cc=(val)
    @smtp.normal.cc.value = val
  end
  # bcc
  def bcc
    @smtp.normal.bcc.value
  end
  def bcc=(val)
    @smtp.normal.bcc.value = val
  end
  # reply_to
  def reply_to
    @smtp.expt.reply.value
  end
  def reply_to=(val)
    @smtp.expt.reply.value = val
  end
  # in_reply_to
  def in_reply_to
    @smtp.expt.inreply.value
  end
  def in_reply_to=(val)
    @smtp.expt.inreply.value = val
  end
  # addition header
  def addition
    @smtp.expt.addition.value
  end
end

#
# send text editor frame
#

class Send_editor < SGFrame
include Fold
attr :ybar
attr :text
  def initialize(top,par)
    @top = top
    super(par)
    @ybar = SGScrollbar.new(self) {
      pack('side'=>'right','fill'=>'y')
    }
    @text = SGText.new(self) {
      foreground $tkrc['send_color'][0]
      background $tkrc['send_color'][1]
      width $tkrc['send_width']
      height$tkrc['send_height']
      focus
      pack('side'=>'left','fill'=>'both','expand'=>'yes')
    }
    @cursor = TkTextMarkInsert.new(@text,1.0)
    @ybar.command proc {|idx| @text.yview *idx }
    @text.yscroll proc {|idx| @ybar.set *idx }
    # wheel mouse by akkey
    @text.bind "ButtonPress-5",proc { @text.yview 'scroll','5','units' }
    @text.bind "ButtonPress-4",proc { @text.yview 'scroll','-5','units' }
    #
    @text.bind "Control-v",proc { paste }
    @text.bind "Control-p",proc { pasterep }
    @text.bind "Return",proc {
      index = @text.index('insert')
      text = OS.fromutf8(@text.get("#{Integer(index)}.0","#{index}"))
      text = fold(text,$rc2['foldchar'],$rc['width'] - 2)
      @text.delete("#{Integer(index)}.0","#{index}")
      @text.insert('insert',OS.toutf8(text))
    }
#    @text.bind "ButtonPress-3",proc {|x,y|
#      SGColorPopup.new(x,y,'send_color',@text)
#    },"%X %Y"
  end
  # clear text
  def clear
    @text.delete('1.0','end')
  end
  # to
  def body
    OS.fromutf8(@text.value)
  end
  def insert(val)
    @text.insert('insert',OS.toutf8(val))
  end
  # move cursor
  def locate(pos)
    @cursor.set(pos)
  end
  # cut
  def cut
    begin
      sel = TkTextTagSel.new(@text)
      text = @text.get(sel.first,sel.last)
      TkClipboard.set(text)
      @text.delete(sel.first,sel.last)
    rescue
    end
  end
  # copy
  def copy
    begin
      sel = TkTextTagSel.new(@text)
      text = @text.get(sel.first,sel.last)
      TkClipboard.set(text)
    rescue
    end
  end
  # paste
  def paste
    begin
      sel = TkTextTagSel.new(@text)
      @text.delete(sel.first,sel.last)
    rescue
    end
    @text.insert('insert',TkClipboard.get)
    @text.see(@text.index('insert'))
  end
  # paste with reply mark
  def pasterep
    begin
      sel = TkTextTagSel.new(@text)
      @text.delete(sel.first,sel.last)
    rescue
    end
    text = TkClipboard.get
    text.each_line {|line|
      @text.insert('insert',OS.toutf8($rc['reply']+OS.fromutf8(line)))
    }
    @text.see(@text.index('insert'))
  end
  # insert text
  def load
    name = Tk.getOpenFile(
      'filetypes'=>"{{Text Files} {.txt}} {{All Files} {*}}",
      'initialdir'=>OS::HOME,
      'title'=>$rc2['dialog']['open'][0])
    if(name != "") then
      fh = open(OS.path(name),"r")
      fh.each {|line|
        @text.insert('insert',OS.toutf8(line))
      }
      fh.close
    end
  end
end

#
# send window
#

class Send < TkToplevel
attr :command
attr :header
attr :editor
  def initialize(ttl,svr,tag,to)
    super()
    @title = ttl
    @server = svr
    @tag = tag
    @to = to
    @quitting = false
    @expert = false
    title @title
    iconname @title
    tcb = Proc.new {|name,value|
      case @title
      when "Send"
	setup_send(name,@to)
      when "Reply"
	setup_reply(name)
      when "Transfer"
	setup_transfer(name)
      when "Resend"
	setup_resend
      end
    }
    @command = Send_command.new(self,self,@server.signature,@server.where)
    @header = Send_header.new(self,self,@server.address,@server.template,tcb)
    @editor = Send_editor.new(self,self)
    @command.pack('side'=>'top','fill'=>'x')
    @header.pack('side'=>'top','fill'=>'x','expand'=>'yes')
    @editor.pack('side'=>'top','fill'=>'both','expand'=>'yes')
    case @title
    when "Send"
      setup_send("send",@to)
    when "Reply"
      setup_reply("reply")
    when "Transfer"
      setup_transfer("transfer")
    when "Resend"
      setup_resend
    end
    # launch editor
    if($rc['editor'] != "self") then
      launch_editor()
    end
  end
  # busy
  def busy
    cursor "watch"
    @editor.text.cursor "watch"
  end
  # idle
  def idle
    cursor ""
    @editor.text.cursor "xterm"
  end
  # tuggle expart mode
  def exptmode
    if(@expert) then
      @header.smtp.expt.unpack
      @expert = false
    else
      @header.smtp.expt.pack('side'=>'top','fill'=>'x','expand'=>'yes')
      @expert = true
    end
  end
  # setup template text
  def setup(tmp)
    @editor.focus
    @editor.clear
    hdr = nil
    hdr = @tag.header[0] if(@tag)
    temp = @server.conf[tmp]
    hmode = true
    temp.each_line {|line|
      if(hmode) then
	if(line =~ /^$/) then
	  hmode = false
	else
	  case line
	  when /^Subject: (.*)$/i
	    @header.subject = $1
	  when /^To: (.*)$/i
	    to = $1
	    @header.to = to
	  when /^Cc: (.*)$/i
	    @header.cc = $1
	  when /^Bcc: (.*)$/i
	    @header.bcc = $1
	  when /^Reply-To: (.*)$/i
	    @header.reply_to = $1
	  else
	    @header.addition = line
	  end
	end
      else
	if(hdr) then
	  hdr.each {|k,v|
	    if(line =~ /%#{k}/i) then
	      line.gsub!(/%#{k}/i,v)
	    end
	  }
	  if(line =~ /%nickname/i) then
	    from = hdr['From']
	    if(from =~ /\((.*?)\)/) then
	      nickname = $1
	    else
	      if(from =~ /(.*?)<.*?>/) then
		nickname = $1.strip
#		nickname = nickname.gsub!(/\"/,"")
	      else
		nickname = @server.address.rev[from.strip]
		if(nickname == nil) then
		  nickname = from
		end
	      end
	    end
	    line.gsub!(/%nickname/i,nickname)
	  end
	end
	@editor.insert(line)
      end
    }
  end
  # setup new mail
  def setup_send(temp,to)
    busy()
    @header.from = @server.rc['smtp']['from']
    @header.bcc = @server.rc['smtp']['bcc']
    setup(temp)
    @header.to = to if(to)
    idle()
  end
  # setup_reply
  def setup_reply(temp)
    busy()
    hdr = @tag.header
    subj = hdr[0]['Subject'].dup
    #begin pached by yasuyuki
    if($CACHE.rc['mlcollapse']) then
      subj.sub!(/#{$rc2['hdrpat']}/i) { "" }
    end
    #end
    subj.sub!(/#{$rc2['repat']}/i) { "" }
    @header.subject = "Re: "+subj
    @header.from = @server.rc['smtp']['from']
    if(hdr[0]['Reply-To']) then
      from = hdr[0]['Reply-To'].dup
    else
      from = hdr[0]['From'].dup
    end
    if(from =~ /<(.*?)>/) then
      from = $1
    end
    from.sub!(/(\(.*?\))/) { "" }
    @header.to = from.strip
    @header.bcc = @server.rc['smtp']['bcc']
    # copy Cc:
    @header.cc = hdr[0]['Cc'] if(hdr[0]['Cc'])
    @header.in_reply_to = hdr[0]['Message-Id'] if(hdr[0]['Message-Id'])
    setup(temp)
    body = @tag.body(0)
    body.each_line {|line|
      @editor.insert($CACHE.rc['reply']+line)
    }
    @editor.locate(1.0)
    idle()
  end
  # setup transfer
  def setup_transfer(temp)
    busy()
    hdr = @tag.header
    subj = hdr[0]['Subject'].dup
    #begin pached by yasuyuki
    if($CACHE.rc['mlcollapse']) then
      subj.sub!(/#{$rc2['hdrpat']}/i) { "" }
    end
    #end
    subj.sub!(/#{$rc2['repat']}/i) { "" }
    @header.subject = "Fw: "+subj
    @header.from = @server.rc['smtp']['from']
    @header.bcc = @server.rc['smtp']['bcc']
    @header.in_reply_to = hdr[0]['Message-Id'] if(hdr[0]['Message-Id'])
    # reply text
    @editor.focus
    @editor.clear
    setup(temp)
    $CACHE.rc['begin'].each_line {|line|
      @editor.insert(line)
    }
    body = @tag.body(0)
    body.each_line {|line|
      @editor.insert(line)
    }
    $CACHE.rc['end'].each_line {|line|
      @editor.insert(line)
    }
    @editor.locate(1.0)
    hdr.each_index {|i|
      if(i != 0) then
	if(hdr[i]['Content-Type']) then
	  @header.mime.attach(@tag.save(i))
	end
      end
    }
    idle()
  end
  # setup resend
  def setup_resend
    busy()
    hdr = @tag.header
    @header.subject = hdr[0]['Subject']
    @header.from = @server.rc['smtp']['from']
    @header.bcc = @server.rc['smtp']['bcc']
    @command.signature.select(['None'])
    if(@tag.folder.name == @server.config.name) then
      @command.sendwhere.select(["config"])
    end
    @header.in_reply_to = hdr[0]['In-Reply-To'] if(hdr[0]['In-Reply-To'])
    # reply text
    @editor.focus
    @editor.clear
    body = @tag.body(0)
    body.each_line {|line|
      @editor.insert(line)
    }
    @editor.locate(1.0)
    hdr.each_index {|i|
      if(i != 0) then
	if(hdr[i]['Content-Type']) then
	  @header.mime.attach(@tag.save(i))
	end
      end
    }
    idle()
  end
  # send
  def send
    hdr = {}
    hdr['Subject'] = @header.subject
    hdr['From'] = @header.from
    hdr['To'] = @header.to
    hdr['Cc'] = @header.cc
    hdr['Bcc'] = @header.bcc
    hdr['Reply-To'] = @header.reply_to
    hdr['In-Reply-To'] = @header.in_reply_to
    add = @header.addition
    if(add =~ /(.*?):\s+(.*)/) then
      hdr[$1] = $2
    end
    case @command.where
    when ""
      if(hdr['To'].gsub(/ /,"") != "") then
	busy()
	@server.send(
	  hdr,@editor.body,@command.sign,@header.mime.files)
	idle()
	quit
      else
	SGDialog.new("no_to_error")
      end
    when "outbox"
      if(hdr['To'].gsub(/ /,"") != "") then
	if(@server.outbox) then
	  busy()
	  @server.outbox.store(
	    hdr,@editor.body,@command.sign,@header.mime.files)
	  idle()
	  quit
	end
      else
	SGDialog.new("no_to_error")
      end
    when "draft"
      if(@server.draft) then
	busy()
	@server.draft.store(
	  hdr,@editor.body,@command.sign,@header.mime.files)
	idle()
	quit
      end
    when "config"
      if(@server.config) then
	busy()
	@server.config.store(
	  hdr,@editor.body,@command.sign,@header.mime.files)
	if(@tag) then
	  @server.config.remove(@tag['uid'])
	end
	@server.getconfig()
	idle()
	quit
      end
    end
  end
  # launch editor
  def launch_editor
    withdraw
    n = 0
    while(true)
      file = OS.path(OS::CACHE+"/tmp/#{$$}#{n}.txt")
      if(not test(?e,file)) then
	break
      end
      n += 1
    end
    open(file,"w") {|fd|
      fd.puts "Subject: "+@header.subject
      fd.puts "From: "+@header.from
      fd.puts "To: "+@header.to
      fd.puts "Cc: "+@header.cc
      fd.puts "Bcc: "+@header.bcc
      fd.puts "Reply-To: "+@header.reply_to
      fd.puts "In-Reply-To: "+@header.in_reply_to
      fd.puts "----Do not delete this line----"
      fd.puts @editor.body
      fd.chmod(0600)
    }
    thread = Thread.start {
      PTY.protect_signal {
	fork() {
	  exec($rc['editor'],file)
	}
	Process.wait
      }
      open(file) {|fd|
	mode = "header"
	fd.each_line {|line|
	  case mode
	  when "header"
	    line.chop!
	    if(line =~ /^([\w-]+):\s(.*)$/) then
	      name = $1
	      body = $2
	      case name
	      when "Subject"
		@header.subject = body
	      when "From"
		@header.from = body
	      when "To"
		@header.to = body
	      when "Cc"
		@header.cc = body
	      when "Bcc"
		@header.bcc = body
	      when "Reply-To"
		@header.reply_to = body
	      when "In-Reply-To"
		@header.in_reply_to = body
	      end
	    else
	      if(line == "----Do not delete this line----") then
		mode = "body"
		@editor.clear
	      end
	    end
	  when "body"
	    @editor.insert(line)
	  end
	}
	@editor.locate(1.0)
      }
      File.unlink(file)
      deiconify
    }
  end
  # quit
  def quit
    if($help) then
      $help.destroy
      $help = nil
    end
    $main.settimer(0,nil)
    destroy
  end
end
