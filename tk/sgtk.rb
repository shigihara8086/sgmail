#
# custom Tk class for SGmail
#

class SGFrame < TkFrame
  def initialize(par)
    super(par)
    borderwidth 0
    relief 'raised'
  end
end

class SGScrollbar < TkScrollbar
  def initialize(par)
    super(par)
    borderwidth 1
    takefocus 0
    case OS::SYS
    when "unix"
      width $tkrc['scbar_width']
    end
  end
end

class SGMenubutton < TkMenubutton
  def initialize(par,msg)
    super(par)
    text OS.toutf8(msg)
    i = msg.index('(')
    if(i) then underline i+1 end
    borderwidth 1
    foreground $tkrc['button_color'][0]
    font FONT::MENU
  end
end

class SGMenu < TkMenu
  def initialize(par)
    super(par)
    tearoff 'off'
    borderwidth 1
    foreground $tkrc['menu_color'][0]
    font FONT::MENU
  end
end

class SGMenubar < TkMenubar
  def initialize(par,msg)
    super(par,OS.toutf8(msg))
    tearoff 'off'
    borderwidth 1
    foreground $tkrc['menu_color'][0]
    font FONT::MENU
  end
end

class SGLabel < TkLabel
  def initialize(par,msg)
    super(par)
    text OS.toutf8(msg)
    borderwidth 1
    foreground $tkrc['label_color'][0]
    font FONT::MENU
  end
end

class SGButton < TkButton
  def initialize(par,msg)
    super(par)
    text OS.toutf8(msg)
    borderwidth 1
    relief 'raised'
    takefocus 0
    foreground $tkrc['button_color'][0]
    font FONT::MENU
  end
end

class SGHelp < TkToplevel
  def initialize(btn,msg)
    super()
    x = TkWinfo.rootx(btn) + 24
    y = TkWinfo.rooty(btn) + 24
    geometry "+#{x}+#{y}"
    tk_call 'wm','overrideredirect',path,'true'
    @label = TkLabel.new(self) {
      text OS.toutf8(msg)
      font FONT::MENU
      foreground "black"
      background "lightyellow"
      highlightbackground "black"
      highlightthickness 1
    }
    @label.pack
  end
end

class SGImagebutton < TkButton
  def initialize(par,img,msg)
    @msg = msg
    super(par)
    image ICON::IMAGE[img]
    relief 'raised'
    takefocus 0
    borderwidth 1
    foreground $tkrc['button_color'][0]
    @cb = Proc.new {
      if($help) then
	$help.destroy
	$help = nil
      end
      $help = SGHelp.new(self,@msg)
    }
    bind 'Enter',proc {
      if($main != nil && $main.busy? == false) then
	$main.settimer(1,@cb)
      end
    }
    bind 'Leave',proc {
      if($main) then
	$main.settimer(0,nil)
	if($help) then
	  $help.destroy
	  $help = nil
	end
      end
    }
  end
end

class SGListbox < TkListbox
  def initialize(par)
    super(par)
    borderwidth 1
    font FONT::LIST
  end
end

class SGText < TkText
  def initialize(par)
    super(par)
    borderwidth 1
    font FONT::TEXT
  end
end

class SGSelectbutton < TkMenubutton
attr :selname
attr :selvalue
  def initialize(par,tree,value,cb=nil)
    super(par)
    @cb = cb
    @tree = tree
    @value = value
    @selname = nil
    @selvalue = nil
    borderwidth 1
    relief 'raised'
    indicator 1
    foreground $tkrc['button_color'][0]
    font FONT::MENU
    if(tree) then
      setmenu(tree)
    end
  end
  def setmenu_subsub(menu,name,value)
    menu.add 'command','label'=>OS.toutf8(name),'command'=>proc {
      @selname = name
      @selvalue = value
      if(@value) then
        text OS.toutf8(value)
      else
        text OS.toutf8(name)
      end
      @cb.call(name,value) if(@cb)
    }
  end
  def setmenu_sub(menu,tree)
    tree.each {|item|
      if(item['cld']) then
        child = TkMenu.new(menu) {
          tearoff 'off'
          borderwidth 1
          foreground $tkrc['menu_color'][0]
          background $tkrc['menu_color'][1]
          font FONT::MENU
        }
        menu.add 'cascade','label'=>OS.toutf8(item['nam']),'menu'=>child
        setmenu_sub(child,item['cld'])
      else
        setmenu_subsub(menu,item['nam'],item['val'])
      end
    }
  end
  def setmenu(tree)
    @tree = tree
    if(@value) then
      text OS.toutf8(tree[0]['val'])
    else
      text OS.toutf8(tree[0]['nam'])
    end
    menu = TkMenu.new(self) {
      tearoff 'off'
      borderwidth 1
      foreground $tkrc['menu_color'][0]
      background $tkrc['menu_color'][1]
      font FONT::MENU
    }
    menu(menu)
    setmenu_sub(menu,tree)
  end
  def select_sub(tree,path)
    name = path.shift
    tree.each {|item|
      if(item['nam'] == name) then
        if(item['cld']) then
          select_sub(item['cld'],path)
        else
          @selname = name
          @selvalue = item['val']
          if(@value) then
            text OS.toutf8(item['val'])
          else
            text OS.toutf8(item['nam'])
          end
          @cb.call(item['nam'],item['val']) if(@cb)
          break
        end
      end
    }
  end
  def select(path)
    select_sub(@tree,path)    
  end
end

class SGSelectbox < TkFrame
  def initialize(par,msg,tree,cb=nil)
    super(par)
    @cb = cb
    borderwidth 0
    relief 'raised'
    @button = TkMenubutton.new(self) {
      borderwidth 1
      relief 'raised'
      indicator 1
      foreground $tkrc['button_color'][0]
      font FONT::MENU
      text OS.toutf8(msg)
      pack('side'=>'left')
    }
    @entry = TkEntry.new(self) {
      borderwidth 1
      foreground $tkrc['entry_color'][0]
      background $tkrc['entry_color'][1]
      font FONT::MENU
      pack('side'=>'left','fill'=>'x','expand'=>'yes')
    }
    if(tree) then
      setmenu(tree)
    end
  end
  def setmenu_subsub(menu,name,value)
    menu.add 'command','label'=>OS.toutf8(name),'command'=>proc {
      if(@entry.value == "") then
        @entry.value = "#{value}"
      else
        @entry.value << ",#{value}"
      end
      @cb.call(name,value) if(@cb)
    }
  end
  def setmenu_sub(menu,tree)
    tree.each {|item|
      if(item['cld']) then
        child = TkMenu.new(menu) {
          tearoff 'off'
          borderwidth 1
          foreground $tkrc['menu_color'][0]
          background $tkrc['menu_color'][1]
          font FONT::MENU
        }
        menu.add 'cascade','label'=>OS.toutf8(item['nam']),'menu'=>child
        setmenu_sub(child,item['cld'])
      else
        setmenu_subsub(menu,item['nam'],item['val'])
      end
    }
  end
  def setmenu(tree)
    menu = TkMenu.new(@button) {
      tearoff 'off'
      borderwidth 1
      foreground $tkrc['menu_color'][0]
      background $tkrc['menu_color'][1]
      font FONT::MENU
    }
    @button.menu(menu)
    setmenu_sub(menu,tree)
  end
  def value
    OS.fromutf8(@entry.value)
  end
  def value=(val)
    @entry.value = OS.toutf8(val)
  end
end

class SGSelectbox2 < TkFrame
  def initialize(par,msg,tree,cb=nil)
    super(par)
    @cb = cb
    borderwidth 0
    relief 'raised'
    @button = TkMenubutton.new(self) {
      borderwidth 1
      relief 'raised'
      indicator 1
      foreground $tkrc['button_color'][0]
      font FONT::MENU
      text OS.toutf8(msg)
      pack('side'=>'left')
    }
    @entry = TkEntry.new(self) {
      borderwidth 1
      foreground $tkrc['entry_color'][0]
      background $tkrc['entry_color'][1]
      font FONT::MENU
      pack('side'=>'left','fill'=>'x','expand'=>'yes')
    }
    if(tree) then
      setmenu(tree)
    end
  end
  def setmenu_subsub(menu,name,value)
    menu.add 'command','label'=>OS.toutf8(name),'command'=>proc {
      @cb.call(name,value) if(@cb)
    }
  end
  def setmenu_sub(menu,tree)
    tree.each {|item|
      if(item['cld']) then
        child = TkMenu.new(menu) {
          tearoff 'off'
          borderwidth 1
          foreground $tkrc['menu_color'][0]
          background $tkrc['menu_color'][1]
          font FONT::MENU
        }
        menu.add 'cascade','label'=>OS.toutf8(item['nam']),'menu'=>child
        setmenu_sub(child,item['cld'])
      else
        setmenu_subsub(menu,item['nam'],item['val'])
      end
    }
  end
  def setmenu(tree)
    menu = TkMenu.new(@button) {
      tearoff 'off'
      borderwidth 1
      foreground $tkrc['menu_color'][0]
      background $tkrc['menu_color'][1]
      font FONT::MENU
    }
    @button.menu(menu)
    setmenu_sub(menu,tree)
  end
  def value
    OS.fromutf8(@entry.value)
  end
  def value=(val)
    @entry.value = OS.toutf8(val)
  end
  def label=(val)
    @button.text OS.toutf8(val)
  end
end

class SGInputbox < TkFrame
  def initialize(par,msg,val)
    super(par)
    borderwidth 0
    relief 'raised'
    @button = TkMenubutton.new(self) {
      borderwidth 1
      relief 'raised'
      indicator 1
      foreground $tkrc['button_color'][0]
      font FONT::MENU
      text OS.toutf8(msg)
      pack('side'=>'left')
    }
    @entry = TkEntry.new(self) {
      borderwidth 1
      foreground $tkrc['entry_color'][0]
      background $tkrc['entry_color'][1]
      font FONT::MENU
      pack('side'=>'left','fill'=>'x','expand'=>'yes')
    }
    @entry.value = OS.toutf8(val)
  end
  def value
    OS.fromutf8(@entry.value)
  end
  def value=(val)
    @entry.value = OS.toutf8(val)
  end
end

=begin
class SGPopup < TkMenu
  def initialize(list)
    super()
    tearoff 'off'
    borderwidth 1
    foreground $tkrc['menu_color'][0]
    background $tkrc['menu_color'][1]
    font FONT::MENU
    list.each {|k,v|
      add 'command','label'=>OS.toutf8(k),'command'=>v
    }
  end
end

class SGColorPopup < SGPopup
  def initialize(x,y,type,object)
    super(
      'Foreground Color'=>proc {
        color = Tk.chooseColor('initialcolor'=>$tkrc[type][0])
        if(color != "") then
          object.foreground color
          $tkrc[type][0] = color
        end
      },
      'Background Color'=>proc {
        color = Tk.chooseColor('initialcolor'=>$tkrc[type][1])
        if(color != "") then
          object.background color
          $tkrc[type][1] = color
        end
      }
    )
    post(x - 10,y - 10)
    activate 0
    if(OS::SYS != "win") then
      focus
      grab
    end
  end
end
=end
