#
# SGmail configuration
#

#
# configuration editor menu
#

#
# command frame
#

class Config_command < SGFrame
  def initialize(top,par)
    @top = top
    super(par)
    borderwidth 1
    relief 'raised'
    # save
    SGImagebutton.new(self,"save",$rc2['button']['save']) {
      command proc { top.quit(true) }
      pack('side'=>'left')
    }
    # sep
    TkLabel.new(self) {
      image ICON::IMAGE['separater']
      pack('side'=>'left')
    }
    # cut
    SGImagebutton.new(self,"cut",$rc2['button']['cut']) {
      command proc { top.editor.cut }
      pack('side'=>'left')
    }
    # copy
    SGImagebutton.new(self,"copy",$rc2['button']['copy']) {
      command proc { top.editor.copy }
      pack('side'=>'left')
    }
    # paste
    SGImagebutton.new(self,"paste",$rc2['button']['paste']) {
      command proc { top.editor.paste }
      pack('side'=>'left')
    }
    # quit
    SGImagebutton.new(self,"cancel",$rc2['button']['close']) {
      command proc { top.quit(false) }
      pack('side'=>'right')
    }
  end
end

#
# configure editor text frame
#

class Config_editor < SGFrame
attr :ybar
attr :text
  def initialize(top,par)
    @top = top
    super(par)
    borderwidth 1
    relief 'raised'
    @ybar = SGScrollbar.new(self) {
      pack('side'=>'right','fill'=>'y')
    }
    @text = SGText.new(self) {
      foreground $tkrc['config_color'][0]
      background $tkrc['config_color'][1]
      width $tkrc['conf_width']
      height $tkrc['conf_height']
      focus
      pack('side'=>'left','fill'=>'both','expand'=>'yes')
    }
    @ybar.command proc{|idx| @text.yview *idx }
    @text.yscroll proc {|idx| @ybar.set *idx }
    @text.bind "Control-v",proc { paste }
    @text.bind "ButtonPress-3",proc {|x,y|
      SGColorPopup.new(x,y,'config_color',@text)
    },"%X %Y"
  end
  # cut
  def cut
    begin
      sel = TkTextTagSel.new(@text)
      text = @text.get(sel.first,sel.last)
      TkClipboard.set(text)
      @text.delete(sel.first,sel.last)
    rescue
    end
  end
  # copy
  def copy
    begin
      sel = TkTextTagSel.new(@text)
      text = @text.get(sel.first,sel.last)
      TkClipboard.set(text)
    rescue
    end
  end
  # paste
  def paste
    begin
      sel = TkTextTagSel.new(@text)
      @text.delete(sel.first,sel.last)
    rescue
    end
    @text.insert('insert',TkClipboard.get)
    l,c = @text.index('insert').split(/\./)
    l = l.to_i + 1
    @text.see("#{l}.#{c}")
  end
end

#
# RC file editor
#

class Rc_config < TkToplevel
attr :menu
attr :editor
  def initialize
    super
    title ".sgmailrc"
    iconname ".sgmailrc"
    @command = Config_command.new(self,self)
    @editor = Config_editor.new(self,self)
    @command.pack('side'=>'top','fill'=>'x')
    @editor.pack('side'=>'top','fill'=>'both','expand'=>'yes')
    # load .sgmailrc file
    if(test(?f,OS.path(OS::RC))) then
      fh = open(OS.path(OS::RC),"r")
      fh.each {|line|
        line.chop!
        @editor.text.insert 'end',OS.toutf8(line)+"\n"
      }
      fh.close
    end
    cursor = TkTextMarkInsert.new(@editor.text,1.0)
    cursor.set 'insert linestart'
    update
    grab
    wait_destroy
  end
  # quit
  def quit(flag)
    $main.settimer(0,nil)
    if($help) then
      $help.destroy
      $help = nil
    end
    if(flag) then
      # save .sgmailrc file
      fh = open(OS.path(OS::RC),"w")
      @editor.text.value.each {|line|
        fh.puts OS.fromutf8(line)
      }
      fh.close      
    end
    destroy
  end
end
