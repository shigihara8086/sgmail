#
# recv mailbox list and subject list,header,progress bar,viewer
#

class Tiny_frame < SGFrame
attr :mbox
attr :list
  def initialize(top,par)
    super(par)
    @mbox = Mbox.new(top,self)
    @list = List.new(top,self)
    @mbox.pack('side'=>'left','fill'=>'y')
    @list.pack('side'=>'right','fill'=>'both','expand'=>'yes')
  end
end

class Large_frame < SGFrame
attr :wlist
attr :wview
#attr :wheader
  def initialize(top,par)
    super(par)
    @list = List.new(top,self)
    @view = View_frame.new(top,self)
    @list.pack('side'=>'top','fill'=>'x')
    @view.pack('side'=>'bottom','fill'=>'both','expand'=>'yes')
    @wlist = @list.list
#    @wheader = @list.header
    @wview = @view
  end
end

class Large_frame2 < SGFrame
attr :mbox
attr :large
  def initialize(top,par)
    super(par)
    @mbox = Mbox.new(top,self)
    @large = Large_frame.new(top,self)
    @mbox.pack('side'=>'left','fill'=>'y')
    @large.pack('side'=>'right','fill'=>'both','expand'=>'yes')
  end
end

class Recv_tiny < SGFrame
attr :wmbox
attr :wlist
#attr :wheader
attr :wview
attr :wprog
  def initialize(top,par)
    super(par)
    @top = top
    @frame = Tiny_frame.new(top,self)
    @wprog = Progress.new(top,self)
    @frame.pack('side'=>'top','fill'=>'both','expand'=>'yes')
    @wprog.pack('side'=>'bottom','fill'=>'x')
    @wmbox = @frame.mbox.tree
    @wlist = @frame.list.list
#    @wheader = @frame.list.header
    @wview = View_window.new(top)
  end
  # remake view
  def remake
    @wview = View_window.new(@top)    
  end
end

class Recv_small < SGFrame
attr :wmbox
attr :wlist
#attr :wheader
attr :wview
attr :wprog
  def initialize(top,par)
    super(par)
    @tiny = Tiny_frame.new(top,self)
    @view = View_frame.new(top,self)
    @wprog = Progress.new(top,self)
    @tiny.pack('side'=>'top','fill'=>'x')
    @view.pack('side'=>'top','fill'=>'both','expand'=>'yes')
    @wprog.pack('side'=>'bottom','fill'=>'x')
    @wmbox = @tiny.mbox.tree
    @wlist = @tiny.list.list
#    @wheader = @tiny.list.header
    @wview = @view
  end
end

class Recv_large < SGFrame
attr :wmbox
attr :wlist
#attr :wheader
attr :wview
attr :wprog
  def initialize(top,par)
    super(par)
    @large = Large_frame2.new(top,self)
    @wprog = Progress.new(top,self)
    @large.pack('side'=>'top','fill'=>'both','expand'=>'yes')
    @wprog.pack('side'=>'bottom','fill'=>'x')
    @wmbox = @large.mbox.tree
    @wlist = @large.large.wlist
#    @wheader = @large.large.wheader
    @wview = @large.large.wview
  end
end

#
# main
#

class Recv < TkToplevel
attr :widget
  def initialize
    super()
    @work = false
    title("#{OS::PRG} Ver #{$Version}")
    bind "Destroy",proc { quit }
    # password callback
    pass_cb = Proc.new {|user|
      dlg = PasswdDialog.new(user)
      user = dlg.u
      passwd = dlg.p
      [user,passwd]
    }
    # dialog,status,progress callback
    dlog_cb = Proc.new {|msg| SGDialog.new(msg) }
    stat_cb = Proc.new {|msg| @widget.wprog.status(msg) }
    prog_cb = Proc.new {|ratio| @widget.wprog.progress(ratio) }
    # set callback
    $CACHE.callback(pass_cb,stat_cb,prog_cb,dlog_cb)
    # make window
    case $rc['style']
    when "tiny"
      @widget = Recv_tiny.new(self,self)
    when "small"
      @widget = Recv_small.new(self,self)
    when "large"
      @widget = Recv_large.new(self,self)
    end
    @widget.pack('side'=>'top','fill'=>'both','expand'=>'yes')
    withdraw
    tk_call 'bind','Text','<Control-v>',' '
    tk_call 'bind','Text','<Control-p>',' '
    # timer
    @ticks = 0
    @timer = 0
    @timer_cb = nil
    timer = Proc.new {
      # oneshot
      if(@timer) then
	@timer -= 1
	if(@timer == 0) then
	  if(@timer_cb) then
	    @timer_cb.call()
	  end
	end
      end
      # interval
      @ticks += 1
      $main.widget.wlist.biff(@ticks)
      # startup
      if(@ticks == 2) then
	$tkroot.withdraw
	deiconify
      end
      Tk.after(1000) { timer.call }
    }
    Tk.after(1000) { timer.call }
  end
  # busy?
  def busy?
    @work
  end
  # busy
  def busy
    @work = true
    cursor "watch"
    @widget.wmbox.busy
    @widget.wlist.busy
#    @widget.wheader.busy
    @widget.wview.busy
    update
  end
  # idle
  def idle
    cursor ""
    @widget.wmbox.idle
    @widget.wlist.idle
#    @widget.wheader.idle
    @widget.wview.idle
    update
    @work = false
  end
  # set timer callback
  def settimer(t,cb)
    @timer = t
    @timer_cb = cb
  end
  # hide text view
  def hideview
    if($rc['style'] == "tiny")
      $main.widget.wview.withdraw
    else
      $main.widget.wview.clear
      $main.widget.wlist.focused.focus
    end
  end
  # remake view
  def remake
    $main.widget.remake if($rc['style'] == "tiny")
  end
  # find next
  def findnext
    $main.widget.wlist.findnext
  end
  # quit
  def quit
    $main.widget.wlist.close
    $CACHE.finish()
    exit
  end
end
