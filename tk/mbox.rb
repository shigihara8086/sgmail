#
# recv command frame
#

class Mbox_cmdbar < SGFrame
  def initialize(top,par)
    @top = top
    super(par)
    borderwidth 1
    # send
    SGImagebutton.new(self,"send",$rc2['button']['send']) {
      command proc { par.tree.send unless($main.busy?) }
      pack('side'=>'left')
    }
    # sep
    TkLabel.new(self) {
      image ICON::IMAGE['separater']
      pack('side'=>'left')
    }
    # create mail box
    SGImagebutton.new(self,"newb",$rc2['button']['create']) {
      command proc { par.tree.create unless($main.busy?) }
      pack('side'=>'left')
    }
    # delete mail box
    SGImagebutton.new(self,"delb",$rc2['button']['delete']) {
      command proc { par.tree.delete unless($main.busy?) }
      pack('side'=>'left')
    }
    # rename mail box
    SGImagebutton.new(self,"rename",$rc2['button']['rename']) {
      command proc { par.tree.rename unless($main.busy?) }
      pack('side'=>'left')
    }
    # clear trash mail box
    SGImagebutton.new(self,"trash",$rc2['button']['trash']) {
      command proc { par.tree.trash unless($main.busy?) }
      pack('side'=>'left')
    }
  end
end

#
# Folder list
#

class Folder_list < SGListbox
attr :tree
attr :now
attr :pos
attr :parent
  def initialize(par)
    super(par)
    @par = par
    @now = nil
    @pos = nil
    @parent = nil
    @tree = []
    $CACHE.each_server {|name,server|
      @tree << { 'nam'=>name,'val'=>server,'cld'=>[],'exp'=>false }
    }
    # wheelmouse by akkey
    bind "ButtonPress-4",proc { yview 'scroll','-1','units' }
    bind "ButtonPress-5",proc { yview 'scroll','1','units' }
    #
    bind "ButtonRelease-1",proc { click }
    bind "Double-ButtonRelease-1",proc { double_click }
    @list = []
    @line = 0
    @ind = 0
    disp(@tree)
  end
  # show tree
  def disp(tree)
    tree.each {|v|
      @list[@line] = v
      if(v['cld']) then
	if(v['exp']) then
	  insert(@line,format("%s[-]%s"," " * @ind,v['nam']))
	  @line += 1
	  @ind += 1
	  disp(v['cld'])
	  @ind -= 1
	else
	  insert(@line,format("%s[+]%s"," " * @ind,v['nam']))
	  @line += 1
	end
      else
	if(v['lef'] == nil) then
	  folder = v['val']
	  if(folder.box != "") then
	    list = folder.folder
	    if(list != []) then
	      v['lef'] = false
	    else
	      v['lef'] = true
	    end
	  else
	    v['lef'] = true
	  end
	end
	if(v['lef']) then
	  insert(@line,format("%s[ ]%s"," " * @ind,v['nam']))
	else
	  insert(@line,format("%s[+]%s"," " * @ind,v['nam']))
	end
	@line += 1
      end
    }
  end
  # redisplay
  def redisp(pos)
    @list = []
    @line = 0
    @ind = 0
    delete(0,'end')
    disp(@tree)
    see(pos)
    selection_anchor(pos)
    selection_set(pos,pos)
    activate(pos)
  end
  # colappse
  def colappse(pos)
    item = @list[pos]
    folder = item['val']
    folder.close() if(folder.box == "/")
    item['cld'] = []
    item['exp'] = false
  end
  # expand
  def expand(pos)
    item = @list[pos]
    folder = item['val']
    if(folder.box != "") then
      folder.open() if(folder.box == "/")
      list = folder.folder
      if(list != []) then
	child = []
	list.each {|f|
	  child << { 'nam'=>f.name,'val'=>f }
	}
	item['cld'] = child
	item['exp'] = true
      else
	$main.widget.wlist.open(folder)
	item['lef'] = true
      end
    else
      $main.widget.wlist.open(folder)
      item['lef'] = true
    end
  end
  # click item
  def click
    @pos = curselection[0]
    @now = @list[@pos]
    folder = @now['val']
    if(folder.box == "/") then
      @parent = folder
    else
      @parent = nil
    end
  end
  # double click item
  def double_click
    pos = curselection[0]
    if(@list[pos]['exp']) then
      colappse(pos)
    else
      expand(pos)
    end
    redisp(pos)
  end
  # find folder
  def find(folder)
    @list.each_index {|i|
      item = @list[i]
      if(folder == item['val']) then
	return(i)
      end
    }
    nil
  end
end

#
# mail box list frame
#

class Mbox_tree < SGFrame
attr :list
  def initialize(top,par)
    super(par)
    @top = top
    @ybar = SGScrollbar.new(self) {
      orient 'vertical'
      pack('side'=>'right','fill'=>'y')
    }
    @list = Folder_list.new(self)
    @list.setgrid 'yes'
    @list.selectborderwidth 0
    @list.foreground $tkrc['mbox_color'][0]
    @list.background $tkrc['mbox_color'][1]
    @list.selectforeground $tkrc['select_color'][0]
    @list.selectbackground $tkrc['select_color'][1]
    @list.width $tkrc['mbox_width']
    @list.height $tkrc['list_height']
    @list.pack('side'=>'left','fill'=>'y')
    @ybar.command proc{|idx| @list.yview *idx }
    @list.yscroll proc {|idx| @ybar.set *idx }
    @list.bind "ButtonPress-1",proc {|x,y|
      @list.focus
    },"%X %Y"
  end
  # busy
  def busy
    cursor "watch"
    @list.cursor "watch"
  end
  # idle
  def idle
    cursor ""
    @list.cursor ""
  end
  # send new mail
  def send
    if(@list.now) then
      server = @list.now['val'].server
      if(server) then
	send = Send.new("Send",server,nil,nil)
      else
	SGDialog.new("select_server_error")
      end
    end
  end
  # create new folder
  def create
    if(@list.parent) then
      folder = @list.parent
      if(folder.box != "") then
	dlg = MboxCreateDialog.new("")
	if(dlg.value && dlg.value != "") then
	  if(folder.create(dlg.value)) then
	  end
	end
      end
    end
  end
  # delete folder
  def delete
    if(@list.now) then
      folder = @list.now['val']
      if(folder.box != "/" && folder.box != "") then
        dlg = SGDialog.new(
	  "delete_correct","warning",["OK","Cancel"],folder.name)
	if(dlg.value == "OK") then
	  $main.widget.wlist.close
	  if(folder.delete()) then
	  end
	end
      end
    end
  end
  # rename folder
  def rename
    if(@list.now) then
      folder = @list.now['val']
      dlg = MboxRenameDialog.new(folder.box)
      if(dlg.value && dlg.value != "") then
	$main.widget.wlist.close
	if(folder.rename(dlg.value)) then
	end
      end
    end
  end
  # trash clear
  def trash
    if(@list.now) then
      folder = @list.now['val']
      dlg = SGDialog.new("trash_correct","warning",["OK","Cancel"])
      if(dlg.value == "OK") then
	$main.widget.wlist.close
	folder.server.trash()
      end
    end
  end
  # config
  def rc_config
    Rc_config.new
  end
end

#
# recv mailbox list and subject list,header,progress bar,viewer
#

class Mbox < SGFrame
attr :tree
  def initialize(top,par)
    super(par)
    @top = top
    @cbar = Mbox_cmdbar.new(top,self)
    @tree = Mbox_tree.new(top,self)
    @cbar.pack('side'=>'top','fill'=>'x')
    @tree.pack('side'=>'bottom','fill'=>'both','expand'=>'yes')
  end
end
